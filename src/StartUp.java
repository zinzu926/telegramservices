
import cat.tecnocampus.telegramservice.TelegramService;


public class StartUp {
    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {       

        TelegramService telegramService = TelegramService.getInstancia();
        telegramService.startService();
    }

}
