package cat.tecnocampus.telegramservice;

import cat.tecnocampus.telegramservice.apistate.Account;
import cat.tecnocampus.telegramservice.controller.OutgoingMessagesController;
import cat.tecnocampus.telegramservice.controller.SocketController;
import cat.tecnocampus.telegramservice.persistence.AccountMapper;
import cat.tecnocampus.telegramservice.thread.ApplicationThread;
import cat.tecnocampus.telegramservice.util.MyLogger;
import cat.tecnocampus.telegramservice.util.MySocketException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.telegram.api.engine.LoggerInterface;
import org.telegram.mtproto.log.LogInterface;
import org.telegram.mtproto.log.Logger;

public class TelegramService {

    private ExecutorService threadPool;
    private static Map<String, ApplicationThread> applications;
    private OutgoingMessagesController outgoingMessagesController;
    private SocketController socketController;
    private static TelegramService telegramService = null;
 

    private TelegramService() {

    }

    public static synchronized TelegramService getInstancia() {
        if (telegramService == null) {
            telegramService = new TelegramService();
            disableLogging();
        }
        return telegramService;

    }

    public void startService() {

        this.threadPool = Executors.newCachedThreadPool();
        this.applications = new HashMap<>();
        this.initAccounts();
        this.initControllers();
    }

    private void initAccounts() {
        AccountMapper accountMapper = new AccountMapper();
 
        List<Account> accounts = accountMapper.getAccounts();
        for (Account account : accounts) {
            startApplicationThread(account);
        }
        accounts.clear();
        accounts = null;
        accountMapper = null;
    }

    public synchronized void startApplicationThread(Account account) {
        MyLogger.log("Starting application thread for phone number: " + account.getPhoneNumber());
        ApplicationThread thread = new ApplicationThread(account);
        thread.createApi();
        applications.put(account.getPhoneNumber(), thread);
        threadPool.execute(thread);

    }

    
    
    public synchronized void removeApplicationThread(String phoneNumber, boolean remove) {
        MyLogger.log("Removing application thread for phone number: " + phoneNumber);
        if (!applications.containsKey(phoneNumber)) {
            return;
        }
        ApplicationThread thread = applications.get(phoneNumber);
        thread.shutdown();
        thread = null;
        if (remove) {
            applications.remove(phoneNumber);
        }

        System.gc();
    }

    private void initControllers() {
        outgoingMessagesController = new OutgoingMessagesController(applications);
        outgoingMessagesController.startOutgoingMessagesPoller();
        socketController = new SocketController(this);
        socketController.startSocket();
    }

    public synchronized void addNewPhoneNumber(String phoneNumber) throws MySocketException {
        if (!applications.containsKey(phoneNumber)) {
           // throw new MySocketException("0<separa>Error<separa>Phone number is already running<separa></finalitzacio>");

            AccountMapper accountMapper = new AccountMapper();
            /*if(accountMapper.exists(phoneNumber))
             throw new MySocketException("0<separa>Error<separa>Phone number is already registered in the database<separa></finalitzacio>");*/
            Account account = new Account(-1, -1, phoneNumber, false);
            int accountId = accountMapper.getId(account);
            account.setAccountId(accountId);
            ApplicationThread thread = new ApplicationThread(account);
            thread.createApi();
            applications.put(phoneNumber, thread);
            threadPool.execute(thread);
            accountMapper = null;
        }
    }

    public void removePhoneNumber(String phoneNumber) {
        removeApplicationThread(phoneNumber, true);
        new AccountMapper().delete(phoneNumber);
    }

    public synchronized ApplicationThread getApplicationThread(String phoneNumber) throws MySocketException {
        if (!applications.containsKey(phoneNumber)) {
            throw new MySocketException("0<separa>Error<separa>Phone number is not running<separa></finalitzacio>");
        }
        return applications.get(phoneNumber);
    }

    public synchronized void login(String phoneNumber) throws MySocketException {
        if (applications.containsKey(phoneNumber)) {
            throw new MySocketException("0<separa>Error<separa>Phone number is already running</finalitzacio>");
        }
        AccountMapper accountMapper = new AccountMapper();
        Account account = accountMapper.get(phoneNumber);
        if (account == null) {
            throw new MySocketException("0<separa>Error<separa>No account with this phone number registered in the database<separa></finalitzacio>");
        }
        account.setLoggedIn(true);
        accountMapper.update(phoneNumber,true, true);
        startApplicationThread(account);
        accountMapper = null;
    }

    public synchronized void logout(String phoneNumber) throws MySocketException {
        AccountMapper accountMapper = new AccountMapper();
        if (!accountMapper.exists(phoneNumber)) {
            throw new MySocketException("0<separa>Error<separa>No account with this phone number registered in the database<separa></finalitzacio>");
        }
        accountMapper.update(phoneNumber,false, false);
        if (!applications.containsKey(phoneNumber)) {
            throw new MySocketException("0<separa>Error<separa>This phone number is not running</finalitzacio>");
        }
        applications.get(phoneNumber).getApiState().logout();
        applications.get(phoneNumber).getUpdatesStateController().deleteUpdateState(phoneNumber);
        removeApplicationThread(phoneNumber, true);
        accountMapper = null;
    }

    private static void disableLogging() {
        Logger.registerInterface(new LogInterface() {
            @Override
            public void w(String tag, String message) {

            }

            @Override
            public void d(String tag, String message) {

            }

            @Override
            public void e(String tag, Throwable t) {

            }
        });
        org.telegram.api.engine.Logger.registerInterface(new LoggerInterface() {
            @Override
            public void w(String tag, String message) {

            }

            @Override
            public void d(String tag, String message) {

            }

            @Override
            public void e(String tag, Throwable t) {

            }
        });
    }

}
