/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.tecnocampus.telegramservice.process;

import cat.tecnocampus.telegramservice.controller.IncomingMessagesController;
import cat.tecnocampus.telegramservice.persistence.EncryptedChat;
import cat.tecnocampus.telegramservice.persistence.EncryptedChatMapper;
import cat.tecnocampus.telegramservice.persistence.EncryptedChatState;
import cat.tecnocampus.telegramservice.thread.ApplicationThread;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.telegram.api.TLAbsEncryptedChat;
import org.telegram.api.TLEncryptedChat;
import org.telegram.api.TLEncryptedChatDiscarded;
import org.telegram.api.TLEncryptedChatRequested;
import org.telegram.api.TLEncryptedChatWaiting;
import org.telegram.api.TLInputEncryptedChat;
import org.telegram.api.messages.TLDhConfig;
import org.telegram.api.requests.TLRequestMessagesAcceptEncryption;
import org.telegram.api.requests.TLRequestMessagesGetDhConfig;
import org.telegram.mtproto.secure.CryptoUtils;
import static org.telegram.mtproto.secure.CryptoUtils.alignKeyZero;
import static org.telegram.mtproto.secure.CryptoUtils.fromBigInt;
import static org.telegram.mtproto.secure.CryptoUtils.loadBigInt;
import static org.telegram.mtproto.secure.CryptoUtils.substring;
import org.telegram.mtproto.secure.Entropy;
import org.telegram.tl.StreamingUtils;
import static org.telegram.tl.StreamingUtils.readLong;
import org.telegram.tl.TLBytes;

/**
 *
 * @author netlab
 */
public class UpdateEncryption implements Runnable {

    private final TLAbsEncryptedChat chat;
    private final ApplicationThread application;
    private static final BigInteger[] KNOWN_PRIMES = new BigInteger[]{
        new BigInteger("C71CAEB9C6B1C9048E6C522F70F13F73980D40238E3E21C14934D037563D930F48198A0AA7C14058229493D22530F4DBFA336F6E0AC925139543AED44CCE7C3720FD51F69458705AC68CD4FE6B6B13ABDC9746512969328454F18FAF8C595F642477FE96BB2A941D5BCD1D4AC8CC49880708FA9B378E3C4F3A9060BEE67CF9A4A4A695811051907E162753B56B0F6B410DBA74D8A84B2A14B3144E0EF1284754FD17ED950D5965B4B9DD46582DB1178D169C6BC465B0D6FF9CA3928FEF5B9AE4E418FC15E83EBEA0F87FA9FF5EED70050DED2849F47BF959D956850CE929851F0D8115F635B105EE2E4E15D04B2454BF6F4FADF034B10403119CD8E3B92FCC5B", 16)
    };

    public UpdateEncryption(ApplicationThread app, TLAbsEncryptedChat chat) {
        this.chat = chat;
        application = app;
    }

    @Override
    public void run() {
        int id = chat.getId();
        EncryptedChatMapper encryptedChatMapper = new EncryptedChatMapper();
        encryptedChatMapper.crearConnexio();
        EncryptedChat encChat = encryptedChatMapper.getEncryptedChat(id, application.getAccount().getAccountId());
        if (encChat != null && encChat.getState() == EncryptedChatState.WAITING && chat instanceof TLEncryptedChat) {
            enableEncryption(encChat, (TLEncryptedChat) chat, encryptedChatMapper);
        }
        encChat = updateEncryptedChat(chat, encryptedChatMapper);
        if (encChat != null) {
            if (encChat.getState() == EncryptedChatState.REQUESTED) {
                confirmEncryption(encChat, encryptedChatMapper);
            } else if (encChat.getState() == EncryptedChatState.DISCARDED) {
                encryptedChatMapper.deleteChat(encChat);
            }
        }
        encryptedChatMapper.tancarConnexio();
    }

    public void confirmEncryption(EncryptedChat chat, EncryptedChatMapper encryptedChatMapper) {
        try {

            if (chat != null) {
                byte[] rawGa;
// byte[] nonce;
                try {
                    ByteArrayInputStream stream = new ByteArrayInputStream(chat.getKey());
                    int primeLen = StreamingUtils.readInt(stream);
                    rawGa = StreamingUtils.readBytes(primeLen, stream);
// int aLen = StreamingUtils.readInt(stream);
// nonce = StreamingUtils.readBytes(aLen, stream);
                } catch (IOException e) {
                    e.printStackTrace();
                    return;
                }
                TLDhConfig dhConfig = (TLDhConfig) application.getApi().doRpcCall(new TLRequestMessagesGetDhConfig(0, 256));
                BigInteger g = new BigInteger("" + dhConfig.getG());
                BigInteger dhPrime = loadBigInt(dhConfig.getP().cleanData());
                try {
                    checkDhConfig(dhPrime, dhConfig.getG());
                } catch (IOException e) {
                    e.printStackTrace();
                    return;
                }
                BigInteger ga = CryptoUtils.loadBigInt(rawGa);
                BigInteger b = CryptoUtils.loadBigInt(Entropy.generateSeed(dhConfig.getRandom().cleanData()));
                BigInteger gb = g.modPow(b, dhPrime);
                BigInteger pow2 = new BigInteger("2").pow(2048 - 64);
                if (ga.equals(new BigInteger("1")) || ga.compareTo(dhPrime.subtract(new BigInteger("1"))) >= 0) {
                    return;
                }
                if (ga.compareTo(pow2) <= 0 || ga.compareTo(dhPrime.subtract(pow2)) >= 0) {
                    return;
                }
                if (gb.equals(new BigInteger("1")) || gb.compareTo(dhPrime.subtract(new BigInteger("1"))) >= 0) {
                    return;
                }
                if (gb.compareTo(pow2) <= 0 || gb.compareTo(dhPrime.subtract(pow2)) >= 0) {
                    return;
                }
                byte[] key = alignKeyZero(CryptoUtils.fromBigInt(ga.modPow(b, dhPrime)), 256);
                long keyF = readLong(substring(CryptoUtils.SHA1(key), 12, 8), 0);

                TLAbsEncryptedChat encryptedChat = application.getApi().doRpcCall(new TLRequestMessagesAcceptEncryption(new TLInputEncryptedChat(chat.getIdChat(), chat.getAccessHash()),
                        new TLBytes(gb.toByteArray()), keyF));
                updateEncryptedChat(encryptedChat, encryptedChatMapper);
                encryptedChatMapper.updateEncryptedChatKey(encryptedChat, chat.getIdCompte(), key);

            }

        } catch (IOException ex) {
            Logger.getLogger(IncomingMessagesController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void checkDhConfig(BigInteger p, int g) throws IOException {
        if (g != 2 && g != 3 && g != 4 && g != 5 && g != 6 && g != 7) {
            throw new IOException();
        }
        if (p.bitLength() != 2048) {
            throw new IOException();
        }
        for (int i = 0; i < KNOWN_PRIMES.length; i++) {
            if (KNOWN_PRIMES[i].equals(p)) {
                return;
            }
        }
        if (!p.isProbablePrime(20)) {
            throw new IOException();
        }
        if (!p.subtract(new BigInteger("1").divide(new BigInteger("2"))).isProbablePrime(20)) {
            throw new IOException();
        }
//Since g is always equal to 2, 3, 4, 5, 6 or 7, this is easily done using quadratic reciprocity law,
// yielding a simple condition on p mod 4g — namely,
// p mod 8 = 7 for g = 2,
// p mod 3 = 2 for g = 3,
// no extra condition for g = 4,
// p mod 5 = 1 or 4 for g = 5,
// p mod 24 = 11 or 23 for g = 6
// and p mod 7 = 2 or 3 for g = 7.
        if (g == 2) {
            int res = p.mod(new BigInteger("8")).intValue();
            if (res != 7) {
                throw new IOException();
            }
        }
        if (g == 3) {
            int res = p.mod(new BigInteger("3")).intValue();
            if (res != 2) {
                throw new IOException();
            }
        }
        if (g == 5) {
            int res = p.mod(new BigInteger("5")).intValue();
            if (res != 1 && res != 4) {
                throw new IOException();
            }
        }
        if (g == 6) {
            int res = p.mod(new BigInteger("24")).intValue();
            if (res != 11 && res != 23) {
                throw new IOException();
            }
        }
        if (g == 7) {
            int res = p.mod(new BigInteger("7")).intValue();
            if (res != 2 && res != 3) {
                throw new IOException();
            }
        }
    }

    public EncryptedChat updateEncryptedChat(TLAbsEncryptedChat chat, EncryptedChatMapper encryptedChatMapper) {
        int id = chat.getId();
        if (id == 0) {

            return null;
        }
        /*int date = (int) (TimeOverlord.getInstance().getServerTime() / 1000);
         if (chat instanceof TLEncryptedChat) {
         date = ((TLEncryptedChat) chat).getDate();
         } else if (chat instanceof TLEncryptedChatRequested) {
         date = ((TLEncryptedChatRequested) chat).getDate();
         } else if (chat instanceof TLEncryptedChatWaiting) {
         date = ((TLEncryptedChatWaiting) chat).getDate();
         }*/
        EncryptedChat encryptedChat = encryptedChatMapper.getEncryptedChat(chat.getId(), application.getAccount().getAccountId());
        if (encryptedChat != null) {
            if (!(chat instanceof TLEncryptedChat) && !(chat instanceof TLEncryptedChatDiscarded)) {
                return null;
            }
            encryptedChat = writeEncryptedChatInfo(encryptedChat, chat);
            encryptedChatMapper.updateChat(encryptedChat);
        } else {
            if (!(chat instanceof TLEncryptedChatWaiting) && !(chat instanceof TLEncryptedChatRequested)) {
                return null;
            }
            encryptedChat = new EncryptedChat();
            encryptedChat.setIdChat(id);
            encryptedChat.setIdCompte(application.getAccount().getAccountId());

            encryptedChat = writeEncryptedChatInfo(encryptedChat, chat);            
            encryptedChatMapper.insertChat(encryptedChat);
            //secretDatabase.createChat(encryptedChat);
        }
        return encryptedChat;

    }

    private EncryptedChat writeEncryptedChatInfo(EncryptedChat chat, TLAbsEncryptedChat rawChat) {
        if (rawChat instanceof TLEncryptedChatRequested) {
            TLEncryptedChatRequested requested = (TLEncryptedChatRequested) rawChat;
            chat.setAccessHash(requested.getAccessHash());
            chat.setUserId(requested.getAdminId());
            byte[] tmpKey = CryptoUtils.concat(
                    StreamingUtils.intToBytes(requested.getGA().getLength()),
                    requested.getGA().cleanData());
            chat.setDate(requested.getDate());
            chat.setKey(tmpKey);
            chat.setState(EncryptedChatState.REQUESTED);
            chat.setOut(false);
        } else if (rawChat instanceof TLEncryptedChatWaiting) {
            TLEncryptedChatWaiting waiting = (TLEncryptedChatWaiting) rawChat;
            chat.setAccessHash(waiting.getAccessHash());
            chat.setDate(waiting.getDate());
            chat.setUserId(waiting.getParticipantId());
            chat.setState(EncryptedChatState.WAITING);
            chat.setOut(true);
        } else if (rawChat instanceof TLEncryptedChatDiscarded) {
            chat.setState(EncryptedChatState.DISCARDED);
        } else if (rawChat instanceof TLEncryptedChat) {
            chat.setState(EncryptedChatState.NORMAL);
        }
        return chat;
    }

    private void enableEncryption(EncryptedChat encChat, TLEncryptedChat encryptedChat, EncryptedChatMapper encryptedChatMapper) {
        ByteArrayInputStream stream = new ByteArrayInputStream(encChat.getKey());
        byte[] prime;
        byte[] rawA;
        try {
            int primeLen = StreamingUtils.readInt(stream);
            prime = StreamingUtils.readBytes(primeLen, stream);
            int aLen = StreamingUtils.readInt(stream);
            rawA = StreamingUtils.readBytes(aLen, stream);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        BigInteger a = loadBigInt(rawA);
        BigInteger gb = loadBigInt(encryptedChat.getGAOrB().cleanData());
        BigInteger dhPrime = loadBigInt(prime);
        if (gb.equals(new BigInteger("1")) || gb.compareTo(dhPrime.subtract(new BigInteger("1"))) >= 0) {
            return;
        }
        BigInteger pow2 = new BigInteger("2").pow(2048 - 64);
        if (gb.compareTo(pow2) <= 0 || gb.compareTo(dhPrime.subtract(pow2)) >= 0) {
            return;
        }
        byte[] key = alignKeyZero(fromBigInt(gb.modPow(a, dhPrime)), 256);
        long keyF = readLong(substring(CryptoUtils.SHA1(key), 12, 8), 0);
        encryptedChatMapper.updateEncryptedChatKey(encryptedChat, encChat.getIdCompte(), key);
        //Logger.d(TAG, "Complete encryption: " + keyF);
    }

}
