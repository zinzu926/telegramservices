package cat.tecnocampus.telegramservice.process;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;
import java.util.concurrent.Callable;

import org.telegram.api.TLInputFile;
import org.telegram.api.TLInputGeoPointEmpty;
import org.telegram.api.TLInputPhotoCropAuto;
import org.telegram.api.requests.TLRequestPhotosUploadProfilePhoto;

import cat.tecnocampus.telegramservice.thread.ApplicationThread;
import cat.tecnocampus.telegramservice.util.MyLogger;
import cat.tecnocampus.telegramservice.util.MySocketException;
import cat.tecnocampus.telegramservice.util.UploadResult;

public class UpdateProfilePicture implements Callable<Object> {
	
	private Random r = new Random();
	
	private ApplicationThread application;
	private String imagePath;
	private byte[] data;
	
	public UpdateProfilePicture(ApplicationThread application, String imagePath) {
		this.application = application;
		this.imagePath = imagePath;
	}
	
	private void getData() throws MySocketException {
		try {
			Path path = Paths.get(imagePath);
			data = Files.readAllBytes(path);
		} catch (IOException e) {
			MyLogger.log(application.getAccount().getPhoneNumber(), "ERROR! IOException in getting profile picture from hard drive");
        	MyLogger.log(application.getAccount().getPhoneNumber(), "Message: " + e.getMessage());
			throw new MySocketException("IOException in getting profile picture from hard drive");
		}
	}
	
	private void updateProfilePicture() throws MySocketException {
		MyLogger.log(application.getAccount().getPhoneNumber(), "Updating profile picture");
		long fileId = r.nextLong();
		ByteArrayInputStream bis = new ByteArrayInputStream(data);
		UploadResult res = application.getUploadController().uploadFile(bis, data.length, fileId);
		try {
			application.getApi().doRpcCall(new TLRequestPhotosUploadProfilePhoto(
		        new TLInputFile(fileId, res.getPartsCount(), "ProfilePicture.jpg", res.getHash()),
		        "ProfilePicture", new TLInputGeoPointEmpty(), new TLInputPhotoCropAuto()));
		} catch (IOException e) {
			MyLogger.log(application.getAccount().getPhoneNumber(), "ERROR! IOException in updating profile picture");
        	MyLogger.log(application.getAccount().getPhoneNumber(), "Message: " + e.getMessage());
			throw new MySocketException("IOException in updating profile picture");
		}
	}

	@Override
	public Object call() throws Exception {
		getData();
		updateProfilePicture();
		return null;
	}

}