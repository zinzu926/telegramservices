/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.tecnocampus.telegramservice.process;

import cat.tecnocampus.telegramservice.persistence.EncryptedChat;
import cat.tecnocampus.telegramservice.persistence.EncryptedChatMapper;
import cat.tecnocampus.telegramservice.thread.ApplicationThread;
import java.io.IOException;
import org.telegram.api.TLAbsEncryptedMessage;
import org.telegram.api.TLDecryptedMessage;
import org.telegram.api.TLDecryptedMessageLayer;
import org.telegram.api.TLDecryptedMessageMediaEmpty;
import org.telegram.api.TLEncryptedMessage;
import org.telegram.mtproto.secure.CryptoUtils;
import static org.telegram.mtproto.secure.CryptoUtils.AES256IGEDecrypt;
import static org.telegram.mtproto.secure.CryptoUtils.SHA1;
import static org.telegram.mtproto.secure.CryptoUtils.arrayEq;
import static org.telegram.mtproto.secure.CryptoUtils.substring;
import org.telegram.tl.StreamingUtils;
import static org.telegram.tl.StreamingUtils.readLong;
import org.telegram.tl.TLObject;

/**
 *
 * @author netlab
 */
public class EncryptedIncomingMessage implements Runnable {

    private final ApplicationThread application;
    private final TLAbsEncryptedMessage encryptedMessage;

    public EncryptedIncomingMessage(ApplicationThread app, TLAbsEncryptedMessage encryptedMessage) {
        application = app;
        this.encryptedMessage = encryptedMessage;
    }

    @Override
    public void run() {

        if (encryptedMessage instanceof TLEncryptedMessage) {
            TLEncryptedMessage encMsg = (TLEncryptedMessage) encryptedMessage;

            EncryptedChatMapper encryptedChatMapper = new EncryptedChatMapper();
            encryptedChatMapper.crearConnexio();
            EncryptedChat chat = encryptedChatMapper.getEncryptedChat(encMsg.getChatId(), application.getAccount().getAccountId());
            if (chat == null) {
                return;
            }
            try {
                byte[] rawMessage = decryptMessage(encMsg.getBytes().cleanData(), encMsg.getChatId());
                if (rawMessage == null) {
                    return;
                }
                TLObject object = application.getApi().getApiContext().deserializeMessage(rawMessage);
                if (object instanceof TLDecryptedMessageLayer) {
                    object = ((TLDecryptedMessageLayer) object).getMessage();
                }
                if (object instanceof TLDecryptedMessage) {
                    TLDecryptedMessage decryptedMessage = (TLDecryptedMessage) object;
                    if (decryptedMessage.getMedia() instanceof TLDecryptedMessageMediaEmpty) {                     
                        new IncomingMessage(application, chat.getDate(), chat.getUserId(), decryptedMessage.getMessage(), null, encMsg, chat.getIdChat()).processa();
                    } else {
                        new IncomingMessage(application, chat.getDate(), chat.getUserId(), "", decryptedMessage, encMsg, chat.getIdChat()).processa();
                    }

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            encryptedChatMapper.tancarConnexio();
        } 
    }

    public byte[] decryptMessage(byte[] rawData, int chatId) {
        EncryptedChatMapper encryptedChatMapper = new EncryptedChatMapper();
        encryptedChatMapper.crearConnexio();
        EncryptedChat chat = encryptedChatMapper.getEncryptedChat(chatId, application.getAccount().getAccountId());

        if (chat == null) {
            return null;
        }
        byte[] key = chat.getKey();
        long originalFingerprint = readLong(substring(SHA1(key), 12, 8), 0);
        long keyFingerprint = readLong(substring(rawData, 0, 8), 0);
        byte[] msgKey = substring(rawData, 8, 16);
        byte[] msg = substring(rawData, 24, rawData.length - 24);
        if (keyFingerprint != originalFingerprint) {

            return null;
        }
        byte[] sha1_a = SHA1(msgKey, substring(key, 0, 32));
        byte[] sha1_b = SHA1(substring(key, 32, 16), msgKey, substring(key, 48, 16));
        byte[] sha1_c = SHA1(substring(key, 64, 32), msgKey);
        byte[] sha1_d = SHA1(msgKey, substring(key, 96, 32));
        byte[] aesKey = CryptoUtils.concat(substring(sha1_a, 0, 8), substring(sha1_b, 8, 12), substring(sha1_c, 4, 12));
        byte[] aesIv = CryptoUtils.concat(substring(sha1_a, 8, 12), substring(sha1_b, 0, 8), substring(sha1_c, 16, 4), substring(sha1_d, 0, 8));
        byte[] decrypted = AES256IGEDecrypt(msg, aesIv, aesKey);
        int len = StreamingUtils.readInt(decrypted);
        byte[] rawMessage = substring(decrypted, 4, len);
        byte[] decrSha = substring(SHA1(substring(decrypted, 0, len + 4)), 4, 16);
        if (!arrayEq(decrSha, msgKey)) {

            return null;
        }
        return rawMessage;
    }

}
