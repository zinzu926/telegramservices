package cat.tecnocampus.telegramservice.process;

import cat.tecnocampus.telegramservice.persistence.DbBroadcast;
import cat.tecnocampus.telegramservice.persistence.DbBroadcastMapper;
import cat.tecnocampus.telegramservice.persistence.DbMessage;
import cat.tecnocampus.telegramservice.persistence.DbMessageMapper;
import cat.tecnocampus.telegramservice.persistence.DbUser;
import cat.tecnocampus.telegramservice.persistence.DbUserMapper;
import cat.tecnocampus.telegramservice.persistence.EncryptedChat;
import cat.tecnocampus.telegramservice.persistence.EncryptedChatMapper;
import cat.tecnocampus.telegramservice.thread.ApplicationThread;
import cat.tecnocampus.telegramservice.util.Configuracio;
import cat.tecnocampus.telegramservice.util.MyLogger;
import cat.tecnocampus.telegramservice.util.RespostaKeyword;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.telegram.api.TLAbsInputFileLocation;
import org.telegram.api.TLAbsMessageMedia;
import org.telegram.api.TLAbsPhotoSize;
import org.telegram.api.TLAbsUser;
import org.telegram.api.TLDecryptedMessage;
import org.telegram.api.TLDecryptedMessageMediaAudio;
import org.telegram.api.TLDecryptedMessageMediaDocument;
import org.telegram.api.TLDecryptedMessageMediaGeoPoint;
import org.telegram.api.TLDecryptedMessageMediaPhoto;
import org.telegram.api.TLDecryptedMessageMediaVideo;
import org.telegram.api.TLEncryptedFile;
import org.telegram.api.TLEncryptedMessage;
import org.telegram.api.TLFileLocation;
import org.telegram.api.TLGeoPoint;
import org.telegram.api.TLInputEncryptedChat;
import org.telegram.api.TLInputEncryptedFileLocation;
import org.telegram.api.TLInputFileLocation;
import org.telegram.api.TLInputPeerForeign;
import org.telegram.api.TLMessageMediaAudio;
import org.telegram.api.TLMessageMediaDocument;
import org.telegram.api.TLMessageMediaEmpty;
import org.telegram.api.TLMessageMediaGeo;
import org.telegram.api.TLMessageMediaPhoto;
import org.telegram.api.TLMessageMediaUnsupported;
import org.telegram.api.TLMessageMediaVideo;
import org.telegram.api.TLPhoto;
import org.telegram.api.TLPhotoSize;
import org.telegram.api.TLUserContact;
import org.telegram.api.TLUserForeign;
import org.telegram.api.TLUserRequest;
import org.telegram.api.engine.RpcException;
import org.telegram.api.messages.TLAbsMessages;
import org.telegram.api.messages.TLAffectedHistory;
import org.telegram.api.requests.TLRequestMessagesGetMessages;
import org.telegram.api.requests.TLRequestMessagesReadEncryptedHistory;
import org.telegram.api.requests.TLRequestMessagesReadHistory;
import org.telegram.api.requests.TLRequestMessagesReceivedMessages;
import org.telegram.api.storage.TLAbsFileType;
import org.telegram.api.storage.TLFileGif;
import org.telegram.api.storage.TLFileJpeg;
import org.telegram.api.storage.TLFileMov;
import org.telegram.api.storage.TLFileMp3;
import org.telegram.api.storage.TLFileMp4;
import org.telegram.api.storage.TLFilePartial;
import org.telegram.api.storage.TLFilePdf;
import org.telegram.api.storage.TLFilePng;
import org.telegram.api.storage.TLFileUnknown;
import org.telegram.api.storage.TLFileWebp;
import org.telegram.api.upload.TLFile;
import org.telegram.mtproto.secure.CryptoUtils;
import org.telegram.tl.TLBytes;
import org.telegram.tl.TLIntVector;
import org.telegram.tl.TLVector;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class IncomingMessage implements Runnable {

    private static final DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm");
    private static final int timeoutWebService = 30 * 1000;

    private static final List<String> alta = Arrays.asList("alta");
    private static final List<String> baixa = Arrays.asList("baixa", "baja");

    private final ApplicationThread application;
    private DbMessageMapper dbMessageMapper;
    private ByteArrayOutputStream outputStream;

    private DbMessage dbMessage;

    private final int messageId;
    private final int fromId;
    private String message;
    private final long timestamp;
    private final Object media;

    private int datatype = 0, tipusMissatge = 0;
    private String path = "";
    double latitude = 0;
    double longitude = 0;
    private int idUser;
    private String fileExtension;
    private long access_hash;
    private String origen = "";
    private TLEncryptedMessage encMsg;
    private boolean isEncrypted;
    private int idChat;

    public IncomingMessage(ApplicationThread application, int messageId, int fromId, String message, Object media, TLEncryptedMessage encMsg, int idChat) {
        this.application = application;
        this.messageId = messageId;
        this.fromId = fromId;
        this.message = message;
        this.timestamp = Calendar.getInstance(TimeZone.getTimeZone("Europe/Madrid")).getTimeInMillis() / 1000;
        this.media = media;
        idUser = -1;
        this.encMsg = encMsg;
        this.idChat = idChat;
    }

    @Override
    public void run() {

        processa();
    }

    public void processa() {
        if (encMsg != null) {
            isEncrypted = true;
        }
        dbMessageMapper = new DbMessageMapper();
        boolean isMedia = false;
        boolean isMediaSupport = false;
        if ((media != null) && !(media instanceof TLMessageMediaEmpty) && !(media instanceof TLMessageMediaUnsupported)) {
            if (media instanceof TLMessageMediaGeo) {
                
                datatype = 4;
                TLMessageMediaGeo mediaGeo = (TLMessageMediaGeo) media;
                if (mediaGeo.getGeo() instanceof TLGeoPoint) {
                    latitude = ((TLGeoPoint) mediaGeo.getGeo()).getLat();
                    longitude = ((TLGeoPoint) mediaGeo.getGeo()).getLon();
                    message = Double.toString(latitude)+","+Double.toString(longitude);
                    //message=message+ "http://maps.google.com/?q="+Double.toString(latitude)+","+Double.toString(longitude);
                    isMedia = false;
                };
            } else {
                isMediaSupport = downloadMedia(media);
                isMedia = true;
            }
        }

        dbMessageMapper.crearConnexio();
        if (!isMedia) {
            if (!message.contains("'")) {
                checkIfMessageIsKeyword();

            } else {
                if (processaOrigen(false)) {
                    String miss = verificaHorari();
                    if (miss != null && !miss.equals("")) {
                        if (tipusMissatge == 4) {
                            saveMessage(5, 0, -2);
                        } else {
                            saveMessage(5, 0, -1);
                        }
                        checkTimetable(miss);
                    } else {
                        saveMessage(tipusMissatge, 0, -1);
                    }
                }
            }

        } else {
            if (isMediaSupport) {
                if (processaOrigen(false)) {
                    if (writeFile()) {
                        String miss = verificaHorari();
                        if (miss != null && !miss.equals("")) {
                            if (tipusMissatge == 4) {
                                saveMessage(5, 0, -2);
                            } else {
                                saveMessage(5, 0, -1);
                            }
                            checkTimetable(miss);
                        } else {
                            saveMessage(tipusMissatge, 0, -1);
                        }
                    }
                }
            } else {
                if (processaOrigen(false)) {
                    crearDBMessage(1, 0, -1);
                    dbMessage.setText(dbMessageMapper.getMessageError(application.getAccount().getCompanyId(), 1));
                    if (isEncrypted) {
                        dbMessage.setDataType(10);
                    } else {
                        dbMessage.setDataType(0);
                    }
                    dbMessageMapper.storeMessageThatNeedsToBeSend(dbMessage);
                }
            }
        }
        dbMessageMapper.tancarConnexio();
        dbMessageMapper = null;
        dbMessage = null;
    }

    private boolean processaOrigen(boolean broadcast) {
        boolean ok = true;
        if (!isEncrypted) {
            TLIntVector messageIds = new TLIntVector();
            messageIds.add(messageId);
            try {
                TLAbsMessages messages = application.getApi().doRpcCall(new TLRequestMessagesGetMessages(messageIds));
                TLVector<TLAbsUser> users = messages.getUsers();
                for (TLAbsUser user : users) {

                    if ((user instanceof TLUserRequest) && (user.getId() == fromId)) {
                        TLUserRequest user2 = (TLUserRequest) user;
                        origen = user2.getPhone();
                        access_hash = user2.getAccessHash();
                    } else if ((user instanceof TLUserForeign) && (user.getId() == fromId)) {
                        TLUserForeign user2 = (TLUserForeign) user;
                        access_hash = user2.getAccessHash();
                        origen = String.valueOf(fromId);

                    } else if ((user instanceof TLUserContact) && (user.getId() == fromId)) {
                        TLUserContact tlUserContact = ((TLUserContact) user);
                        access_hash = tlUserContact.getAccessHash();
                        origen = tlUserContact.getPhone();
                    }

                }
            } catch (IOException e) {
                ok = false;
                MyLogger.log(application.getAccount().getPhoneNumber(), "ERROR! in getting user access hash: " + messageId);
                MyLogger.log(application.getAccount().getPhoneNumber(), "Message: " + e.getMessage());
            }

            if (origen != null && !origen.equals("") && ok) {

                DbUserMapper dbUserMapper = new DbUserMapper();

                idUser = dbUserMapper.exists(dbMessageMapper.getConnection(), origen, application.getAccount().getAccountId());
                if (idUser < 0) {
                    int idResul = dbUserMapper.existsUID(dbMessageMapper.getConnection(), String.valueOf(fromId), application.getAccount().getAccountId());
                    if (idResul == -1) {
                        idUser = dbUserMapper.insertAll(dbMessageMapper.getConnection(), new DbUser(String.valueOf(fromId), origen, access_hash, application.getAccount().getAccountId()));
                    } else {
                        dbMessageMapper.updateMessageOrigen(origen, String.valueOf(fromId));
                        dbUserMapper.updateUserUID(dbMessageMapper.getConnection(), new DbUser(String.valueOf(fromId), origen, access_hash, application.getAccount().getAccountId()));
                    }

                } else {
                    dbUserMapper.updateUser(dbMessageMapper.getConnection(), new DbUser(String.valueOf(fromId), origen, access_hash, application.getAccount().getAccountId()));
                }
                dbUserMapper = null;
            }
        } else {
            EncryptedChatMapper encryptedChatMapper = new EncryptedChatMapper();
            encryptedChatMapper.crearConnexio();
            EncryptedChat chat = encryptedChatMapper.getEncryptedChatPerUid(fromId, application.getAccount().getAccountId());
            access_hash = chat.getAccessHash();
            encryptedChatMapper.updateDate(idChat, application.getAccount().getAccountId(), (int) timestamp);
            encryptedChatMapper.tancarConnexio();
            DbUserMapper dbUserMapper = new DbUserMapper();
            origen = dbUserMapper.getOrigenPerUid(dbMessageMapper.getConnection(), fromId, application.getAccount().getAccountId());
            if (origen != null && origen.equals("")) {
                origen = String.valueOf(fromId);
                dbUserMapper.insertAll(dbMessageMapper.getConnection(), new DbUser(String.valueOf(fromId), origen, access_hash, application.getAccount().getAccountId()));
            }
            encryptedChatMapper = null;
        }
        if (!dbMessageMapper.existUsuari(origen, application.getAccount().getCompanyId())) {
            tipusMissatge = 4;
        }

        return ok;
    }

    private void checkIfMessageIsKeyword() {
        boolean websevices = false;
        String[] array;
        String parametre = "";
        RespostaKeyword resposta;

        String missatge = message;
        if (message.contains(":")) {
            array = message.split(":");
            missatge = array[0].trim() + ":";
            parametre = (array[1].trim()).toUpperCase();
            websevices = true;
        }
        resposta = dbMessageMapper.checkIfMessageIsKeyword(application.getAccount().getCompanyId(), missatge, false, 0);
        if (resposta != null) {
            if (resposta.getMida() == 0) {
                if (message.contains(" ")) {
                    array = message.split(" ");
                    missatge = array[0];
                    parametre = (array[1]).toUpperCase();
                    resposta = dbMessageMapper.checkIfMessageIsKeyword(application.getAccount().getCompanyId(), missatge, true, 1);
                    if (resposta != null && resposta.getMida() == 1) {
                        websevices = true;
                    }
                }
            }
            if (resposta != null && resposta.getMida() == 2) {
                resposta = dbMessageMapper.checkIfMessageIsKeyword(application.getAccount().getCompanyId(), missatge, true, 0);
            }

            if (resposta != null && resposta.getMida() == 1) {
                String result;
                boolean envia = true;
                if (resposta.getServei() == 1) {
                    websevices = true;
                }
                if (websevices) {
                    result = getInfoWebServices(resposta.getResposta(), parametre);
                    if (result.equals("")) {
                        result = dbMessageMapper.getMessageError(application.getAccount().getCompanyId(), 0);
                    }

                } else {
                    result = resposta.getResposta();
                }
                boolean broadcast = false;
                if (resposta.getServei() == 2) {
                    broadcast = true;

                }
                synchronized (this) {
                    if (processaOrigen(broadcast)) {
                        if (broadcast && !isEncrypted) {
                            DbBroadcastMapper dbBroadcastMapper = new DbBroadcastMapper();
                            DbBroadcast dbBroadcast = new DbBroadcast(resposta.getIdRespostaRapida(), idUser);
                            missatge = missatge.toLowerCase().trim();
                            String accio = missatge.split(" ")[0];
                            String resta_missatge=missatge.replace(accio+" ", "");
                            if (alta.contains(accio) && !dbBroadcastMapper.exists(dbMessageMapper.getConnection(), dbBroadcast)) {
                                saveMessage(tipusMissatge, 1, resposta.getIdRespostaRapida());
                                String alta_llista="S'ha donat d'alta correctament a la llista "+resta_missatge.toUpperCase();
                                if (alta_llista.length() > 4000) {
                                    processaMultipleMissatges(alta_llista);
                                } else {
                                    missatgePerEnviar(alta_llista);
                                }                                
                                dbBroadcastMapper.insert(dbMessageMapper.getConnection(), dbBroadcast);
                            }else if (alta.contains(accio) && dbBroadcastMapper.exists(dbMessageMapper.getConnection(), dbBroadcast)) {
                                saveMessage(tipusMissatge, 1, resposta.getIdRespostaRapida());
                                String alta_llista="Usuari existent a la llista "+resta_missatge.toUpperCase();
                                if (alta_llista.length() > 4000) {
                                    processaMultipleMissatges(alta_llista);
                                } else {
                                    missatgePerEnviar(alta_llista);
                                }                                
                                dbBroadcastMapper.insert(dbMessageMapper.getConnection(), dbBroadcast);
                                
                            } else if (baixa.contains(accio)) {
                                saveMessage(tipusMissatge, 1, resposta.getIdRespostaRapida());
                                String alta_llista="S'ha donat de baixa correctament de la llista "+resta_missatge.toUpperCase();
                                if (alta_llista.length() > 4000) {
                                    processaMultipleMissatges(alta_llista);
                                } else {
                                    missatgePerEnviar(alta_llista);
                                }                                
                                envia = false;
                                dbBroadcastMapper.delete(dbMessageMapper.getConnection(), dbBroadcast);
                            }
                        } else if (broadcast && isEncrypted) {
                            int index = dbMessageMapper.getIdIdioma(application.getAccount().getCompanyId());
                            if (index > -1) {
                                result = Configuracio.noservei.get(index);
                            }
                        }
                        if (tipusMissatge != 4) {
                            tipusMissatge = 3;
                        }
                        if (envia) {
                            saveMessage(tipusMissatge, 1, resposta.getIdRespostaRapida());
                            if (result.length() > 4000) {
                                processaMultipleMissatges(result);
                            } else {
                                missatgePerEnviar(result);
                            }
                        }
                        MyLogger.log(application.getAccount().getPhoneNumber(), "Message is a keyword, storing answer in database: ");
                    }
                }

                result = null;
            } else {
                if (processaOrigen(false)) {
                    String miss = verificaHorari();
                    if (miss != null && !miss.equals("")) {
                        if (tipusMissatge == 4) {
                            saveMessage(5, 0, -2);
                        } else {
                            saveMessage(5, 0, -1);
                        }

                        checkTimetable(miss);
                    } else {
                        saveMessage(tipusMissatge, 0, -1);
                    }
                }
            }

            resposta = null;

        }
    }

    private void processaMultipleMissatges(String result) {
        int posInicial = 0;
        int posFinal = 0;
        int posTalla = 0;
        String missatge = "";
        String missatge2 = "";

        while (posInicial < result.length()) {
            posFinal = posInicial + 4000;
            boolean finalMiss = false;
            if (posFinal > result.length()) {
                posFinal = result.length();
                finalMiss = true;

            }

            missatge = result.substring(posInicial, posFinal);
            if (finalMiss) {
                posTalla = missatge.length();
            } else {
                posTalla = missatge.lastIndexOf("[");
                if (posTalla == 0) {
                    posTalla = missatge.length();
                }
            }
            missatge2 = missatge.substring(0, posTalla);
            posInicial = posInicial + posTalla;
            missatgePerEnviar(missatge2);
        }
    }

    private void missatgePerEnviar(String result) {

        if (isEncrypted) {
            dbMessage.setDataType(10);
        } else {
            dbMessage.setDataType(0);
        }
        dbMessage.setText(result);

        dbMessage.increaseTimestampWith();
        dbMessageMapper.storeMessageThatNeedsToBeSend(dbMessage);
    }

    private String verificaHorari() {
        return dbMessageMapper.checkTimetable(application.getAccount().getCompanyId());
    }

    private void checkTimetable(String result) {

        if (!result.equals("")) {
            if (isEncrypted) {
                dbMessage.setDataType(10);
            } else {
                dbMessage.setDataType(0);
            }
            dbMessage.setText(result);
            dbMessage.increaseTimestampWith();
            dbMessageMapper.storeMessageThatNeedsToBeSend(dbMessage);
            MyLogger.log(application.getAccount().getPhoneNumber(), "Service is not opened now, storing answer in database id: " + messageId);

        }
        result = null;
    }

    private void saveMessage(int tipusMissatge, int esProcessat, int idRespostaRapida) {
        if (isEncrypted && datatype == 0) {
            datatype = 10;
        }
        crearDBMessage(tipusMissatge, esProcessat, idRespostaRapida);
        MyLogger.log(application.getAccount().getPhoneNumber(), "New message from #" + dbMessage.getSender() + ": " + dbMessage.getText() + " id: " + messageId);
        dbMessageMapper.storeReceivedMessage(dbMessage);
        confirmMessage();

    }

    private void crearDBMessage(int tipusMissatge, int esProcessat, int idRespostaRapida) {
        //check nou telefon

        /* if (tipusMissatge != 4 && esProcessat == 1) {
         tipusMissatge = 3;
         } else if (tipusMissatge != 4) {
         tipusMissatge = 0;
         }*/
        dbMessage = new DbMessage(
                formatDate(),
                message,
                origen,
                timestamp,
                esProcessat,
                tipusMissatge,
                datatype,
                path,
                latitude,
                longitude,
                application.getAccount().getCompanyId(),
                application.getAccount().getPhoneNumber(), idRespostaRapida);
    }

    private String formatDate() {
        String dataText;
        df.setTimeZone(TimeZone.getTimeZone("Europe/Madrid"));
        dataText = df.format(new Date(timestamp * 1000));
        return dataText;
    }

    private String getInfoWebServices(String url, String parametre) {
        return parseResponseWS(peticioHTTP(url + parametre));
    }

    private String peticioHTTP(String url) {
        String str = "";
        HttpGet httpGet;
        HttpResponse rp;
        HttpClient hc;
        HttpParams httpParams;
        try {
            httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, timeoutWebService);
            HttpConnectionParams.setSoTimeout(httpParams, timeoutWebService);
            hc = new DefaultHttpClient(httpParams);
            httpGet = new HttpGet(url);
            rp = hc.execute(httpGet);
            if (rp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                str = EntityUtils.toString(rp.getEntity(), "UTF-8");
            }

        } catch (IOException ex) {
            Logger.getLogger(IncomingMessage.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            httpGet = null;
            hc = null;
            rp = null;
            httpParams = null;
        }
        return str;
    }

    private static String parseResponseWS(String xml) {
        Document doc = null;
        String text = "";
        if (xml != null && !xml.equals("")) {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            InputSource is;
            DocumentBuilder db;
            try {
                db = dbf.newDocumentBuilder();
                is = new InputSource();
                is.setCharacterStream(new StringReader(xml));
                doc = db.parse(is);
                doc.getDocumentElement().normalize();
                NodeList nList = doc.getElementsByTagName("result");

                for (int temp = 0; temp < nList.getLength(); temp++) {
                    Node nNode = nList.item(temp);
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        Element el = (Element) nNode;
                        if (el.hasAttribute("name")) {
                            text = text + el.getAttribute("name") + ": " + el.getTextContent() + "\n";
                        } else {
                            text = text + el.getTextContent() + "\n";
                        }
                    }
                }

            } catch (ParserConfigurationException ex) {
                Logger.getLogger(IncomingMessage.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SAXException ex) {
                Logger.getLogger(IncomingMessage.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(IncomingMessage.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                dbf = null;
                is = null;
                db = null;
                xml = null;
            }

        }
        doc = null;

        return text;
    }

    private void confirmMessage() {
        Thread thread = new Thread() {
            @Override
            public void run() {
                markAsReceived();
                markAsRead();
            }
        };
        thread.start();
    }

    private void markAsReceived() {
        try {
            application.getApi().doRpcCall(new TLRequestMessagesReceivedMessages(messageId));
        } catch (IOException e) {
            MyLogger.log(application.getAccount().getPhoneNumber(), "ERROR! in marking messages as received");
            MyLogger.log(application.getAccount().getPhoneNumber(), "Message: " + e.getMessage());
        }
    }

    private void markAsRead() {
        if (!isEncrypted) {
            try {
                TLAffectedHistory history = application.getApi().doRpcCall(
                        new TLRequestMessagesReadHistory(new TLInputPeerForeign(fromId, access_hash), messageId, 0));
                if (history != null) {
                    int offset2 = history.getOffset();
                    while (offset2 != 0) {
                        history = application.getApi().doRpcCall(
                                new TLRequestMessagesReadHistory(new TLInputPeerForeign(fromId, access_hash), messageId, offset2));
                        if (history != null) {
                            offset2 = history.getOffset();
                        }
                    }
                    application.getUpdatesController().onMessage((Object) history);
                }
            } catch (IOException ex) {
                MyLogger.log(application.getAccount().getPhoneNumber(), "Message marked as read errooooooooooooor");

                Logger.getLogger(IncomingMessage.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                application.getApi().doRpcCall(new TLRequestMessagesReadEncryptedHistory(new TLInputEncryptedChat(idChat, access_hash), messageId));
            } catch (IOException ex) {
                Logger.getLogger(IncomingMessage.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    private boolean downloadMedia(Object messageMedia) {
        boolean supported = false;
        if (messageMedia instanceof TLAbsMessageMedia) {
            TLAbsInputFileLocation location = null;
            if (messageMedia instanceof TLMessageMediaAudio) {
                /*if (((TLMessageMediaAudio) messageMedia).getAudio() instanceof TLAudio) {
                 TLAudio tlAudio = (TLAudio) ((TLMessageMediaAudio) messageMedia).getAudio();
                 fileExtension = "mp3";
                 datatype = 3;
                 message = "Àudio";
                 location = new TLInputAudioFileLocation(tlAudio.getId(), tlAudio.getAccessHash());
                 downloadMedia(tlAudio.getDcId(), location, tlAudio.getSize());
                 }*/
            } else if (messageMedia instanceof TLMessageMediaDocument) {
                /*if (((TLMessageMediaDocument) messageMedia).getDocument() instanceof TLDocument) {
                 TLDocument tlDocument = (TLDocument) ((TLMessageMediaDocument) messageMedia).getDocument();
                 fileExtension = FilenameUtils.getExtension(tlDocument.getFileName());
                 datatype = 5;
                 message = "Fitxer";
                 location = new TLInputDocumentFileLocation(tlDocument.getId(), tlDocument.getAccessHash());
                 downloadMedia(tlDocument.getDcId(), location, tlDocument.getSize());
                 }*/
            } else if (messageMedia instanceof TLMessageMediaVideo) {
                /*if (((TLMessageMediaVideo) messageMedia).getVideo() instanceof TLVideo) {
                 TLVideo tlVideo = (TLVideo) ((TLMessageMediaVideo) messageMedia).getVideo();
                 fileExtension = "mp4";
                 datatype = 2;
                 message = "Vídeo";
                 location = new TLInputVideoFileLocation(tlVideo.getId(), tlVideo.getAccessHash());
                 downloadMedia(tlVideo.getDcId(), location, tlVideo.getSize());
                 }*/
            } else if (messageMedia instanceof TLMessageMediaPhoto) {
                if (((TLMessageMediaPhoto) messageMedia).getPhoto() instanceof TLPhoto) {
                    TLPhoto tlPhoto = (TLPhoto) ((TLMessageMediaPhoto) messageMedia).getPhoto();
                    fileExtension = "jpeg";
                    datatype = 1;
                    message = "Imatge";
                    supported = true;
                    TLPhotoSize tlPhotoSize = getBiggestPhoto(tlPhoto.getSizes());
                    TLFileLocation fileLocation = (TLFileLocation) tlPhotoSize.getLocation();
                    location = new TLInputFileLocation(fileLocation.getVolumeId(), fileLocation.getLocalId(), fileLocation.getSecret());
                    downloadMedia(fileLocation.getDcId(), location, tlPhotoSize.getSize(), null, null);
                    tlPhotoSize = null;
                    fileLocation = null;
                    location = null;
                    tlPhoto = null;
                    messageMedia = null;

                }
            }

        } else if (messageMedia instanceof TLDecryptedMessage) {
            TLDecryptedMessage decryptedMessage = (TLDecryptedMessage) messageMedia;
            if (decryptedMessage.getMedia() instanceof TLDecryptedMessageMediaGeoPoint) {

            } else if (decryptedMessage.getMedia() instanceof TLDecryptedMessageMediaPhoto) {
                fileExtension = "jpeg";
                TLDecryptedMessageMediaPhoto photo = (TLDecryptedMessageMediaPhoto) decryptedMessage.getMedia();
                TLEncryptedFile file = (TLEncryptedFile) encMsg.getFile();
                TLAbsInputFileLocation location = new TLInputEncryptedFileLocation(file.getId(), file.getAccessHash());

                downloadMedia(file.getDcId(), location, photo.getSize(), photo.getIv(), photo.getKey());
                supported = true;

                datatype = 11;
                message = "Imatge";
                photo = null;
                file = null;
                location = null;
            } else if (decryptedMessage.getMedia() instanceof TLDecryptedMessageMediaVideo) {

            } else if (decryptedMessage.getMedia() instanceof TLDecryptedMessageMediaDocument) {

            } else if (decryptedMessage.getMedia() instanceof TLDecryptedMessageMediaAudio) {

            }
        }
        return supported;
    }

    private void downloadMedia(int dcId, TLAbsInputFileLocation location, int size, TLBytes iv, TLBytes key) {
        TLFile file = null;
        ByteArrayOutputStream byteOutput = new ByteArrayOutputStream();
        try {
            int offset = 0;
            int packetSize = 1024 * 1024;
            outputStream = new ByteArrayOutputStream();
            ;
            while (offset < size) {

                if (size - offset < packetSize) {
                    file = application.getApi().doGetFile(dcId, location, offset, size - offset);
                } else {
                    file = application.getApi().doGetFile(dcId, location, offset, packetSize);
                }
                byteOutput.write(file.getBytes().cleanData());
                if (isEncrypted) {
                    outputStream.write(CryptoUtils.AES256IGEDecrypt(byteOutput.toByteArray(), iv.cleanData(), key.cleanData()));
                } else {
                    outputStream = byteOutput;
                }

                offset += packetSize;
                if (!(file.getType() instanceof TLFilePartial)) {
                    TLAbsFileType fileType = file.getType();
                    if (fileType instanceof TLFileUnknown) {
                        fileExtension = "unknownFiletype";
                    } else if (fileType instanceof TLFileJpeg) {
                        fileExtension = "jpeg";
                    } else if (fileType instanceof TLFileGif) {
                        fileExtension = "gif";
                    } else if (fileType instanceof TLFilePng) {
                        fileExtension = "png";
                    } else if (fileType instanceof TLFileMp3) {
                        fileExtension = "mp3";
                    } else if (fileType instanceof TLFileMov) {
                        fileExtension = "mov";
                    } else if (fileType instanceof TLFileMp4) {
                        fileExtension = "mp4";
                    } else if (fileType instanceof TLFileWebp) {
                        fileExtension = "webp";
                    } else if (fileType instanceof TLFilePdf) {
                        fileExtension = "pdf";
                    }
                }
            }
        } catch (RpcException e) {
            MyLogger.log(application.getAccount().getPhoneNumber(), "ERROR! in download media");
            MyLogger.log(application.getAccount().getPhoneNumber(), "Errorcode: " + e.getErrorCode());
            MyLogger.log(application.getAccount().getPhoneNumber(), "Message: " + e.getErrorTag());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            file = null;
            try {
                if (byteOutput != null) {
                    byteOutput.close();
                }

            } catch (IOException ex) {
                Logger.getLogger(IncomingMessage.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                byteOutput = null;
            }

        }
    }

    private boolean writeFile() {
        synchronized (application) {
            boolean ok = true;
            MyLogger.log(application.getAccount().getPhoneNumber(), "EntraaaaaaaaaaaaFileeeeeee " + String.valueOf(messageId));
            byte[] data = outputStream.toByteArray();
            int counter = 0;

            path = Configuracio.pathFolder + application.getAccount().getPhoneNumber() + "/" + origen + "/" + timestamp + "-" + counter + "." + fileExtension;

            File file = new File(path);
            while (file.exists()) {
                counter++;

                path = Configuracio.pathFolder + application.getAccount().getPhoneNumber() + "/" + origen + "/" + timestamp + "-" + counter + "." + fileExtension;

                file = new File(path);
            }

            MyLogger.log(application.getAccount().getPhoneNumber(), path);
            try {
                FileUtils.writeByteArrayToFile(file, data);
            } catch (IOException e) {
                ok = false;
                MyLogger.log(application.getAccount().getPhoneNumber(), "ERROR! in writing file");
                MyLogger.log(application.getAccount().getPhoneNumber(), "IOException, message: " + e.getMessage());
            } finally {
                try {
                    MyLogger.log(application.getAccount().getPhoneNumber(), "SuuuuuurtFileeeeeeee " + String.valueOf(messageId));

                    file = null;
                    data = null;
                    if (outputStream != null) {
                        outputStream.close();
                        outputStream = null;
                    }

                } catch (IOException ex) {
                    Logger.getLogger(IncomingMessage.class.getName()).log(Level.SEVERE, null, ex);
                }

            }

            return ok;
        }

    }

    private TLPhotoSize getBiggestPhoto(TLVector<TLAbsPhotoSize> tlAbsPhotoSizes) {
        TLPhotoSize biggestPhotoSize = null;
        for (TLAbsPhotoSize tlAbsPhotoSize : tlAbsPhotoSizes) {
            if (tlAbsPhotoSize instanceof TLPhotoSize) {
                TLPhotoSize tlPhotoSize = (TLPhotoSize) tlAbsPhotoSize;
                if (biggestPhotoSize == null) {
                    biggestPhotoSize = tlPhotoSize;
                } else {
                    if (biggestPhotoSize.getSize() < tlPhotoSize.getSize()) {
                        biggestPhotoSize = tlPhotoSize;
                    }
                }
            }
        }
        return biggestPhotoSize;
    }

}
