package cat.tecnocampus.telegramservice.process;

import cat.tecnocampus.telegramservice.TelegramService;
import cat.tecnocampus.telegramservice.util.Configuracio;
import cat.tecnocampus.telegramservice.util.MySocketException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import org.telegram.api.engine.RpcException;

public class ClientConnection implements Runnable {

    private static final int timeout = 10 * 1000;
    private TelegramService service;
    private Socket socket;
    private BufferedReader socketIn;
    private PrintWriter socketOut;

    private String command;
    private String commandCode;
    private String phoneNumber;

    public ClientConnection(TelegramService service, Socket socket) {
        this.service = service;
        this.socket = socket;
    }

    @Override
    public void run() {
        try {
            socket.setSoTimeout(timeout);
            socketIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            socketOut = new PrintWriter(socket.getOutputStream(), true);

            while ((command = socketIn.readLine()) != null) {
                if (command != null && command.contains(Configuracio.tagFinal)) {
                    break;
                }
            }
            System.out.println("New command: " + command);

            commandCode = getCommandPart(0);
            phoneNumber = getCommandPart(1);
            switch (commandCode) {
                case "0":
                    requestActivationCode();
                    break;
                case "1":
                    createAccount();
                    break;
                case "2":
                    login();
                    break;
                case "4":
                    updateImage();
                    break;
                case "5":
                    logout();
                    break;
                default:
                    throw new MySocketException(phoneNumber + Configuracio.tagSepara + "0<separa>Error<separa>Invalid command<separa></finalitzacio>");
            }
        } catch (MySocketException e) {
            sendResponse(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }

    }

    private void requestActivationCode() throws MySocketException {
        service.addNewPhoneNumber(phoneNumber);
        try {
            service.getApplicationThread(phoneNumber).requestActivationCode();
        } catch (MySocketException e) {
            service.removePhoneNumber(phoneNumber);
            throw e;
        }
        sendResponse(phoneNumber + Configuracio.tagSepara + "1<separa>Sent<separa></finalitzacio>");
    }

    private void createAccount() throws MySocketException {
        String code = getCommandPart(2);
        int companyId = -1;
        try {
            companyId = Integer.parseInt(getCommandPart(3));
        } catch (NumberFormatException ex) {
            throw new MySocketException(phoneNumber + Configuracio.tagSepara + "0<separa>Error<separa>Company id is not a number<separa></finalitzacio>");
        }
        String descripcio = getCommandPart(4);
        try {
            service.getApplicationThread(phoneNumber).createAccount(code, companyId, descripcio);
        } catch (RpcException e) {
            service.removePhoneNumber(phoneNumber);
            throw new MySocketException(phoneNumber + Configuracio.tagSepara + "0<separa>Error<separa>Code expired, request new activation code<separa></finalitzacio>");
        }
        service.removeApplicationThread(phoneNumber, true);
        sendResponse(phoneNumber + Configuracio.tagSepara + "2<separa>created account<separa></finalitzacio>");
    }

    private void login() throws MySocketException {
        service.login(phoneNumber);
        sendResponse(phoneNumber + Configuracio.tagSepara + "2<separa>Login<separa></finalitzacio>");
    }

    private void updateImage() throws MySocketException {
        String imagePath = getCommandPart(2);
        service.getApplicationThread(phoneNumber).updateImage(imagePath);
        sendResponse(phoneNumber + Configuracio.tagSepara + "2<separa>Updated<separa></finalitzacio>");
    }

    private void logout() throws MySocketException {
        service.logout(phoneNumber);
        sendResponse(phoneNumber + Configuracio.tagSepara + "2<separa>Logout<separa></finalitzacio>");
    }

    private void sendResponse(String response) {
        System.out.println(response);
        socketOut.println(response);
    }

    private String getCommandPart(int part) throws MySocketException {
        String commandPart = "";
        try {
            commandPart = command.split(Configuracio.tagSepara)[part];
        } catch (Exception e) {
            throw new MySocketException(phoneNumber + Configuracio.tagSepara + "0<separa>Error<separa>Invalid command<separa></finalitzacio>");
        }
        return commandPart;
    }

    private void closeConnection() {
        try {
            if (socketOut != null) {
                socketOut.close();
                socketOut = null;
            }
            if (socketIn != null) {
                socketIn.close();
                socketIn = null;
            }
            if (socket != null) {
                socket.close();
                socket = null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
