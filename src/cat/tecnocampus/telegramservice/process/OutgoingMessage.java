package cat.tecnocampus.telegramservice.process;

import cat.tecnocampus.telegramservice.persistence.DbMessage;
import cat.tecnocampus.telegramservice.persistence.DbMessageMapper;
import cat.tecnocampus.telegramservice.persistence.EncryptedChat;
import cat.tecnocampus.telegramservice.persistence.EncryptedChatMapper;
import cat.tecnocampus.telegramservice.thread.ApplicationThread;
import cat.tecnocampus.telegramservice.util.MimeTypeMap;
import cat.tecnocampus.telegramservice.util.MyLogger;
import cat.tecnocampus.telegramservice.util.UploadResult;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FilenameUtils;
import org.telegram.api.TLAbsDecryptedMessage;
import org.telegram.api.TLAbsInputFile;
import org.telegram.api.TLAbsInputMedia;
import org.telegram.api.TLDecryptedMessage;
import org.telegram.api.TLDecryptedMessageMediaEmpty;
import org.telegram.api.TLInputEncryptedChat;
import org.telegram.api.TLInputFile;
import org.telegram.api.TLInputGeoPoint;
import org.telegram.api.TLInputMediaGeoPoint;
import org.telegram.api.TLInputMediaUploadedDocument;
import org.telegram.api.TLInputPeerForeign;
import org.telegram.api.engine.RpcCallbackEx;
import org.telegram.api.messages.TLAbsSentEncryptedMessage;
import org.telegram.api.messages.TLAbsSentMessage;
import org.telegram.api.messages.TLAbsStatedMessage;
import org.telegram.api.requests.TLRequestMessagesSendEncrypted;
import org.telegram.api.requests.TLRequestMessagesSendMedia;
import org.telegram.api.requests.TLRequestMessagesSendMessage;
import org.telegram.mtproto.secure.CryptoUtils;
import static org.telegram.mtproto.secure.CryptoUtils.SHA1;
import static org.telegram.mtproto.secure.CryptoUtils.concat;
import static org.telegram.mtproto.secure.CryptoUtils.substring;
import org.telegram.mtproto.secure.Entropy;
import org.telegram.tl.StreamingUtils;
import org.telegram.tl.TLBytes;
import org.telegram.tl.TLMethod;

public class OutgoingMessage {

    private static final Random r = new Random();
    private static final int TIMEOUT = 5 * 1000;
    private final ApplicationThread application;
    private DbMessageMapper dbMessageMapper;

    private final DbMessage dbMessage;
    private Thread thread;
    private final Connection connectionBBDD;

    public OutgoingMessage(Connection connection, ApplicationThread application, DbMessage dbMessage) {
        this.application = application;
        this.dbMessage = dbMessage;
        connectionBBDD = connection;
    }

    public void send() {

        dbMessageMapper = new DbMessageMapper();

        sendMessage();
    }

    private void sendMessage() {
        MyLogger.log(application.getAccount().getPhoneNumber(), "Sending message with database id " + dbMessage.getDbId() + " to #" + Integer.parseInt(dbMessage.getSender()) + ": " + dbMessage.getText() + " -> Updating database");

        dbMessageMapper.updateMessage(connectionBBDD, dbMessage, true);
        if (dbMessage.getText()!=null && !dbMessage.getText().equals("") && dbMessage.getText().length()>3) {

            boolean isEncrypted = false;
            if (dbMessage.getDatatype() > 9) {
                isEncrypted = true;
                dbMessage.setDataType(dbMessage.getDatatype() - 10);
            }

            if (dbMessage.getDatatype() == 0) {
                sendTextMessage(isEncrypted);
            } else if (dbMessage.getDatatype() == 4) {
                sendLocationMessage();
            } else {
                sendMediaMessage();
            }
        }

    }

    private void sendTextMessage(boolean encrypted) {
        int uid = Integer.parseInt(dbMessage.getSender());
        String text = dbMessage.getText();
        try {
            text = new String(text.getBytes("UTF-8"), "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(OutgoingMessage.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (text.length() > 4000) {
            text = text.substring(0, 3999);
        }
        final SalvaMissatgeEnviat salva = new SalvaMissatgeEnviat(dbMessageMapper, dbMessage);
        final UpdateSeq updateSeq = new UpdateSeq(false, encrypted);
        if (encrypted) {
            int idRandom = r.nextInt();
            EncryptedChatMapper encryptedChatMapper = new EncryptedChatMapper();
            encryptedChatMapper.crearConnexio();
            EncryptedChat chat = encryptedChatMapper.getEncryptedChatPerUid(uid, application.getAccount().getAccountId());
            encryptedChatMapper.tancarConnexio();
            TLDecryptedMessage decryptedMessage = new TLDecryptedMessage();
            decryptedMessage.setRandomId(idRandom);
            decryptedMessage.setRandomBytes(new TLBytes(Entropy.generateSeed(32)));
            decryptedMessage.setMessage(text);
            decryptedMessage.setMedia(new TLDecryptedMessageMediaEmpty());
            byte[] bundle;
            try {
                bundle = encryptMessage(decryptedMessage, chat);
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
            TLRequestMessagesSendEncrypted request = new TLRequestMessagesSendEncrypted(new TLInputEncryptedChat(chat.getIdChat(), chat.getAccessHash()), idRandom,
                    new TLBytes(bundle));
            application.getApi().doRpcCall(request, TIMEOUT,
                    new RpcCallbackEx<TLAbsSentEncryptedMessage>() {
                        @Override
                        public void onConfirmed() {
                        }

                        @Override
                        public void onResult(TLAbsSentEncryptedMessage result) {
                            MyLogger.log(application.getAccount().getPhoneNumber(), "Message with database id " + dbMessage.getDbId());
                            updateSeq.setTLAbsSentEncryptedMessage(result);
                            thread = new Thread(updateSeq);
                            thread.start();
                        }

                        @Override
                        public void onError(int errorCode, String message) {
                            thread = new Thread(salva);
                            thread.start();
                            if (errorCode == 420) {
                                int temps = Integer.parseInt(message.split("_")[2]);
                                application.getFloodController().setWait(temps);
                            }
                            MyLogger.log(application.getAccount().getPhoneNumber(), "ERROR! in sending message with database id " + OutgoingMessage.this.dbMessage.getDbId() + " -> resetting database esProcessat status");
                            MyLogger.log(application.getAccount().getPhoneNumber(), "Errorcode: " + errorCode);
                            MyLogger.log(application.getAccount().getPhoneNumber(), "Message: " + message);
                        }
                    });
        } else {
            TLMethod request = new TLRequestMessagesSendMessage(new TLInputPeerForeign(uid, dbMessage.getAccess_hash()), text, r.nextInt());
            application.getApi().doRpcCall(request, TIMEOUT,
                    new RpcCallbackEx<TLAbsSentMessage>() {
                        @Override
                        public void onConfirmed() {
                        }

                        @Override
                        public void onResult(TLAbsSentMessage result) {
                            MyLogger.log(application.getAccount().getPhoneNumber(), "Message with database id " + dbMessage.getDbId() + " successfully send " + result.getId());
                            updateSeq.setTLAbsSentMessage(result);
                            thread = new Thread(updateSeq);
                            thread.start();
                        }

                        @Override
                        public void onError(int errorCode, String message) {
                            thread = new Thread(salva);
                            thread.start();
                            if (errorCode == 420) {
                                int temps = Integer.parseInt(message.split("_")[2]);
                                application.getFloodController().setWait(temps);
                            }
                            MyLogger.log(application.getAccount().getPhoneNumber(), "ERROR! in sending message with database id " + OutgoingMessage.this.dbMessage.getDbId() + " -> resetting database esProcessat status");
                            MyLogger.log(application.getAccount().getPhoneNumber(), "Errorcode: " + errorCode);
                            MyLogger.log(application.getAccount().getPhoneNumber(), "Message: " + message);
                        }
                    });
        }
        text = null;
    }

    private void sendMediaMessage() {
        try {
            long fileId = r.nextLong();
            Path path = Paths.get(dbMessage.getPath());
            byte[] data = Files.readAllBytes(path);
            String fileName = path.getFileName().toString();
            UploadResult res = application.getUploadController().uploadFile(new ByteArrayInputStream(data), data.length, fileId);
            TLAbsInputFile file = new TLInputFile(fileId, res.getPartsCount(), fileName, res.getHash());
            String fileExtension = FilenameUtils.getExtension(fileName);
            String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension);
            TLAbsInputMedia media = new TLInputMediaUploadedDocument(file, fileName, mimeType);
            sendMediaMessage(media, false);
        } catch (IOException e) {
            MyLogger.log(application.getAccount().getPhoneNumber(), "ERROR! in sending message with database id " + OutgoingMessage.this.dbMessage.getDbId() + " -> resetting database esProcessat status");
            MyLogger.log(application.getAccount().getPhoneNumber(), "IOException, message: " + e.getMessage());
        }
    }

    public byte[] encryptMessage(TLAbsDecryptedMessage message, EncryptedChat chat) throws IOException {

        if (chat == null) {
            return null;
        }
        byte[] rawMessage = message.serialize();
        byte[] msgWithLen = CryptoUtils.concat(StreamingUtils.intToBytes(rawMessage.length), rawMessage);
        byte[] allMessage = CryptoUtils.align(msgWithLen, 16);
        byte[] msgKey = substring(CryptoUtils.SHA1(msgWithLen), 4, 16);
        byte[] key = chat.getKey();
        byte[] fingerprint = substring(SHA1(key), 12, 8);
        byte[] sha1_a = SHA1(msgKey, substring(key, 0, 32));
        byte[] sha1_b = SHA1(substring(key, 32, 16), msgKey, substring(key, 48, 16));
        byte[] sha1_c = SHA1(substring(key, 64, 32), msgKey);
        byte[] sha1_d = SHA1(msgKey, substring(key, 96, 32));
        byte[] aesKey = concat(substring(sha1_a, 0, 8), substring(sha1_b, 8, 12), substring(sha1_c, 4, 12));
        byte[] aesIv = concat(substring(sha1_a, 8, 12), substring(sha1_b, 0, 8), substring(sha1_c, 16, 4), substring(sha1_d, 0, 8));
        byte[] encrypted = CryptoUtils.AES256IGEEncrypt(allMessage, aesIv, aesKey);
        return concat(fingerprint, msgKey, encrypted);
    }

    private void sendLocationMessage() {
        TLAbsInputMedia media = new TLInputMediaGeoPoint(new TLInputGeoPoint(dbMessage.getLatitude(), dbMessage.getLongitude()));
        sendMediaMessage(media, false);
    }

    private void sendMediaMessage(TLAbsInputMedia media, boolean isEncrypted) {
        int uid = Integer.parseInt(dbMessage.getSender());
        final SalvaMissatgeEnviat salva = new SalvaMissatgeEnviat(dbMessageMapper, dbMessage);
        final UpdateSeq updateSeq = new UpdateSeq(true, isEncrypted);
        application.getApi().doRpcCall(
                new TLRequestMessagesSendMedia(new TLInputPeerForeign(uid, dbMessage.getAccess_hash()), media, r.nextInt()), TIMEOUT,
                new RpcCallbackEx<TLAbsStatedMessage>() {
                    @Override
                    public void onConfirmed() {

                    }

                    @Override
                    public void onResult(TLAbsStatedMessage result) {
                        updateSeq.setTLAbsStatedMessage(result);
                        thread = new Thread(updateSeq);
                        thread.start();
                        MyLogger.log(application.getAccount().getPhoneNumber(), "Message with database id " + dbMessage.getDbId() + " successfully sent");
                    }

                    @Override
                    public void onError(int errorCode, String message) {
                        thread = new Thread(salva);
                        thread.start();
                        if (errorCode == 420) {
                            int temps = Integer.parseInt(message.split("_")[2]);
                            application.getFloodController().setWait(temps);
                        }
                        MyLogger.log(application.getAccount().getPhoneNumber(), "ERROR! in sending message with database id " + OutgoingMessage.this.dbMessage.getDbId() + " -> resetting database respostesrapides status");
                        MyLogger.log(application.getAccount().getPhoneNumber(), "Errorcode: " + errorCode);
                        MyLogger.log(application.getAccount().getPhoneNumber(), "Message: " + message);
                    }
                });
    }

    class UpdateSeq implements Runnable {

        private boolean isMedia;
        private boolean isEncrypted;
        private TLAbsSentMessage tlAbsSentMessage;
        private TLAbsStatedMessage tlAbsStatedMesssage;
        private TLAbsSentEncryptedMessage tlAbsSentEncryptedMessage;

        public void setTLAbsSentMessage(TLAbsSentMessage result) {
            tlAbsSentMessage = result;
        }

        public void setTLAbsStatedMessage(TLAbsStatedMessage result) {
            tlAbsStatedMesssage = result;
        }

        public void setTLAbsSentEncryptedMessage(TLAbsSentEncryptedMessage result) {
            tlAbsSentEncryptedMessage = result;
        }

        public UpdateSeq(boolean media, boolean isEncrypted) {
            isMedia = media;
            this.isEncrypted = isEncrypted;
        }

        @Override
        public void run() {
            if (!isEncrypted) {
                if (isMedia) {
                    application.getUpdatesController().onMessage((Object) tlAbsStatedMesssage);
                } else {
                    application.getUpdatesController().onMessage((Object) tlAbsSentMessage);
                }
            } else {
                application.getUpdatesController().onMessage((Object) tlAbsSentEncryptedMessage);
            }
        }

    }

    class SalvaMissatgeEnviat implements Runnable {

        private DbMessageMapper dbMessageMapper;
        private DbMessage dbMessage;

        public SalvaMissatgeEnviat(DbMessageMapper dbMessageMapper, DbMessage dbMessage) {
            this.dbMessage = dbMessage;
            this.dbMessageMapper = dbMessageMapper;

        }

        @Override
        public void run() {

            dbMessageMapper.crearConnexio();
            dbMessageMapper.updateMessage(dbMessageMapper.getConnection(), dbMessage, false);
            dbMessageMapper.tancarConnexio();
            dbMessageMapper = null;
            dbMessage = null;

        }

    }

}
