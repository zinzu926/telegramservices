package cat.tecnocampus.telegramservice.apistate;

import cat.tecnocampus.telegramservice.persistence.ApiStorageMapper;
import cat.tecnocampus.telegramservice.persistence.AuthInfoMapper;
import cat.tecnocampus.telegramservice.persistence.DcInfoMapper;
import cat.tecnocampus.telegramservice.thread.ApplicationThread;
import cat.tecnocampus.telegramservice.util.DcInitialConfig;
import cat.tecnocampus.telegramservice.util.MyLogger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.telegram.api.TLConfig;
import org.telegram.api.TLDcOption;
import org.telegram.api.engine.storage.AbsApiState;
import org.telegram.mtproto.state.AbsMTProtoState;
import org.telegram.mtproto.state.ConnectionInfo;
import org.telegram.mtproto.state.KnownSalt;

public class ApiState implements AbsApiState {

    private ApplicationThread application;
    private ApiStorage storage;
    private ApiStorageMapper apiStorageMapper;
    private AuthInfoMapper authinfoMapper;
    private DcInfoMapper dcInfoMapper;

    public ApiState(ApplicationThread application) {
        this.application = application;
        this.apiStorageMapper = new ApiStorageMapper();
        authinfoMapper = new AuthInfoMapper();
        dcInfoMapper = new DcInfoMapper();

        this.storage = apiStorageMapper.getStorage(application.getAccount().getAccountId());

        if (storage.getDcInfos().isEmpty()) {
            storage.getDcInfos().add(new DcInfo(1, DcInitialConfig.ADDRESS, DcInitialConfig.PORT, 0));
        }
    }

    public List<Integer> getKnownDc() {
        return new DcInfoMapper().getDcId(storage.getId());
    }

    @Override
    public synchronized int getPrimaryDc() {
        return storage.getPrimaryDc();
    }

    @Override
    public synchronized void setPrimaryDc(int dc) {
        MyLogger.log(application.getAccount().getPhoneNumber(), "setPrimaryDc dc #" + dc);
        storage.setPrimaryDc(dc);
        updateSetPrimaryDC(dc);



    }

    public synchronized boolean isAuthenticated() {
        boolean isAuth = isAuthenticated(storage.getPrimaryDc());
        return isAuth;
    }

    @Override
    public synchronized boolean isAuthenticated(int dcId) {
        AuthInfo authInfo = findAuthInfo(dcId);
        return (authInfo != null && authInfo.isAuthorised());
    }

    private synchronized void insertApiStorage() {
        apiStorageMapper.crearConnexio();
        if (!apiStorageMapper.existeix(application.getAccount().getAccountId())) {
            long id = apiStorageMapper.insert(application.getAccount().getAccountId(), storage.getPrimaryDc());
            storage.setId(id);
        }
        apiStorageMapper.tancarConnexio();

    }

    public synchronized void setAuthenticated(boolean auth) {
        AuthInfo authInfo = findAuthInfo(storage.getPrimaryDc());
        authInfo.setAuthorised(auth);
        authinfoMapper.updateAuthenticated(authInfo.getId(), auth);
        
    }

    @Override
    public synchronized void setAuthenticated(int dcId, boolean auth) {
        AuthInfo authInfo = findAuthInfo(dcId);
        authInfo.setAuthorised(auth);
        authinfoMapper.updateAuthenticated(authInfo.getId(), auth);
        // saveChanges();
    }

    @Override
    public synchronized void updateSettings(TLConfig config) {
        int version = 0;
        for (DcInfo info : storage.getDcInfos()) {
            version = Math.max(version, info.getVersion());
        }

        boolean hasUpdates = false;
        for (TLDcOption option : config.getDcOptions()) {
            boolean contains = false;
            for (DcInfo info : storage.getDcInfos().toArray(new DcInfo[0])) {
                if (info.getAddress().equals(option.getIpAddress()) && info.getPort() == option.getPort() && info.getDcId() == option.getId() && info.getVersion() == version) {
                    contains = true;
                    break;
                }
            }

            if (!contains) {
                hasUpdates = true;
            }
        }

        if (!hasUpdates) {
            MyLogger.log(application.getAccount().getPhoneNumber(), "No updates for DC");
            return;
        }

        int nextVersion = version + 1;
        dcInfoMapper.crearConnexio();

        List<DcInfo> listDCInfo = new ArrayList();
        for (TLDcOption option : config.getDcOptions()) {
            boolean trobat = false;
            for (DcInfo info : storage.getDcInfos().toArray(new DcInfo[0])) {
                if (info.getAddress().equals(option.getIpAddress()) && info.getDcId() == option.getId()) {
                    trobat = true;
                    storage.getDcInfos().remove(info);
                    dcInfoMapper.updateDCInfo(storage.getId(), info.getDcId(), info.getAddress(), option.getPort(), nextVersion);
                }
            }
            DcInfo dcInfo = new DcInfo(option.getId(), option.getIpAddress(), option.getPort(), nextVersion);
            if (!trobat) {
                listDCInfo.add(dcInfo);
            }
            storage.getDcInfos().add(dcInfo);

        }
        if (!listDCInfo.isEmpty()) {
            dcInfoMapper.insertAll(storage.getId(), listDCInfo);
        }
        dcInfoMapper.tancarConnexio();
    }

    @Override
    public synchronized byte[] getAuthKey(int dcId) {
        AuthInfo authInfo = findAuthInfo(dcId);
        return authInfo != null ? authInfo.getAuthKey() : null;
    }

    @Override
    public synchronized void putAuthKey(int dcId, byte[] key) {
        AuthInfo authInfo = findAuthInfo(dcId);
        if (authInfo != null) {
            return;
        }
        insertApiStorage();
        AuthInfo authInf = new AuthInfo(dcId, key);
        authInf.setId(authinfoMapper.insert(storage.getId(), authInf));
        storage.getAuthInfos().add(authInf);
        
    }

    @Override
    public ConnectionInfo[] getAvailableConnections(int dcId) {
        ArrayList<DcInfo> infos = new ArrayList<DcInfo>();
        int maxVersion = 0;
        for (DcInfo info : storage.getDcInfos()) {
            if (info.getDcId() == dcId) {
                infos.add(info);
                maxVersion = Math.max(maxVersion, info.getVersion());
            }
        }

        ArrayList<ConnectionInfo> res = new ArrayList<ConnectionInfo>();

        // Maximum version addresses
        HashMap<String, DcAddress> mainAddresses = new HashMap<String, DcAddress>();
        for (DcInfo i : infos) {
            if (i.getVersion() != maxVersion) {
                continue;
            }

            if (mainAddresses.containsKey(i.getAddress())) {
                mainAddresses.get(i.getAddress()).ports.put(i.getPort(), 1);
            } else {
                DcAddress address = new DcAddress();
                address.ports.put(i.getPort(), 1);
                address.host = i.getAddress();
                mainAddresses.put(i.getAddress(), address);
            }
        }

        for (DcAddress address : mainAddresses.values()) {
            address.ports.put(443, 2);
            address.ports.put(80, 1);
            address.ports.put(25, 0);
        }

        HashMap<Integer, HashMap<String, DcAddress>> otherAddresses = new HashMap<Integer, HashMap<String, DcAddress>>();

        for (DcInfo i : infos) {
            if (i.getVersion() == maxVersion) {
                continue;
            }

            if (!otherAddresses.containsKey(i.getVersion())) {
                otherAddresses.put(i.getVersion(), new HashMap<String, DcAddress>());
            }

            HashMap<String, DcAddress> addressHashMap = otherAddresses.get(i.getVersion());

            if (addressHashMap.containsKey(i.getAddress())) {
                addressHashMap.get(i.getAddress()).ports.put(i.getPort(), 1);
            } else {
                DcAddress address = new DcAddress();
                address.ports.put(i.getPort(), 1);
                address.host = i.getAddress();
                addressHashMap.put(i.getAddress(), address);
            }
        }

        for (Integer version : otherAddresses.keySet()) {
            for (DcAddress address : otherAddresses.get(version).values()) {
                if (mainAddresses.containsKey(address.host)) {
                    continue;
                }
                address.ports.put(443, 2);
                address.ports.put(80, 1);
                address.ports.put(25, 0);
            }
        }

        // Writing main addresses
        int index = 0;

        for (DcAddress address : mainAddresses.values()) {
            for (Integer port : address.ports.keySet()) {
                int priority = maxVersion + address.ports.get(port);
                res.add(new ConnectionInfo(index++, priority, address.host, port));
            }
        }

        // Writing other addresses
        for (Integer version : otherAddresses.keySet()) {
            for (DcAddress address : otherAddresses.get(version).values()) {
                for (Integer port : address.ports.keySet()) {
                    int priority = version + address.ports.get(port);
                    res.add(new ConnectionInfo(index++, priority, address.host, port));
                }
            }
        }

        MyLogger.log(application.getAccount().getPhoneNumber(), "Created connections for dc #" + dcId);
        for (ConnectionInfo c : res) {
            MyLogger.log(application.getAccount().getPhoneNumber(), "Connection: #" + c.getId() + " " + c.getAddress() + ":" + c.getPort() + " at " + c.getPriority());
        }

        return res.toArray(new ConnectionInfo[0]);
    }

    private synchronized void writeKnownSalts(int dcId, KnownSalt[] salts) {
        AuthInfo authInfo = findAuthInfo(dcId);
        authInfo.getSalts().clear();
        for (int i = 0; i < salts.length; i++) {
            if (salts[i].getValidUntil() >= (System.currentTimeMillis() / 1000L)) {
                authInfo.getSalts().add(new LastKnownSalt(salts[i].getValidSince(), salts[i].getValidUntil(), salts[i].getSalt()));
            }
        }
        authinfoMapper.updateSalts(authInfo);
        
    }

    private synchronized KnownSalt[] readKnownSalts(int dcId) {
        AuthInfo authInfo = findAuthInfo(dcId);
        KnownSalt[] salts = new KnownSalt[authInfo.getSalts().size()];
        for (int i = 0; i < salts.length; i++) {
            LastKnownSalt sourceSalt = authInfo.getSalts().get(i);
            salts[i] = new KnownSalt(sourceSalt.getValidSince(), sourceSalt.getValidUntil(), sourceSalt.getSalt());
        }
        return salts;
    }

    @Override
    public synchronized AbsMTProtoState getMtProtoState(final int dcId) {
        return new AbsMTProtoState() {
            private KnownSalt[] knownSalts = null;

            @Override
            public byte[] getAuthKey() {
                return ApiState.this.getAuthKey(dcId);
            }

            @Override
            public ConnectionInfo[] getAvailableConnections() {
                return ApiState.this.getAvailableConnections(dcId);
            }

            @Override
            public KnownSalt[] readKnownSalts() {
                if (knownSalts == null) {
                    knownSalts = ApiState.this.readKnownSalts(dcId);
                }
                return knownSalts;
            }

            @Override
            protected void writeKnownSalts(KnownSalt[] salts) {
                ApiState.this.writeKnownSalts(dcId, salts);
                knownSalts = null;
            }
        };
    }

    @Override
    public synchronized void resetAuth() {
        MyLogger.log(application.getAccount().getPhoneNumber(), "resetAuth");

        for (AuthInfo authInfo : storage.getAuthInfos()) {
            authInfo.setAuthorised(false);

        }
        authinfoMapper.resetAuthenticated(storage.getId());
    
    }

    @Override
    public synchronized void reset() {
        MyLogger.log(application.getAccount().getPhoneNumber(), "reseeeeet");
        authinfoMapper.resetAuthenticated(storage.getId());
        storage.getAuthInfos().clear();
        authinfoMapper.deleteAuthInfo(storage, false);

    }
    
    public void logout(){
        apiStorageMapper.delete(storage.getId());
        authinfoMapper.deleteAuthInfo(storage, true);
        dcInfoMapper.deleteDCInfo(storage.getId());
    }

    private AuthInfo findAuthInfo(int dc) {
        for (AuthInfo authInfo : storage.getAuthInfos()) {
            if (authInfo.getDcId() == dc) {
                return authInfo;
            }
        }
        return null;
    }

    private void updateSetPrimaryDC(int dc) {
        apiStorageMapper.setPrimaryDc(application.getAccount().getAccountId(), dc);
    }


    private class DcAddress {

        public String host;
        public HashMap<Integer, Integer> ports = new HashMap<Integer, Integer>();
    }
}
