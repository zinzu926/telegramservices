package cat.tecnocampus.telegramservice.apistate;

public class DcInfo {

    private int dcId;
    private String address;
    private int port;
    private int version;

    public DcInfo(int dcId, String address, int port, int version) {
        this.dcId = dcId;
        this.address = address;
        this.port = port;
        this.version = version;
    }

    public DcInfo() {

    }

    public int getDcId() {
        return dcId;
    }

    public String getAddress() {
        return address;
    }

    public int getPort() {
        return port;
    }

    public int getVersion() {
        return version;
    }
    
}