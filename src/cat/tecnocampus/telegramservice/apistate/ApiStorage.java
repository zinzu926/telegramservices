package cat.tecnocampus.telegramservice.apistate;

import java.util.ArrayList;
import java.util.List;

public class ApiStorage {

    private List<AuthInfo> authInfos;
    private List<DcInfo> dcInfos;
    private int primaryDc;
    private long id;
    
    public ApiStorage() {
    	authInfos = new ArrayList<AuthInfo>();
        dcInfos = new ArrayList<DcInfo>();
        primaryDc = 1;
    }

    public List<AuthInfo> getAuthInfos() {
        return authInfos;
    }
    
    public void setAuthInfos(List<AuthInfo> authInfos) {
    	this.authInfos = authInfos;
    }

         public List<DcInfo> getDcInfos() {
        return dcInfos;
    }
    
    public void setDcInfos(List<DcInfo> dcInfos) {
    	this.dcInfos = dcInfos;
    }

    public int getPrimaryDc() {
        return primaryDc;
    }

    public void setPrimaryDc(int primaryDc) {
        this.primaryDc = primaryDc;
    }
    
    public long getId(){
        return id;
    }
    
    public void setId(long id){
        this.id=id;
    }
    
}