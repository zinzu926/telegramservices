package cat.tecnocampus.telegramservice.apistate;

public class OldSession {
	
    private byte[] session;

    public OldSession(byte[] session) {
        this.session = session;
    }

    public OldSession() {

    }

    public byte[] getSession() {
        return session;
    }
    
}