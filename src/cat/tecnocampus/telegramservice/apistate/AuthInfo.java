package cat.tecnocampus.telegramservice.apistate;

import java.util.ArrayList;
import java.util.List;

public class AuthInfo {

    private int dcId;
    private long id;
    private long apiStorageId;

    private byte[] authKey;

    private boolean isAuthorised;

    private List<LastKnownSalt> salts;

    private List<OldSession> oldSessions;

    public AuthInfo(int dcId, byte[] authKey) {
        this.dcId = dcId;
        this.authKey = authKey;
        this.isAuthorised = false;
        this.salts = new ArrayList<LastKnownSalt>();
        this.oldSessions = new ArrayList<OldSession>();
    }

    public AuthInfo() {

    }

    
    public long getapiStorageId(){
        return apiStorageId;
    }
    
    public void setapiStorageId(long id){
        apiStorageId =id;
    }
    
    public long getId(){
        return id;
    }
    
    public void setId(long id){
        this.id=id;
    }
    
    public int getDcId() {
        return dcId;
    }

    public byte[] getAuthKey() {
        return authKey;
    }

    public boolean isAuthorised() {
        return isAuthorised;
    }

    public void setAuthorised(boolean authorised) {
        isAuthorised = authorised;
    }

    public List<LastKnownSalt> getSalts() {
        return salts;
    }
    
    public void setSalts(List<LastKnownSalt> salts) {
    	this.salts = salts;
    }

    public List<OldSession> getOldSessions() {
        return oldSessions;
    }
    
    public void setOldSession(List<OldSession> oldSessions) {
    	this.oldSessions = oldSessions;
    }
    
}