package cat.tecnocampus.telegramservice.apistate;

public class Account {
	
	private int accountId;
	private int companyId;
	private String phoneNumber;
	private boolean isLoggedIn;
	
	public Account(int accountId, int companyId, String phoneNumber, boolean isLoggedIn) {
		this.accountId = accountId;
		this.companyId = companyId;
		this.phoneNumber = phoneNumber;
		this.isLoggedIn = isLoggedIn;
	}
	
	public int getAccountId() {
		return accountId;
	}
	
	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	public int getCompanyId() {
		return companyId;
	}
	
	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public boolean isLoggedIn() {
		return isLoggedIn;
	}
	
	public void setLoggedIn(boolean isLoggedIn) {
		this.isLoggedIn = isLoggedIn;
	}

}