package cat.tecnocampus.telegramservice.apistate;

public class LastKnownSalt {

    private int validSince;
    private int validUntil;
    private long salt;

    public LastKnownSalt(int validSince, int validUntil, long salt) {
        this.validSince = validSince;
        this.validUntil = validUntil;
        this.salt = salt;
    }

    public LastKnownSalt() {

    }

    public int getValidSince() {
        return validSince;
    }

    public int getValidUntil() {
        return validUntil;
    }

    public long getSalt() {
        return salt;
    }
    
}