package cat.tecnocampus.telegramservice.util;

public class MySocketException extends Exception {
	
	public MySocketException() {
		
	}
	
	public MySocketException(String message) {
		super(message);
	}

}
