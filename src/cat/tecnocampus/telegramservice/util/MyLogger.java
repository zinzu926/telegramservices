package cat.tecnocampus.telegramservice.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyLogger {
    private static final Logger log=LoggerFactory.getLogger(MyLogger.class);
	
	public static void log(String message) {
		log.info(message);
	}
	
	public static void log(String phoneNumberOfService, String message) {
		log.info(phoneNumberOfService + " -> " + message);
	}
	
	public static void log(String phoneNumberOfService, Exception e) {
		log.error(phoneNumberOfService + " -> " + e.getMessage());
	}

}
