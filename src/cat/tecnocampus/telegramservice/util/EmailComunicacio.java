/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cat.tecnocampus.telegramservice.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author netlab
 */
public class EmailComunicacio {
    
       private static EmailComunicacio ipcEmail=null;
    private static final int numPort = 5600;
    private static final int timeout = 7 * 1000;
    private static final String ipLocalhost = "localhost";
    

    private EmailComunicacio() {

    }

    public static synchronized EmailComunicacio getInstancia() {
        if (ipcEmail == null) {
            ipcEmail = new EmailComunicacio();
        }
        return ipcEmail;
    }
    
      public boolean missatge(String telefon ) {
        boolean ok = false;
        String missatge = "2" + Configuracio.tagSepara + "" + Configuracio.tagSepara + telefon + Configuracio.tagSepara + "" + Configuracio.tagSepara + "0" + Configuracio.tagSepara+ Configuracio.tagFinal;
        if (envia(missatge,telefon).contains("200")) {
            ok = true;
        }
        return ok;
    }
    
    
    private String envia(String textEnviar, String telefon) {
        String inputLine = "";
        Socket clientSocket = null;
        PrintWriter outToServer = null;
        BufferedReader inFromServer = null;
        try {
            clientSocket = new Socket(ipLocalhost, numPort);
            clientSocket.setSoTimeout(timeout);
            outToServer = new PrintWriter(clientSocket.getOutputStream(), true);
            inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            outToServer.println(textEnviar);

            while ((inputLine = inFromServer.readLine()) != null) {
                if (inputLine != null && inputLine.contains(Configuracio.tagFinal)&& inputLine.contains(telefon)) {
                    break;
                }                
            }
            outToServer.close();
            inFromServer.close();
            clientSocket.close();
        } catch (IOException ex) {
            if (outToServer != null) {
                outToServer.close();
            }
            try {
                if (inFromServer != null) {
                    inFromServer.close();
                }
                if (clientSocket != null) {
                    clientSocket.close();
                }
            } catch (IOException ex1) {
                Logger.getLogger(EmailComunicacio.class.getName()).log(Level.SEVERE, null, ex1);
            }
            Logger.getLogger(EmailComunicacio.class.getName()).log(Level.SEVERE, null, ex);
        }

        return inputLine;
    }
    
    
}
