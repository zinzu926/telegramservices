/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cat.tecnocampus.telegramservice.util;

import java.util.ArrayList;

/**
 *
 * @author netlab
 */
public class RespostaKeyword {
    
    private int webservei;
    private String resposta;
    private int mida;
    private int idRespostaRapida;
    
    
    public RespostaKeyword (int idRespostaRapida, int webservei, String resposta, int mida){
        this.webservei= webservei;
        this.resposta= resposta;
        this.mida = mida;
        this.idRespostaRapida = idRespostaRapida;
    }
    
      public RespostaKeyword (int mida){       
        this.mida = mida;                
    }
    
      public int getServei(){
          return webservei;
      }
      
      public String getResposta (){
          return resposta;
      }
      
      public int getMida(){
          return mida;
      }
      
      public int getIdRespostaRapida(){
          return idRespostaRapida;
      }
    
      public void setIdRespostaRapida(int id){
          idRespostaRapida = id;
      }
      
      public void setServei (int web){
          webservei= web;
      }
      
      
      public void setResposta (String resp){
          resposta=resp;
      }
      
      public void setMida (int mida){
          this.mida=mida;
      }
    
}
