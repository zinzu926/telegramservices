package cat.tecnocampus.telegramservice.util;
public class ApiConfig {

	public static final String DEVICE_MODEL = "deviceModel";
	public static final String SYSTEM_VERSION = "systemVersion";
	public static final String APP_VERSION = "appVersion";
	public static final String LANG_CODE = "es";
	
    public static final int API_ID = 16656;
    public static final String API_HASH = "5253c0343e4340e6f1bee43a66ea64a2";
    
    public static final int API_ID_TEST = 2521;
    public static final String API_HASH_TEST = "64d20abd7ee359213579e9a599fde866";
    
}