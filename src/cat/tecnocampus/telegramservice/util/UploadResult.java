package cat.tecnocampus.telegramservice.util;

public class UploadResult {
	
	private int partsCount;
	private String hash;

	public UploadResult(String hash, int partsCount) {
		this.hash = hash;
		this.partsCount = partsCount;
	}

	public int getPartsCount() {
		return partsCount;
	}

	public String getHash() {
		return hash;
	}
	
}