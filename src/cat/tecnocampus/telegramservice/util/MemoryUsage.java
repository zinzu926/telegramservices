/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cat.tecnocampus.telegramservice.util;

/**
 *
 * @author netlab
 */
public class MemoryUsage {
    
    private static MemoryUsage memoria=null;
    private static int comptador;
    private static boolean inici=false;
    private static long compteTemps=0;
    private static int comptaRebuts=0;
    private static int comptaRebutsAccess=0;
    private static int comptaEnviats=0;
    
    private MemoryUsage(){
        
    }
    
     public static synchronized MemoryUsage getInstancia() {
        if (memoria == null) {
            memoria = new MemoryUsage();
            comptador=0;
            inici=false;
        }
        return memoria;

    }
     
     
    /* public static synchronized void inicia(){
         if (!inici){
             inici=true;
             System.out.println("Temps inicial:"+System.currentTimeMillis()/1000);
         }
     }
     
     public static synchronized void finalitza(){
         compteTemps=System.currentTimeMillis()/1000;
     }
     
     public static synchronized long getTempsFinal(){
         return compteTemps;
     }
     
     public static synchronized void comptaRebuts(){
         comptaRebuts++;
     }
     
     public static synchronized int getRebuts(){
         return comptaRebuts;
     }
     
        public static synchronized void comptaRebutsAccess(){
         comptaRebutsAccess++;
     }
     
     public static synchronized int getRebutsAccess(){
         return comptaRebutsAccess;
     }
     
     
     public static synchronized int getEnviats(){
         return comptaEnviats;
     }
     
     public static synchronized void comptaEnviats(){
         comptaEnviats++;
     }*/
     
     
     public void sendFreeMemory(){
         comptador++;
         if (comptador==6){
             System.out.println("JVM freeMemory: " + Runtime.getRuntime().freeMemory());
              System.out.println("JVM totalMemory also equals to initial heap size of JVM : "
                                         + Runtime.getRuntime().totalMemory());
              System.out.println("JVM maxMemory also equals to maximum heap size of JVM: "
                                         + Runtime.getRuntime().maxMemory());
              comptador=0;
         }
     }
    
}
