/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cat.tecnocampus.telegramservice.util;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author netlab
 */
public class Configuracio {
    
    //public static final String pathFolder = "/opt/010mataro/TL/";//josep
    //public static final String pathFolder = "/opt/citymes/TL/";//josep
    public static final String pathFolder = "C:/opt/citymes/TL/";//josep
    public static final String usuariCitymes = "CityMes";
    //public static final String noSuport = "El servei només pot rebre missatges de text i imatges, la resta de missatges no s'atenen";
    public static final String tagSepara="<separa>";
    public static final String tagFinal="</finalitzacio>";
    public static final List<String> noservei = Arrays.asList("Service not available in private chat","Servicio no disponible en chat privado","Servei no disponible en chat privat","Service non disponible dans le chat privé");
}
