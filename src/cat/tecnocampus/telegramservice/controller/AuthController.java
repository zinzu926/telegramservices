package cat.tecnocampus.telegramservice.controller;

import cat.tecnocampus.telegramservice.TelegramService;
import cat.tecnocampus.telegramservice.thread.ApplicationThread;
import cat.tecnocampus.telegramservice.thread.VerifyAuth;
import cat.tecnocampus.telegramservice.util.ApiConfig;
import cat.tecnocampus.telegramservice.util.MyLogger;
import cat.tecnocampus.telegramservice.util.MySocketException;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.telegram.api.TLConfig;
import org.telegram.api.TLNearestDc;
import org.telegram.api.auth.TLSentCode;
import org.telegram.api.engine.RpcException;
import org.telegram.api.requests.TLRequestAuthSendCode;
import org.telegram.api.requests.TLRequestAuthSignIn;
import org.telegram.api.requests.TLRequestAuthSignUp;
import org.telegram.api.requests.TLRequestHelpGetConfig;
import org.telegram.api.requests.TLRequestHelpGetNearestDc;

public class AuthController {

    private ApplicationThread application;
    private ScheduledExecutorService executor;
    private VerifyAuth verifyAuthPoller;
    
    private String phoneHash;

    public AuthController(ApplicationThread applicationThread) {
        this.application = applicationThread;
        this.executor = Executors.newScheduledThreadPool(1);
        this.verifyAuthPoller = new VerifyAuth(application);
      
    }

    public void startVerifyAuthPoller() {
        executor.scheduleWithFixedDelay(verifyAuthPoller, 0, 5, TimeUnit.MINUTES);
    }

    public void pauseVerifyAuthPoller() {
        verifyAuthPoller.pause();
    }

    public void resumeVerifyAuthPoller() {
        verifyAuthPoller.resume();
    }

 

    public boolean isAuthenticated() {
        return application.getApiState().isAuthenticated();
    }

    public void logout() {
        application.getApiState().resetAuth();

    }

    public void requestActivationCode() throws MySocketException {
        MyLogger.log(application.getAccount().getPhoneNumber(), "Requesting authorization code");
        try {
            TLConfig config = application.getApi().doRpcCallNonAuth(new TLRequestHelpGetConfig());
            application.getApi().getState().updateSettings(config);
            TLNearestDc neardc=application.getApi().doRpcCallNonAuth(new TLRequestHelpGetNearestDc());
            application.getApi().switchToDc(neardc.getNearestDc());
            TLSentCode result = application.getApi().doRpcCallNonAuth(new TLRequestAuthSendCode(application.getAccount().getPhoneNumber(), 0, ApiConfig.API_ID, ApiConfig.API_HASH, ApiConfig.LANG_CODE));
            this.phoneHash = result.getPhoneCodeHash();
            
            MyLogger.log(application.getAccount().getPhoneNumber(), "Authorization code has been sent");
        } catch (RpcException e) {
            MyLogger.log(application.getAccount().getPhoneNumber(), "ERROR! RpcException in requesting authorization code");
            MyLogger.log(application.getAccount().getPhoneNumber(), "Errorcode: " + e.getErrorCode());
            MyLogger.log(application.getAccount().getPhoneNumber(), "Message: " + e.getErrorTag());
            if (e.getErrorCode() == 303) {
                String split[] = e.getErrorTag().split("_");
                int destDC = Integer.parseInt(split[split.length - 1]);
                application.getApi().switchToDc(destDC);
                requestActivationCode();
            } else {
                throw new MySocketException("0<separa>Error<separa>Errorcode: " + e.getErrorCode() + ", Message: " + e.getErrorTag() + "<separa></finalitzacio>");
            }
        } catch (IOException e) {
            MyLogger.log(application.getAccount().getPhoneNumber(), "ERROR! IOException in requesting authorization code");
            MyLogger.log(application.getAccount().getPhoneNumber(), "Message: " + e.getMessage());
            throw new MySocketException("0<separa>Error<separa>IOException in requesting authorization code<separa></finalitzacio>");
        }
    }

    public void signIn(String phoneCode, String des) throws MySocketException, RpcException {
        MyLogger.log(application.getAccount().getPhoneNumber(), "Starting sign in process");
        try {
            application.getApi().doRpcCallNonAuth(new TLRequestAuthSignIn(application.getAccount().getPhoneNumber(), this.phoneHash, phoneCode));
            application.getApiState().setAuthenticated(true);
            MyLogger.log(application.getAccount().getPhoneNumber(), "Authentication process finished");
        } catch (RpcException e) {
            MyLogger.log(application.getAccount().getPhoneNumber(), "ERROR! RpcException in sign in");
            MyLogger.log(application.getAccount().getPhoneNumber(), "Errorcode: " + e.getErrorCode());
            MyLogger.log(application.getAccount().getPhoneNumber(), "Message: " + e.getErrorTag());
            if (e.getErrorCode() == 303) {
                String split[] = e.getErrorTag().split("_");
                int destDC = Integer.parseInt(split[split.length - 1]);
                application.getApi().switchToDc(destDC);
                signIn(phoneCode, des);
            } else if (e.getErrorCode() == 400 && e.getErrorTag().equals("PHONE_CODE_EXPIRED")) {
                throw e;
            } else if (e.getErrorCode() == 400 && e.getErrorTag().equals("PHONE_NUMBER_UNOCCUPIED")) {
                try {
                    signUp(phoneCode, des);
                } catch (IOException ex) {
                    MyLogger.log(application.getAccount().getPhoneNumber(), "ERROR! IOException in sign un");
                    MyLogger.log(application.getAccount().getPhoneNumber(), "Message: " + e.getMessage());
                    throw new MySocketException("0<separa>Error<separa>Errorcode: " + e.getMessage() + "<separa></finalitzacio>");
                }
            } else {
               
                throw new MySocketException("0<separa>Error<separa>Errorcode: " + e.getErrorCode() + ", Message: " + e.getErrorTag() + "<separa></finalitzacio>");
            }
        } catch (IOException e) {
            MyLogger.log(application.getAccount().getPhoneNumber(), "ERROR! IOException in sign in");
            MyLogger.log(application.getAccount().getPhoneNumber(), "Message: " + e.getMessage());
            throw new MySocketException("0<separa>Error<separa>Errorcode: " + e.getMessage() + "<separa></finalitzacio>");
        }
    }

    private void signUp(String phoneCode, String des) throws IOException {
        MyLogger.log(application.getAccount().getPhoneNumber(), "Starting sign up process");
        try {
            String[] array;
            String firstName;
            String secondName = "";

            array = des.split(" ");
            firstName = array[0];
            if (array.length == 2) {
                secondName = array[1];
            }

            application.getApi().doRpcCallNonAuth(new TLRequestAuthSignUp(application.getAccount().getPhoneNumber(), this.phoneHash, phoneCode, firstName, secondName));
            application.getApiState().setAuthenticated(true);
            MyLogger.log(application.getAccount().getPhoneNumber(), "Sign up successful");
            MyLogger.log(application.getAccount().getPhoneNumber(), "Authentication process finished");
        } catch (RpcException e) {
            MyLogger.log(application.getAccount().getPhoneNumber(), "ERROR! in sign up");
            MyLogger.log(application.getAccount().getPhoneNumber(), "Errorcode: " + e.getErrorCode());
            MyLogger.log(application.getAccount().getPhoneNumber(), "Message: " + e.getErrorTag());
        }
    }

}
