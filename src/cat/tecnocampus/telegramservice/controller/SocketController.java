package cat.tecnocampus.telegramservice.controller;

import cat.tecnocampus.telegramservice.TelegramService;
import cat.tecnocampus.telegramservice.thread.ServiceSocket;

public class SocketController {
	
	private ServiceSocket serviceSocket;
	
	public SocketController(TelegramService service) {
		this.serviceSocket = new ServiceSocket(service);
	}
	
	public void startSocket() {
		new Thread(serviceSocket).start();
	}

}
