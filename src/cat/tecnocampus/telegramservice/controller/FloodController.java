/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.tecnocampus.telegramservice.controller;

import cat.tecnocampus.telegramservice.thread.ApplicationThread;

/**
 *
 * @author netlab
 */
public class FloodController {

    private long temps = 0L;
    private ApplicationThread application;

    public FloodController(ApplicationThread app) {
        application = app;
    }

    public void setWait( int espera) {
        synchronized (application) {
            temps = (System.currentTimeMillis() / 1000) + espera;
        }

    }

    public boolean getWait() {
        synchronized (application) {
            boolean ok = false;
            if (temps > (System.currentTimeMillis() / 1000)) {
                ok = true;
            }

            return ok;
        }
    }

}
