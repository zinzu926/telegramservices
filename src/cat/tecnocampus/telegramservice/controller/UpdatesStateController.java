package cat.tecnocampus.telegramservice.controller;

import cat.tecnocampus.telegramservice.persistence.DbUpdatesState;
import cat.tecnocampus.telegramservice.persistence.DbUpdatesStateMapper;
import cat.tecnocampus.telegramservice.thread.ApplicationThread;

public class UpdatesStateController {

	private ApplicationThread application;
	private DbUpdatesStateMapper updatesStateMapper;
	
	public UpdatesStateController(ApplicationThread applicationThread) {
		this.application = applicationThread;
		this.updatesStateMapper = new DbUpdatesStateMapper();
	}
	   
        
	public DbUpdatesState getUpdatesState() {
		return updatesStateMapper.getUpdatesState(application.getAccount().getPhoneNumber());
	}
	
        public void deleteUpdateState(String telefon){
            updatesStateMapper.delete(telefon);
        }
        
	public void insertUpdatesState(int pts, int qts, int date, int seq) {
		updatesStateMapper.createState(application.getAccount().getPhoneNumber(), pts, qts, date,seq);
	}
	
	public void updateSate(int pts, int qts, int date, int seq) {
		updatesStateMapper.updateState(application.getAccount().getPhoneNumber(), pts, qts, date,seq);
	}
	
	public void updatePts(int pts) {
		updatesStateMapper.updatePts(application.getAccount().getPhoneNumber(), pts);
	}
	
	public void updateQts(int qts) {
		updatesStateMapper.updateQts(application.getAccount().getPhoneNumber(), qts);
	}
	
	public void updateDate(int date) {
		updatesStateMapper.updateDate(application.getAccount().getPhoneNumber(), date);
	}
        
        public void updateSeq(int seq) {
		updatesStateMapper.updateSeq(application.getAccount().getPhoneNumber(), seq);
	}
        
        public int getSeq(){
            return updatesStateMapper.getSeq(application.getAccount().getPhoneNumber());
        }
        
         public int getPts(){
            return updatesStateMapper.getPts(application.getAccount().getPhoneNumber());
        }

}
