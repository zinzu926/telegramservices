package cat.tecnocampus.telegramservice.controller;

import cat.tecnocampus.telegramservice.persistence.DbUpdatesState;
import cat.tecnocampus.telegramservice.process.EncryptedIncomingMessage;
import cat.tecnocampus.telegramservice.process.IncomingMessage;
import cat.tecnocampus.telegramservice.process.UpdateEncryption;
import cat.tecnocampus.telegramservice.thread.ApplicationThread;
import cat.tecnocampus.telegramservice.util.MyLogger;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.telegram.api.TLAbsEncryptedMessage;
import org.telegram.api.TLAbsMessage;
import org.telegram.api.TLAbsUpdate;
import org.telegram.api.TLAbsUser;
import org.telegram.api.TLInputPeerChat;
import org.telegram.api.TLInputUserSelf;
import org.telegram.api.TLMessage;
import org.telegram.api.TLMessageForwarded;
import org.telegram.api.TLUpdateChatParticipants;
import org.telegram.api.TLUpdateEncryption;
import org.telegram.api.TLUpdateNewEncryptedMessage;
import org.telegram.api.TLUpdateNewMessage;
import org.telegram.api.TLUpdateReadMessages;
import org.telegram.api.TLUpdateShort;
import org.telegram.api.TLUpdateShortChatMessage;
import org.telegram.api.TLUpdateShortMessage;
import org.telegram.api.TLUpdates;
import org.telegram.api.TLUpdatesCombined;
import org.telegram.api.TLUpdatesTooLong;
import org.telegram.api.TLUserContact;
import org.telegram.api.TLUserForeign;
import org.telegram.api.TLUserRequest;
import org.telegram.api.messages.TLAbsMessages;
import org.telegram.api.messages.TLAbsSentEncryptedMessage;
import org.telegram.api.messages.TLAbsSentMessage;
import org.telegram.api.messages.TLAbsStatedMessage;
import org.telegram.api.messages.TLAffectedHistory;
import org.telegram.api.requests.TLRequestMessagesDeleteChatUser;
import org.telegram.api.requests.TLRequestMessagesDeleteHistory;
import org.telegram.api.requests.TLRequestMessagesGetMessages;
import org.telegram.api.requests.TLRequestUpdatesGetDifference;
import org.telegram.api.requests.TLRequestUpdatesGetState;
import org.telegram.api.updates.TLAbsDifference;
import org.telegram.api.updates.TLDifference;
import org.telegram.api.updates.TLDifferenceEmpty;
import org.telegram.api.updates.TLDifferenceSlice;
import org.telegram.api.updates.TLState;
import org.telegram.tl.TLIntVector;
import org.telegram.tl.TLVector;

public class IncomingMessagesController implements Runnable {

    private class PackageIdentity {

        public int seq;
        public int seqEnd;
        public int date;
        public int pts;
        public int qts;
    }

    private static final BigInteger[] KNOWN_PRIMES = new BigInteger[]{
        new BigInteger("C71CAEB9C6B1C9048E6C522F70F13F73980D40238E3E21C14934D037563D930F48198A0AA7C14058229493D22530F4DBFA336F6E0AC925139543AED44CCE7C3720FD51F69458705AC68CD4FE6B6B13ABDC9746512969328454F18FAF8C595F642477FE96BB2A941D5BCD1D4AC8CC49880708FA9B378E3C4F3A9060BEE67CF9A4A4A695811051907E162753B56B0F6B410DBA74D8A84B2A14B3144E0EF1284754FD17ED950D5965B4B9DD46582DB1178D169C6BC465B0D6FF9CA3928FEF5B9AE4E418FC15E83EBEA0F87FA9FF5EED70050DED2849F47BF959D956850CE929851F0D8115F635B105EE2E4E15D04B2454BF6F4FADF034B10403119CD8E3B92FCC5B", 16)
    };

    private ApplicationThread application;
    private Object absUpdates;
    private static final int DIFF_TIMEOUT = 60 * 1000;
    private static final int TIMEOUT = 5 * 1000;
    private static final int CHECK_TIME = 2 * 1000;
    //private long access_hash;
    private DbUpdatesState updateState;
    private boolean isOKUser = true;

    private HashMap<Integer, Object> further = new HashMap<Integer, Object>();
    private volatile boolean isBrokenSequence = false;
    private Timer timer;
    private volatile boolean isInvalidated = false;
    private ExecutorService incomingMessageProcesses;

    public IncomingMessagesController(ApplicationThread applicationThread) {
        this.application = applicationThread;

        this.incomingMessageProcesses = Executors.newCachedThreadPool();
    }

    public void setIncomingUpdates(Object absUpdates) {
        this.absUpdates = absUpdates;
    }

    @Override
    public void run() {
        //To change body of generated methods, choose Tools | Templates.

        onMessage((Object) absUpdates);

    }

    public synchronized void onMessage(Object object) {

        isOKUser = true;
        PackageIdentity identity = getPackageIdentity(object);

        if (!accept(identity)) {
            return;
        }

        if (isInvalidated) {
            further.put(identity.seq, object);
            doSync();
            return;
        }

        if (causeSeqInvalidation(identity)) {
            further.put(identity.seq, object);
            if (!isBrokenSequence) {
                isBrokenSequence = true;
                requestSequenceCheck();

            }

            return;
        }

        if (isBrokenSequence && identity.seq != 0) {

            cancelSequenceCheck();
        }

        if (object instanceof TLUpdates) {
            TLUpdates updates = (TLUpdates) object;

            for (TLAbsUpdate update : updates.getUpdates()) {
                onUpdate(update, identity);
            }
        } else if (object instanceof TLUpdateShortMessage) {
            TLUpdateShortMessage updateShortMessage = (TLUpdateShortMessage) object;
            if (getUserAccessHash(updateShortMessage.getId(), updateShortMessage.getFromId())) {

                onTLUpdateShortMessage(updateShortMessage);
            } else {
                isOKUser = false;
            }

        } else if (object instanceof TLUpdatesCombined) {
            TLUpdatesCombined updatesCombined = (TLUpdatesCombined) object;
            TLVector<TLAbsUpdate> absUpdate2 = updatesCombined.getUpdates();
            for (TLAbsUpdate absUpdate3 : absUpdate2) {
                onUpdate(absUpdate3, identity);
            }
        } else if (object instanceof TLUpdatesTooLong) {
            invalidateUpdates();
        }

        if (!isOKUser && identity.seq != 0) {
            further.put(identity.seq, object);

            invalidateUpdates();
            return;
        }
        if (identity != null && identity.seq != 0) {
            if (identity.seqEnd != 0) {
                updateState.setSeq(identity.seqEnd);
            } else {
                updateState.setSeq(identity.seq);
            }
            if (identity.date != 0) {

                updateState.setDate(identity.date);
            }
            if (identity.qts != 0) {
                updateState.setQts(identity.qts);
            }
            if (identity.pts != 0) {
                updateState.setPts(identity.pts);
            }

            application.getUpdatesStateController().updateSate(updateState.getPts(), updateState.getQts(), updateState.getDate(), updateState.getSeq());

        }

        onValidated();

    }

    public void onUpdate(TLAbsUpdate update, PackageIdentity identity) {

        if (update instanceof TLUpdateNewMessage) {
            onUpdateNewMessage((TLUpdateNewMessage) update);
        } else if (update instanceof TLUpdateChatParticipants) {
            TLUpdateChatParticipants chatParticipants = (TLUpdateChatParticipants) update;
            int idChat = chatParticipants.getParticipants().getChatId();
            int offset = 0;
            do {
                try {
                    TLAffectedHistory affectedHistory = application.getApi().doRpcCall(new TLRequestMessagesDeleteHistory(new TLInputPeerChat(idChat), offset));
                    onMessage((Object) affectedHistory);
                    if (affectedHistory != null) {
                        offset = affectedHistory.getOffset();
                    }
                } catch (IOException ex) {
                    Logger.getLogger(IncomingMessagesController.class.getName()).log(Level.SEVERE, null, ex);
                }
            } while (offset != 0);
            try {
                application.getApi().doRpcCall(new TLRequestMessagesDeleteChatUser(idChat, new TLInputUserSelf()));
            } catch (IOException ex) {
                Logger.getLogger(IncomingMessagesController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if (update instanceof TLUpdateEncryption) {

            incomingMessageProcesses.execute(new UpdateEncryption(application, ((TLUpdateEncryption) update).getChat()));
        } else if (update instanceof TLUpdateNewEncryptedMessage) {

            incomingMessageProcesses.execute(new EncryptedIncomingMessage(application, ((TLUpdateNewEncryptedMessage) update).getMessage()));
            //onEncryptedMessage(((TLUpdateNewEncryptedMessage) update).getMessage());
            identity.qts = Math.max(identity.qts, ((TLUpdateNewEncryptedMessage) update).getQts());
        }
    }

    private void onUpdateNewMessage(TLUpdateNewMessage update) {

        onAbsMessage(update.getMessage(), false);
    }

    public synchronized void onAbsMessage(TLAbsMessage absMessage, boolean isOffline) {

        if (absMessage instanceof TLMessage) {
            onMessage((TLMessage) absMessage);
        } else if (absMessage instanceof TLMessageForwarded) {
            onMessage((TLMessageForwarded) absMessage);
        }
    }

    private void onMessage(TLMessage message) {
        if (!message.getOut() && message.getUnread() && message.getFromId() != 777000) {
            if (getUserAccessHash(message.getId(), message.getFromId())) {

                IncomingMessage incoming = new IncomingMessage(application, message.getId(), message.getFromId(), message.getMessage(), message.getMedia(), null, 0);
                incomingMessageProcesses.execute(incoming);
            }
        }
    }

    private void onMessage(TLMessageForwarded message) {

        if (!message.getOut() && message.getUnread() && message.getFromId() != 777000) {

            if (getUserAccessHash(message.getId(), message.getFromId())) {

                IncomingMessage incoming = new IncomingMessage(application, message.getId(), message.getFromId(), message.getMessage(), message.getMedia(), null, 0);
                incomingMessageProcesses.execute(incoming);
            }
        }
    }

    private void onTLUpdateShortMessage(TLUpdateShortMessage message) {

        if (message.getFromId() != 777000) {

            IncomingMessage incoming = new IncomingMessage(application, message.getId(), message.getFromId(), message.getMessage(), null, null, 0);
            incomingMessageProcesses.execute(incoming);
        }
    }

    private boolean getUserAccessHash(int messageId, int fromId) {

        boolean ok = false;
        TLIntVector messageIds = new TLIntVector();
        messageIds.add(messageId);
        try {
            TLAbsMessages messages = application.getApi().doRpcCall(new TLRequestMessagesGetMessages(messageIds));
            TLVector<TLAbsUser> users = messages.getUsers();
            for (TLAbsUser user : users) {

                if ((user instanceof TLUserRequest) && (user.getId() == fromId)) {
                    ok = true;
                } else if ((user instanceof TLUserForeign) && (user.getId() == fromId)) {
                    ok = true;
                } else if ((user instanceof TLUserContact) && (user.getId() == fromId)) {
                    ok = true;
                }

            }
        } catch (IOException e) {
            ok = false;
            MyLogger.log(application.getAccount().getPhoneNumber(), "ERROR! in getting user access hash: " + messageId);
            MyLogger.log(application.getAccount().getPhoneNumber(), "Message: " + e.getMessage());
        }
        return ok;
    }

    private synchronized void doSync() {
        int pts = application.getUpdatesStateController().getPts();
        if (!isInvalidated) {
            return;
        }
        if (!hasState(pts)) {
            try {
                TLState state = application.getApi().doRpcCall(new TLRequestUpdatesGetState(), TIMEOUT);
                if (state != null) {

                    application.getUpdatesStateController().updateSate(state.getPts(), state.getQts(), state.getDate(), state.getSeq());

                }
            } catch (IOException ex) {
                Logger.getLogger(IncomingMessagesController.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            checkForMessagesReceivedWhenOffline();
        }

        isInvalidated = false;
        onValidated();

    }

    private void requestSequenceCheck() {
        timer = new Timer();
        timer.schedule(new TimerTask() {

            @Override
            public void run() {
                if (isBrokenSequence) {
                    isBrokenSequence = false;
                    invalidateUpdates();
                }
            }
        }, CHECK_TIME);

    }

    private void cancelSequenceCheck() {
        isBrokenSequence = false;
        if (timer != null) {
            timer.cancel();
            timer = null;
        }

    }

    private boolean hasState(int pts) {

        return pts > 0;
    }

    private synchronized boolean accept(PackageIdentity identity) {

        updateState = application.getUpdatesStateController().getUpdatesState();

        if (identity == null) {
            return true;
        }
        if (!hasState(updateState.getPts())) {
            return true;
        }
        if (identity.seq != 0) {
            if (identity.seq <= updateState.getSeq()) {

                return false;
            } else {
                return true;
            }
        }
        return true;
    }

    private synchronized boolean causeSeqInvalidation(PackageIdentity identi) {

        updateState = application.getUpdatesStateController().getUpdatesState();

        if (!hasState(updateState.getPts())) {
            return false;
        }
        if (identi.seq != 0) {
            if (identi.seq != updateState.getSeq() + 1) {
                return true;
            }
        }
        return false;
    }

    public synchronized void invalidateUpdates() {

        if (!isInvalidated) {
            isInvalidated = true;
            doSync();
        }
    }

    public synchronized boolean checkForMessagesReceivedWhenOffline() {
        boolean ok = true;
        try {

            DbUpdatesState updatesState = application.getUpdatesStateController().getUpdatesState();

            TLAbsDifference absDifference = application.getApi().doRpcCall(new TLRequestUpdatesGetDifference(updatesState.getPts(), updatesState.getDate(), updatesState.getQts()), DIFF_TIMEOUT);
            if (absDifference instanceof TLDifference) {

                TLDifference difference = (TLDifference) absDifference;
                TLState state2 = difference.getState();

                TLVector<TLAbsMessage> absMessages = difference.getNewMessages();

                for (final TLAbsMessage absMessage : absMessages) {
                    onAbsMessage(absMessage, true);
                }
                List<TLAbsEncryptedMessage> messages = difference.getNewEncryptedMessages();
                for (TLAbsEncryptedMessage encryptedMessage : messages) {
                    incomingMessageProcesses.execute(new EncryptedIncomingMessage(application, encryptedMessage));
                }

                if (state2 != null) {
                    application.getUpdatesStateController().updateSate(state2.getPts(), state2.getQts(), state2.getDate(), state2.getSeq());
                }

            } else if (absDifference instanceof TLDifferenceSlice) {
                TLDifferenceSlice differenceSlice = (TLDifferenceSlice) absDifference;
                TLState stateIntermedia = differenceSlice.getIntermediateState();

                for (final TLAbsMessage absMessage : differenceSlice.getNewMessages()) {
                    onAbsMessage(absMessage, true);
                }

                List<TLAbsEncryptedMessage> messages = differenceSlice.getNewEncryptedMessages();
                for (TLAbsEncryptedMessage encryptedMessage : messages) {
                    incomingMessageProcesses.execute(new EncryptedIncomingMessage(application, encryptedMessage));
                }

                if (stateIntermedia != null) {
                    application.getUpdatesStateController().updateSate(stateIntermedia.getPts(), stateIntermedia.getQts(), stateIntermedia.getDate(), stateIntermedia.getSeq());
                }

                Thread.sleep(2000);
                checkForMessagesReceivedWhenOffline();
            } else if (absDifference instanceof TLDifferenceEmpty) {
                TLDifferenceEmpty differenceEmpty = (TLDifferenceEmpty) absDifference;
                application.getUpdatesStateController().updateDate(differenceEmpty.getDate());
                application.getUpdatesStateController().updateSeq(differenceEmpty.getSeq());
            }

        } catch (IOException e) {
            ok = false;

        } catch (InterruptedException ex) {

        } finally {

        }
        return ok;
    }

    private PackageIdentity getPackageIdentity(Object object) {
        if (object instanceof TLUpdateShortChatMessage) {
            PackageIdentity identity = new PackageIdentity();
            identity.seq = ((TLUpdateShortChatMessage) object).getSeq();
            identity.seqEnd = 0;
            identity.pts = ((TLUpdateShortChatMessage) object).getPts();
            identity.date = ((TLUpdateShortChatMessage) object).getDate();
            return identity;
        } else if (object instanceof TLUpdateShortMessage) {
            PackageIdentity identity = new PackageIdentity();
            identity.seq = ((TLUpdateShortMessage) object).getSeq();
            identity.seqEnd = 0;
            identity.pts = ((TLUpdateShortMessage) object).getPts();
            identity.date = ((TLUpdateShortMessage) object).getDate();
            return identity;
        } else if (object instanceof TLUpdateShort) {
            PackageIdentity identity = new PackageIdentity();
            identity.seq = 0;
            identity.seqEnd = 0;
            identity.pts = 0;
            identity.date = ((TLUpdateShort) object).getDate();
            if (((TLUpdateShort) object).getUpdate() instanceof TLUpdateNewEncryptedMessage) {
                identity.qts = ((TLUpdateNewEncryptedMessage) (((TLUpdateShort) object).getUpdate())).getQts();
            }
            return identity;
        } else if (object instanceof TLUpdatesTooLong) {
            PackageIdentity identity = new PackageIdentity();
            identity.seq = 0;
            identity.seqEnd = 0;
            identity.pts = 0;
            identity.date = 0;
            return identity;
        } else if (object instanceof TLUpdates) {
            PackageIdentity identity = new PackageIdentity();
            identity.seq = ((TLUpdates) object).getSeq();
            identity.seqEnd = 0;
            identity.pts = 0;
            identity.date = ((TLUpdates) object).getDate();
            identity.qts = 0;
            for (TLAbsUpdate update : (((TLUpdates) object).getUpdates())) {
                if (update instanceof TLUpdateNewEncryptedMessage) {
                    identity.qts = Math.max(identity.qts, ((TLUpdateNewEncryptedMessage) update).getQts());
                }
            }
            return identity;
        } else if (object instanceof TLUpdatesCombined) {
            PackageIdentity identity = new PackageIdentity();
            identity.seq = ((TLUpdatesCombined) object).getSeqStart();
            identity.seqEnd = ((TLUpdatesCombined) object).getSeq();
            identity.pts = 0;
            identity.date = ((TLUpdatesCombined) object).getDate();
            for (TLAbsUpdate update : (((TLUpdatesCombined) object).getUpdates())) {
                if (update instanceof TLUpdateNewEncryptedMessage) {
                    identity.qts = Math.max(identity.qts, ((TLUpdateNewEncryptedMessage) update).getQts());
                }
            }
            return identity;
        } else if (object instanceof TLAbsSentMessage) {
            TLAbsSentMessage absSentMessage = (TLAbsSentMessage) object;
            PackageIdentity identity = new PackageIdentity();
            identity.seq = absSentMessage.getSeq();
            identity.seqEnd = 0;
            identity.pts = absSentMessage.getPts();
            identity.date = absSentMessage.getDate();
            return identity;
        } else if (object instanceof TLAbsStatedMessage) {
            TLAbsStatedMessage statedMessage = ((TLAbsStatedMessage) object);
            PackageIdentity identity = new PackageIdentity();
            identity.seq = statedMessage.getSeq();
            identity.seqEnd = 0;
            identity.pts = statedMessage.getPts();
            identity.date = 0;
            return identity; //
        } else if (object instanceof TLAffectedHistory) {
            TLAffectedHistory tlAffectedHistory = (TLAffectedHistory) object;
            PackageIdentity identity = new PackageIdentity();
            identity.seq = tlAffectedHistory.getSeq();
            identity.seqEnd = 0;
            identity.pts = tlAffectedHistory.getPts();
            identity.date = 0;
            return identity;
        } else if (object instanceof TLAbsSentEncryptedMessage) {
            TLAbsSentEncryptedMessage encryptedSent = (TLAbsSentEncryptedMessage) object;
            PackageIdentity identity = new PackageIdentity();
            identity.seq = 0;
            identity.seqEnd = 0;
            identity.pts = 0;
            identity.date = encryptedSent.getDate();
            return identity;
        }

        return null;
    }

    private synchronized void onValidated() {
        if (further.size() > 0) {
            Integer[] keys = further.keySet().toArray(new Integer[0]);
            int[] keys2 = new int[keys.length];
            for (int i = 0; i < keys2.length; i++) {
                keys2[i] = keys[i];
            }
            Arrays.sort(keys2);
            Object[] messages = new Object[keys2.length];
            for (int i = 0; i < keys2.length; i++) {
                messages[i] = further.get(keys2[i]);
            }
            further.clear();
            for (Object object : messages) {
                onMessage(object);
            }
        }

    }

}
