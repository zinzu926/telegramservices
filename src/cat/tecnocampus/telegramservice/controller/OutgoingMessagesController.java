package cat.tecnocampus.telegramservice.controller;

import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import cat.tecnocampus.telegramservice.thread.ApplicationThread;
import cat.tecnocampus.telegramservice.thread.OutgoingMessagesPoller;

public class OutgoingMessagesController {
	
	private ScheduledExecutorService executor;
	private OutgoingMessagesPoller outgoingMessagesPoller;
	
	public OutgoingMessagesController(Map<String, ApplicationThread> applications) {
		this.executor = Executors.newScheduledThreadPool(1);
		this.outgoingMessagesPoller = new OutgoingMessagesPoller(applications);
	}
	
	public void startOutgoingMessagesPoller() {
		executor.scheduleWithFixedDelay(outgoingMessagesPoller, 0, 30, TimeUnit.SECONDS);
	}

}