package cat.tecnocampus.telegramservice.persistence;

import cat.tecnocampus.telegramservice.apistate.ApiStorage;
import cat.tecnocampus.telegramservice.apistate.AuthInfo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AuthInfoMapper {

    private LastKnownSaltMapper lastKnownSaltMapper;
    private OldSessionMapper oldSessionMapper;

    public AuthInfoMapper() {
        this.lastKnownSaltMapper = new LastKnownSaltMapper();
        this.oldSessionMapper = new OldSessionMapper();
    }

    public List<AuthInfo> getAuthInfos(long apiStorageId) {
        List<AuthInfo> authInfos = new ArrayList<>();
        Connection connection = null;
        Statement statement = null;
        try {
            
            connection = Database.getConnection();
            statement = connection.createStatement();
            String sql = "SELECT id, dcId, authKey, isAuthorised"
                    + "\nFROM authinfo"
                    + "\nWHERE apiStorageId = " + apiStorageId + ";";
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                AuthInfo authInfo = new AuthInfo(rs.getInt("dcId"), rs.getBytes("authKey"));
                authInfo.setAuthorised(rs.getBoolean("isAuthorised"));
                long id = rs.getLong("id");
                authInfo.setId(id);
                authInfo.setapiStorageId(apiStorageId);
                authInfo.setSalts(lastKnownSaltMapper.getSalts(id));
                authInfo.setOldSession(oldSessionMapper.getOldSessions(id));
                authInfos.add(authInfo);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {

            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                statement = null;
                connection = null;
            }
        }
        return authInfos;
    }

    public void resetAuthenticated(long id) {
        Connection connection = null;
        Statement statement = null;

        try {
            
            connection = Database.getConnection();
            statement = connection.createStatement();
            String sql = "UPDATE authinfo"
                    + "\nSET isAuthorised = 0"
                    + "\nWHERE apiStorageId = " + id + ";";
            statement.executeUpdate(sql);
            sql = null;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {

            try {
                if (statement != null) {
                    statement.close();
                }

            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                statement = null;

            }

        }
    }

    public void updateAuthenticated(long id, boolean is) {
        Connection connection = null;
        Statement statement = null;

        try {
            
            connection = Database.getConnection();
            statement = connection.createStatement();
            String sql = "UPDATE authinfo"
                    + "\nSET isAuthorised = " + is
                    + "\nWHERE id = " + id + ";";
            statement.executeUpdate(sql);
            sql = null;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {

            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }

            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                statement = null;
                connection = null;
            }

        }
    }

    public void updateSalts(AuthInfo authInfo) {
        lastKnownSaltMapper.insertAll(authInfo.getId(), authInfo.getSalts());
    }

    public long insert(long apiStorageId, AuthInfo authInfo) {
        long id = 0L;
        PreparedStatement psmt = null;
        try {
           
            Connection connection = Database.getConnection();
            connection.setAutoCommit(false);
            psmt = connection.prepareStatement("INSERT INTO authinfo (dcId, authkey, isAuthorised, apiStorageId) VALUES (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);

            psmt.setInt(1, authInfo.getDcId());
            psmt.setBytes(2, authInfo.getAuthKey());
            psmt.setBoolean(3, authInfo.isAuthorised());
            psmt.setLong(4, apiStorageId);
            psmt.addBatch();

            psmt.executeBatch();
            connection.commit();
            ResultSet rs = psmt.getGeneratedKeys();
            rs.next();
            id = rs.getLong(1);
            lastKnownSaltMapper.insertAll(id, authInfo.getSalts());
            oldSessionMapper.insertAll(id, authInfo.getOldSessions());

            //connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (psmt != null) {
                    psmt.close();
                }

            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                psmt = null;

            }
        }
        return id;
    }

    public void insertAll(long apiStorageId, List<AuthInfo> authInfos, Connection connection) {
        PreparedStatement psmt = null;
        try {
            
            connection.setAutoCommit(false);
            psmt = connection.prepareStatement("INSERT INTO authinfo (dcId, authkey, isAuthorised, apiStorageId) VALUES (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            for (AuthInfo authInfo : authInfos) {
                psmt.setInt(1, authInfo.getDcId());
                psmt.setBytes(2, authInfo.getAuthKey());
                psmt.setBoolean(3, authInfo.isAuthorised());
                psmt.setLong(4, apiStorageId);
                psmt.addBatch();
            }
            psmt.executeBatch();
            connection.commit();
            ResultSet rs = psmt.getGeneratedKeys();
            int i = 0;
            while (rs.next()) {
                AuthInfo authInfo = authInfos.get(i);
                int authInfoId = rs.getInt(1);
                lastKnownSaltMapper.insertAll(authInfoId, authInfo.getSalts());
                oldSessionMapper.insertAll(authInfoId, authInfo.getOldSessions());
                i++;
            }

            
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (psmt != null) {
                    psmt.close();
                }

            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                psmt = null;

            }
        }
    }

    public void deleteAuthInfo(ApiStorage storage, boolean logout) {
        Statement statement = null;
        Connection connection = null;
        ResultSet rs = null;
        try {
            connection = Database.getConnection();
            statement = connection.createStatement();

            String sqlAuth = "DELETE FROM authinfo"
                    + "\nWHERE apiStorageId = " + storage.getId() + ";";
            statement.executeUpdate(sqlAuth);
            if (logout) {
                for (AuthInfo authInfo : storage.getAuthInfos()) {
                    lastKnownSaltMapper.delete(authInfo.getId());
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(ApiStorageMapper.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            rs = null;
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ApiStorageMapper.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    statement = null;
                }
            }

            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AuthInfoMapper.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    connection = null;
                }
            }
        }
    }

}
