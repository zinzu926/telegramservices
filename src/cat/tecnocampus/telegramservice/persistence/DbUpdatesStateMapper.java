package cat.tecnocampus.telegramservice.persistence;

import cat.tecnocampus.telegramservice.apistate.AuthInfo;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DbUpdatesStateMapper {

  

    public DbUpdatesState getUpdatesState(String phoneNumberOfService) {
        DbUpdatesState updatesState = null;
        Connection connection = null;
        Statement statement = null;
        ResultSet rs = null;
        try {
            
            connection = Database.getConnection();
            statement = connection.createStatement();
            String sql = "SELECT pts, qts, date, seq"
                    + "\nFROM updatesstate"
                    + "\nWHERE entitatTLF = " + phoneNumberOfService + ";";
            rs = statement.executeQuery(sql);
            while (rs.next()) {
                updatesState = new DbUpdatesState(
                        rs.getInt("pts"),
                        rs.getInt("qts"),
                        rs.getInt("date"),
                        rs.getInt("seq"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            rs = null;
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                statement = null;
                connection = null;
            }
        }
        return updatesState;
    }

    public int getSeq(String phoneNumberOfService) {
        Connection connection = null;
        Statement statement = null;
        ResultSet rs = null;
        int seq = 0;
        try {
           
            connection = Database.getConnection();
            statement = connection.createStatement();
            String sql = "SELECT seq"
                    + "\nFROM updatesstate"
                    + "\nWHERE entitatTLF = " + phoneNumberOfService + ";";
            rs = statement.executeQuery(sql);

            while (rs.next()) {
                seq = rs.getInt("seq");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            rs = null;
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                statement = null;
                connection = null;
            }
        }
        return seq;
    }

    public int getPts(String phoneNumberOfService) {
        Connection connection = null;
        Statement statement = null;
        ResultSet rs = null;
        int seq = 0;
        try {
            connection = Database.getConnection();
            statement = connection.createStatement();
            String sql = "SELECT pts"
                    + "\nFROM updatesstate"
                    + "\nWHERE entitatTLF = " + phoneNumberOfService + ";";
            rs = statement.executeQuery(sql);

            while (rs.next()) {
                seq = rs.getInt("pts");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            rs = null;
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                statement = null;
                connection = null;
            }
        }
        return seq;
    }

    public void delete(String telefon){
        Connection connection = null;
        Statement statement = null;
          try {
            connection = Database.getConnection();
            statement = connection.createStatement();
            String sqlAuth = "DELETE FROM updatesstate"
                    + "\nWHERE entitatTLF  = " + telefon  + ";";
            statement.executeUpdate(sqlAuth);
  
        } catch (SQLException ex) {
            Logger.getLogger(DbUpdatesStateMapper.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
           
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DbUpdatesStateMapper.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    statement = null;
                }
            }

            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DbUpdatesStateMapper.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    connection = null;
                }
            }
        }
        
    }
    
    
    public void createState(String phone, int pts, int qts, int date, int seq) {
        Connection connection = null;
        try {
            //Class.forName("com.mysql.jdbc.Driver");
            connection = Database.getConnection();
            PreparedStatement psmt = connection.prepareStatement("INSERT INTO updatesstate (pts, qts, date,seq, entitatTLF) VALUES (?, ?, ?, ?, ?)");
            psmt.setInt(1, pts);
            psmt.setInt(2, qts);
            psmt.setInt(3, date);
            psmt.setInt(4, seq);
            psmt.setString(5, phone);
            psmt.executeUpdate();
            psmt.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {

                if (connection != null) {
                    connection.close();
                }

            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                connection = null;
            }
        }

    }

    public void updateState(String phoneNumberOfService, int pts, int qts, int date, int seq) {
        Connection connection = null;
        Statement statement = null;
        try {
            //Class.forName("com.mysql.jdbc.Driver");
            connection = Database.getConnection();
            statement = connection.createStatement();
            String sql = "UPDATE updatesstate"
                    + "\nSET pts = " + pts + ", qts = " + qts + ", date = " + date + ", seq = " + seq
                    + "\nWHERE entitatTLF = " + phoneNumberOfService;
            statement.executeUpdate(sql);

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                 if (connection != null) {
                 connection.close();
                 }
            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                statement = null;
                connection = null;
            }
        }
    }

    public void updatePts(String phoneNumberOfService, int pts) {
        Connection connection = null;
        Statement statement = null;
        try {
            //Class.forName("com.mysql.jdbc.Driver");
            connection = Database.getConnection();
            statement = connection.createStatement();
            String sql = "UPDATE updatesstate"
                    + "\nSET pts = " + pts
                    + "\nWHERE entitatTLF = " + phoneNumberOfService;
            statement.executeUpdate(sql);

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                statement = null;
                connection = null;
            }
        }
    }

    public void updateQts(String phoneNumberOfService, int qts) {
        Connection connection = null;
        Statement statement = null;
        try {
            //Class.forName("com.mysql.jdbc.Driver");
            connection = Database.getConnection();
            statement = connection.createStatement();
            String sql = "UPDATE updatesstate"
                    + "\nSET qts = " + qts
                    + "\nWHERE entitatTLF = " + phoneNumberOfService;
            statement.executeUpdate(sql);

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                statement = null;
                connection = null;
            }
        }
    }

    public void updateDate(String phoneNumberOfService, int date) {
        Connection connection = null;
        Statement statement = null;
        try {
            //Class.forName("com.mysql.jdbc.Driver");
            connection = Database.getConnection();
            statement = connection.createStatement();
            String sql = "UPDATE updatesstate"
                    + "\nSET date = " + date
                    + "\nWHERE entitatTLF = " + phoneNumberOfService;
            statement.executeUpdate(sql);

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                statement = null;
                connection = null;
            }
        }
    }

    public void updateSeq(String phoneNumberOfService, int seq) {
        Connection connection = null;
        Statement statement = null;
        try {
            //Class.forName("com.mysql.jdbc.Driver");
            connection = Database.getConnection();
            statement = connection.createStatement();
            String sql = "UPDATE updatesstate"
                    + "\nSET seq = " + seq
                    + "\nWHERE entitatTLF = " + phoneNumberOfService;
            statement.executeUpdate(sql);

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                statement = null;
                connection = null;
            }
        }
    }

}
