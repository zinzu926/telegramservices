package cat.tecnocampus.telegramservice.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import cat.tecnocampus.telegramservice.apistate.ApiStorage;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ApiStorageMapper {

    private AuthInfoMapper authInfoMapper;
    private DcInfoMapper dcInfoMapper;
    private Connection connection;

    public ApiStorageMapper() {
        this.authInfoMapper = new AuthInfoMapper();
        this.dcInfoMapper = new DcInfoMapper();
    }

    public void crearConnexio() {
        try {
            
            connection = Database.getConnection();
        } catch (SQLException ex) {
            Logger.getLogger(DbMessageMapper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void tancarConnexio() {
        try {
            if (connection != null) {
                connection.close();

            }

        } catch (SQLException ex) {
            Logger.getLogger(DbMessageMapper.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            connection = null;
        }
    }

    public ApiStorage getStorage(int accountId) {
        ApiStorage apiStorage = new ApiStorage();
        Connection connection = null;
        Statement statement = null;
        ResultSet rs = null;
        try {
            
            connection = Database.getConnection();
            statement = connection.createStatement();
            String sql = "SELECT id, primaryDc"
                    + "\nFROM apistorage"
                    + "\nWHERE accountId = " + accountId + ";";
            rs = statement.executeQuery(sql);
            sql = null;
            if (rs.next()) {
                apiStorage.setPrimaryDc(rs.getInt("primaryDc"));
                long id = rs.getLong("id");
                apiStorage.setId(id);
                apiStorage.setAuthInfos(authInfoMapper.getAuthInfos(id));
                apiStorage.setDcInfos(dcInfoMapper.getDcInfos(id));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            rs = null;
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }

            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                statement = null;
                connection = null;
            }

        }
        return apiStorage;
    }

    public void setPrimaryDc(int accountId, int dc) {
        Connection connection = null;
        Statement statement = null;
        try {
          
            connection = Database.getConnection();
            statement = connection.createStatement();
            String sql = "UPDATE apistorage"
                    + "\nSET primaryDc = " + dc
                    + "\nWHERE accountId = " + accountId + ";";
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ApiStorageMapper.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    statement = null;
                }
            }
            try {

                if (connection != null) {
                    connection.close();
                }

            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {

                connection = null;
            }

        }

    }

    public boolean existeix(int accountId) {

        boolean repetit = false;
        Statement statement = null;
        ResultSet rs = null;
        try {
            statement = connection.createStatement();
            String sql = "SELECT id"
                    + "\nFROM apistorage"
                    + "\nWHERE accountId =" + accountId + ";";
            rs = statement.executeQuery(sql);
            if (rs.next()) {
                repetit = true;
            }
            sql = null;

        } catch (SQLException ex) {
            Logger.getLogger(ApiStorageMapper.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            rs = null;
            try {
                if (statement != null) {
                    statement.close();
                }

            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                statement = null;

            }
        }

        return repetit;
    }

    public long insert(int accountId, int primaryDc) {

        ResultSet rs = null;
        PreparedStatement psmt = null;
        long retorna = 0L;
        try {
          

            psmt = connection.prepareStatement("INSERT INTO apistorage ( accountId, primaryDc) VALUES (?,?)", Statement.RETURN_GENERATED_KEYS);

            psmt.setInt(1, accountId);
            psmt.setInt(2, primaryDc);
            psmt.executeUpdate();
            rs = psmt.getGeneratedKeys();
            if (rs.next()) {
                retorna= rs.getLong(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {

            try {
                if (psmt != null) {
                    psmt.close();
                }
                if (connection != null) {
                    connection.close();
                }

            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                psmt = null;
                connection = null;
            }

        }
        return retorna;
    }

    public void delete(long id) {
        Statement statement = null;
         Connection connection=null;
        try {
            
            connection = Database.getConnection();
            statement = connection.createStatement();
            String sql = "DELETE FROM apistorage"
                    + "\nWHERE id = " + id + ";";
            statement.executeUpdate(sql);
            statement.close();
            //connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ApiStorageMapper.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    statement = null;
                }
            }
            
            if (connection!=null){
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ApiStorageMapper.class.getName()).log(Level.SEVERE, null, ex);
                }finally{
                    connection = null;
                }
            }
        }
    }

}
