package cat.tecnocampus.telegramservice.persistence;

public class DbUpdatesState {

    private int pts;
    private int qts;
    private int date;
    private int seq;

    public DbUpdatesState(int pts, int qts, int date, int seq) {
        this.pts = pts;
        this.qts = qts;
        this.date = date;
        this.seq = seq;
    }

    public int getPts() {
        return pts;
    }

    public void setPts(int pts) {
        this.pts = pts;
    }

    public int getQts() {
        return qts;
    }

    public void setQts(int qts) {
        this.qts = qts;
    }

    public int getDate() {
        return date;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

}
