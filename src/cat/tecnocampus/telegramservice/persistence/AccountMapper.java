package cat.tecnocampus.telegramservice.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import cat.tecnocampus.telegramservice.apistate.Account;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AccountMapper {

    public List<Account> getAccounts() {
        List<Account> accounts = new ArrayList<>();
        Connection connection = null;
        Statement statement = null;
        ResultSet rs = null;
        try {            
            connection = Database.getConnection();
            statement = connection.createStatement();
            String sql = "SELECT id, idEntitat, telefon"
                    + "\nFROM compte"
                    + "\nWHERE isTL = 1 AND isLoggedIn = 1;";
            rs = statement.executeQuery(sql);
            while (rs.next()) {
                accounts.add(new Account(rs.getInt("id"), rs.getInt("idEntitat"), rs.getString("telefon"), true));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            rs = null;
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }

            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                statement = null;
                connection = null;
            }
        }
        return accounts;
    }

    public Account get(String phoneNumber) {
        Account account = null;
        Connection connection = null;
        Statement statement = null;
        ResultSet rs = null;
        try {            
            connection = Database.getConnection();
            statement = connection.createStatement();
            String sql = "SELECT id, idEntitat, telefon, isLoggedIn"
                    + "\nFROM compte"
                    + "\nWHERE telefon = '" + phoneNumber + "';";
            rs = statement.executeQuery(sql);
            if (rs.next()) {
                account = new Account(rs.getInt("id"), rs.getInt("idEntitat"), rs.getString("telefon"), rs.getBoolean("isLoggedIn"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            rs = null;
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }

            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                statement = null;
                connection = null;
            }
        }
        return account;
    }

    public int getId(Account account) {
        int idRetorn = -1;
        Connection connection = null;
       
        
        Statement statement = null;
        Statement statementConsulta = null;
        ResultSet rs;
        try {
            //Class.forName("com.mysql.jdbc.Driver");
            connection = Database.getConnection();
            statement = connection.createStatement();

            statementConsulta = connection.createStatement();
            String sqlConsulta = "SELECT id"
                    + "\nFROM compte"
                    + "\nWHERE  telefon = '" + account.getPhoneNumber() + "';";
            rs = statementConsulta.executeQuery(sqlConsulta);
            sqlConsulta = null;
            if (rs != null) {
                if (rs.next()) {
                    idRetorn = rs.getInt("id");
                }
            }

        } catch (SQLException ex) {
            Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);

        } finally {
            rs = null;
            try {               
               
                if (statement != null) {
                    statement.close();
                }
                if (statementConsulta != null) {
                    statementConsulta.close();
                }
                if (connection != null) {
                    connection.close();
                }
               
                
            } catch (SQLException e) {
            }finally{               
               
                statement = null;
                statementConsulta = null;
                connection = null;
            }
        }
        return idRetorn;
    }

    public boolean exists(String phoneNumber) {
        boolean result = false;
        Connection connection = null;
        Statement statement = null;
        ResultSet rs = null;
        try {
            
            connection = Database.getConnection();
            statement = connection.createStatement();
            String sql = "SELECT *"
                    + "\nFROM compte"
                    + "\nWHERE isTL = 1 AND telefon = '" + phoneNumber + "';";
            rs = statement.executeQuery(sql);
            if (rs.next()) {
                result = true;
            }
            sql = null;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            rs = null;
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }          
                
            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            }finally {
                statement = null;
                connection = null;
            }
        }
        return result;
    }

    public void delete(String phoneNumber) {
        Connection connection = null;
        Statement statement = null;
        try {
           
            connection = Database.getConnection();
            statement = connection.createStatement();
            String sql = "UPDATE compte"
                    + "\nSET isTL = 0,isLoggedIn = 0"
                    + "\nWHERE telefon = '" + phoneNumber + "';";
            statement.executeUpdate(sql);
            sql = null;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }                
            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            }finally {
                statement = null;
                connection = null;
            }
        }
    }

    public void update(String phoneNumber,boolean isTL, boolean isLoggedIn) {
        Connection connection = null;
        Statement statement = null;
        try {
            
            connection = Database.getConnection();
            statement = connection.createStatement();
            String sql = "UPDATE compte"
                    + "\nSET isLoggedIn = " + isLoggedIn +" ,isTL = "+isTL
                    + "\nWHERE telefon = '" + phoneNumber + "';";
            statement.executeUpdate(sql);
            sql = null;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }               
            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            }finally {
                statement = null;
                connection = null;
            }
        }
    }

}
