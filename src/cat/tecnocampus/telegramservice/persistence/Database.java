package cat.tecnocampus.telegramservice.persistence;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {
	
	private static final String url ="jdbc:mysql://localhost:3306/010mataro";//010mataro //testMultiEmpresaIM
	//private static final String url ="jdbc:mysql://localhost:3306/citymes";//010mataro //testMultiEmpresaIM
	//private static final String url ="jdbc:mysql://localhost:3306/guarana";//010mataro //testMultiEmpresaIM
	private static final String username = "citymes";//citymes
	private static final String password = "TCMNetLab12";//TCMNetLab12
	
       
	private Database() {
		
	}
	
	public static Connection getConnection() throws SQLException {
		return DriverManager.getConnection(url, username, password);
	}
	
}