/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.tecnocampus.telegramservice.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.telegram.api.TLAbsEncryptedChat;

/**
 *
 * @author netlab
 */
public class EncryptedChatMapper {

    private Connection connection;

    public void crearConnexio() {
        try {
            //Class.forName("com.mysql.jdbc.Driver");
            connection = Database.getConnection();
        } catch (SQLException ex) {
            Logger.getLogger(DbMessageMapper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void tancarConnexio() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(DbMessageMapper.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            connection = null;
        }
    }

    public EncryptedChat getEncryptedChat(int id, int idCompte) {
        Statement statement = null;
        EncryptedChat chat = null;
        try {

            statement = connection.createStatement();
            String sql = "SELECT * "
                    + "\nFROM encryptedChat WHERE idChat = " + id + " and idCompte = " + idCompte + ";";

            ResultSet rs = statement.executeQuery(sql);
            if (rs != null) {

                if (rs.next()) {
                    chat = new EncryptedChat(rs.getInt("idChat"), rs.getInt("idCompte"), rs.getLong("accessHash"), rs.getInt("userId"), rs.getInt("state"), rs.getBytes("keyAccess"), rs.getInt("selfDestructTime"), rs.getBoolean("isOut"),rs.getInt("date"));
                }
                rs = null;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();

                }
            } catch (SQLException ex) {
                Logger.getLogger(DbMessageMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                statement = null;
            }
        }
        return chat;
    }
    
        public EncryptedChat getEncryptedChatPerUid(int uid, int idCompte) {
        Statement statement = null;
        EncryptedChat chat = null;
        try {

            statement = connection.createStatement();
            String sql = "SELECT * "
                    + "\nFROM encryptedChat WHERE userId = " + uid + " and idCompte = " + idCompte + ";";

            ResultSet rs = statement.executeQuery(sql);
            if (rs != null) {

                if (rs.next()) {
                    chat = new EncryptedChat(rs.getInt("idChat"), rs.getInt("idCompte"), rs.getLong("accessHash"), rs.getInt("userId"), rs.getInt("state"), rs.getBytes("keyAccess"), rs.getInt("selfDestructTime"), rs.getBoolean("isOut"),rs.getInt("date"));
                }
                rs = null;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();

                }
            } catch (SQLException ex) {
                Logger.getLogger(DbMessageMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                statement = null;
            }
        }
        return chat;
    }

     public void updateDate(int idChat, int idCompte, int timestamp) {
        Statement statement = null;

        try {
            String sqlAuthsqlAuth = "UPDATE encryptedChat"
                    + "\nSET  date ="+timestamp
                    + "\nWHERE idCompte = " + idCompte + " and idChat =" + idChat + ";";
            statement = connection.createStatement();
               
            statement.executeUpdate(sqlAuthsqlAuth);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();

                }
            } catch (SQLException ex) {
                Logger.getLogger(DbMessageMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                statement = null;
            }
        }
    }
    
    
    
    public void updateEncryptedChatKey(TLAbsEncryptedChat chat, int idCompte, byte[] key) {
        PreparedStatement preparedStatement = null;

        try {
            String sqlAuthsqlAuth = "UPDATE encryptedChat"
                    + "\nSET  keyAccess =?"
                    + "\nWHERE idCompte = " + idCompte + " and idChat =" + chat.getId() + ";";
            preparedStatement = connection.prepareStatement(sqlAuthsqlAuth);
               preparedStatement.setBytes(1, key);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();

                }
            } catch (SQLException ex) {
                Logger.getLogger(DbMessageMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                preparedStatement = null;
            }
        }
    }

    public void updateChat(EncryptedChat chat) {

        PreparedStatement preparedStatement = null;

        try {
            String sqlAuthsqlAuth = "UPDATE encryptedChat"
                    + "\nSET idChat =" + chat.getIdChat() + ", userId =" + chat.getUserId() + ", keyAccess = ?, isOut =" + chat.isOut() + ", selfDestructTime =" + chat.getSelfDestructTime()
                    + "\n, accessHash =" + chat.getAccessHash() + ", state =" + chat.getState()
                    + "\nWHERE idCompte = " + chat.getIdCompte() + " and idChat =" + chat.getIdChat() + ";";
            preparedStatement = connection.prepareStatement(sqlAuthsqlAuth);
            preparedStatement.setBytes(1, chat.getKey());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();

                }
            } catch (SQLException ex) {
                Logger.getLogger(DbMessageMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                preparedStatement = null;
            }
        }
    }

    public void deleteChat(EncryptedChat chat) {
        Statement statement = null;

        try {
            statement = connection.createStatement();
            String sqlAuth = "DELETE FROM encryptedChat"
                    + "\nWHERE idCompte = " + chat.getIdCompte() + " and idChat =" + chat.getIdChat() + ";";
            statement.executeUpdate(sqlAuth);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();

                }
            } catch (SQLException ex) {
                Logger.getLogger(DbMessageMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                statement = null;
            }
        }
    }

    public void insertChat(EncryptedChat chat) {
        PreparedStatement psmt = null;

        try {

            psmt = connection.prepareStatement(""
                    + "INSERT INTO encryptedChat (idChat, idCompte, accessHash, userId, state, keyAccess, selfDestructTime, isOut,date) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
            psmt.setInt(1, chat.getIdChat());
            psmt.setInt(2, chat.getIdCompte());
            psmt.setLong(3, chat.getAccessHash());
            psmt.setInt(4, chat.getUserId());
            psmt.setInt(5, chat.getState());
            psmt.setBytes(6, chat.getKey());
            psmt.setInt(7, chat.getSelfDestructTime());
            psmt.setBoolean(8, chat.isOut());
            psmt.setInt(9, chat.getDate());
            psmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (psmt != null) {
                try {
                    psmt.close();

                } catch (SQLException ex) {
                    Logger.getLogger(DbMessageMapper.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    psmt = null;
                }
            }
        }
    }
}
