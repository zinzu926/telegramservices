package cat.tecnocampus.telegramservice.persistence;

import cat.tecnocampus.telegramservice.util.Configuracio;
import cat.tecnocampus.telegramservice.util.RespostaKeyword;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DbMessageMapper {

    private Connection connection;

    public void crearConnexio() {
        try {
            //Class.forName("com.mysql.jdbc.Driver");
            connection = Database.getConnection();
        } catch (SQLException ex) {
            Logger.getLogger(DbMessageMapper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void tancarConnexio() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(DbMessageMapper.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            connection = null;
        }
    }

    public Connection getConnection() {
        return connection;
    }

    public void storeReceivedMessage(DbMessage message) {
        PreparedStatement psmt = null;
        try {

            psmt = connection.prepareStatement(""
                    + "INSERT INTO missatge (data, missatge, origen, timestamp, tipusMissatge, esProcessat, tipusDades, path, latitut, longitut, usuari, idEntitat, telefonServei,idRespostaRapida, idIM) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, 1)");
            psmt.setString(1, message.getDate());
            psmt.setString(2, message.getText());
            psmt.setString(3, message.getSender());
            psmt.setLong(4, message.getTimestamp());
            psmt.setInt(5, message.getTipusMissatge());
            psmt.setInt(6, message.getEsprocessat());
            psmt.setInt(7, message.getDatatype());
            psmt.setString(8, message.getPath());
            psmt.setDouble(9, message.getLatitude());
            psmt.setDouble(10, message.getLongitude());
            if (message.getEsprocessat() > 0) {
                psmt.setString(11, "CityMes");
            } else {
                psmt.setNull(11, Types.VARCHAR);
            }

            psmt.setInt(12, message.getCompanyId());
            psmt.setString(13, message.getPhoneNumberOfService());
            psmt.setInt(14, message.getIdRespostaRapida());
            psmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (psmt != null) {
                try {
                    psmt.close();

                } catch (SQLException ex) {
                    Logger.getLogger(DbMessageMapper.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    psmt = null;
                }
            }
        }
    }

    public int storeMessageThatNeedsToBeSend(DbMessage message) {
        int idRetorn = -1;
        PreparedStatement psmt = null;
        ResultSet generatedKeys = null;
        try {

            psmt = connection.prepareStatement(""
                    + "INSERT INTO missatge (data, missatge, origen, timestamp, tipusMissatge, esProcessat, tipusDades,usuari, idEntitat, telefonServei, idRespostaRapida, idIM) "
                    + "VALUES (?, ?, ?, ?, 1, 0, ?, ?, ?, ?, ?, 1)", Statement.RETURN_GENERATED_KEYS);
            psmt.setString(1, message.getDate());
            psmt.setString(2, message.getText());
            psmt.setString(3, message.getSender());
            psmt.setLong(4, message.getTimestamp());
            psmt.setInt(5, message.getDatatype());
            psmt.setString(6, Configuracio.usuariCitymes);
            psmt.setInt(7, message.getCompanyId());
            psmt.setString(8, message.getPhoneNumberOfService());
            psmt.setInt(9, message.getIdRespostaRapida());
            psmt.executeUpdate();
            generatedKeys = psmt.getGeneratedKeys();
            if (generatedKeys.next()) {
                return generatedKeys.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (psmt != null) {
                try {
                    psmt.close();

                } catch (SQLException ex) {
                    Logger.getLogger(DbMessageMapper.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    psmt = null;
                }
            }
        }
        return idRetorn;
    }

      public void updateMessageOrigen(String origen, String uid) {
        Statement statement = null;
        try {

            statement = connection.createStatement();
            String sql = "UPDATE missatge"
                    + "\nSET origen = " +origen
                    + "\nWHERE origen = " + uid;
            statement.executeUpdate(sql);

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                try {
                    statement.close();

                } catch (SQLException ex) {
                    Logger.getLogger(DbMessageMapper.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    statement = null;
                }
            }
        }
    }
    
    
    
    public void updateMessage(Connection connection, DbMessage message, boolean success) {
        Statement statement = null;
        try {

            statement = connection.createStatement();
            String sql = "UPDATE missatge"
                    + "\nSET esProcessat = " + (success ? "1" : "0")
                    + "\nWHERE id = " + message.getDbId();
            statement.executeUpdate(sql);

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                try {
                    statement.close();

                } catch (SQLException ex) {
                    Logger.getLogger(DbMessageMapper.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    statement = null;
                }
            }
        }
    }

    public List<String> getMessages() {
        List<String> llistaTLF = null;
        Statement statement = null;
        try {

            statement = connection.createStatement();
            String sql = "SELECT telefonServei FROM missatge WHERE idIM = 1 AND"
                    + "\ntipusMissatge = 1 AND esProcessat = 0 GROUP BY telefonServei;";

            ResultSet rs = statement.executeQuery(sql);
            if (rs != null) {
                llistaTLF = new ArrayList<>();
                while (rs.next()) {
                    llistaTLF.add(rs.getString("telefonServei"));
                }
                rs = null;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();

                }
            } catch (SQLException ex) {
                Logger.getLogger(DbMessageMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                statement = null;
            }
        }
        return llistaTLF;
    }

    public List<String> getMessagesBroadcast() {
        List<String> llistaTLF = null;
        Statement statement = null;
        try {

            statement = connection.createStatement();
            String sql = "SELECT telefonServei FROM missatge WHERE idIM = 1 AND"
                    + "\ntipusMissatge = 2 AND esProcessat = 0 GROUP BY telefonServei;";

            ResultSet rs = statement.executeQuery(sql);
            if (rs != null) {
                llistaTLF = new ArrayList<>();
                while (rs.next()) {
                    llistaTLF.add(rs.getString("telefonServei"));
                }
                rs = null;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DbMessageMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                statement = null;
            }
        }
        return llistaTLF;
    }

    public String getMessageError(int idEntitat, int codi) {
        Statement statement = null;
        ResultSet rs = null;
        String misssatge = "";
        try {

            statement = connection.createStatement();
            String sql;
            if (codi == 0) {
                sql = "SELECT missatgeError"
                        + "\nFROM entitat"
                        + "\nWHERE id = " + idEntitat + ";";
            } else {
                sql = "SELECT missatgeNoSuport"
                        + "\nFROM entitat"
                        + "\nWHERE id = " + idEntitat + ";";
            }
            rs = statement.executeQuery(sql);
            rs.first();
            misssatge = rs.getString(1);
        } catch (SQLException ex) {
            Logger.getLogger(DbMessageMapper.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DbMessageMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                statement = null;
            }
        }
        return misssatge;
    }

    public int getIdIdioma(int idEntitat) {
        Statement statement = null;
        ResultSet rs = null;
        int misssatge = -1;
        try {

            statement = connection.createStatement();
            String sql;

            sql = "SELECT idIdioma"
                    + "\nFROM entitat"
                    + "\nWHERE id = " + idEntitat + ";";

            rs = statement.executeQuery(sql);
            if (rs != null) {
                rs.first();
                misssatge = rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DbMessageMapper.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (SQLException ex) {
                Logger.getLogger(DbMessageMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                statement = null;
            }
        }
        return misssatge;
    }

    public List<DbMessage> getMessages(String telefonServei, int idCompte) {
        List<DbMessage> messages = null;
        Statement statement = null;
        try {

            statement = connection.createStatement();
            String sql = "SELECT missatge.id, missatge, uid, tipusDades, path, latitut, longitut, users.access_hash"
                    + "\nFROM missatge INNER JOIN users ON origen=telefon WHERE idIM = 1 AND"
                    + "\ntipusMissatge = 1 AND esProcessat = 0 and missatge.telefonServei='" + telefonServei + "' and users.idCompte = " + idCompte
                    + "\nORDER BY timestamp ASC LIMIT 0,15;";
            ResultSet rs = statement.executeQuery(sql);
            if (rs != null) {
                messages = new ArrayList<>();
                while (rs.next()) {
                    messages.add(new DbMessage(rs.getInt("id"), rs.getString("missatge"), rs.getString("uid"), rs.getInt("tipusDades"), rs.getString("path"), rs.getDouble("latitut"), rs.getDouble("longitut"), telefonServei, rs.getLong("access_hash")));
                }
                rs = null;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();

                }
            } catch (SQLException ex) {
                Logger.getLogger(DbMessageMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                statement = null;
            }
        }
        return messages;
    }

    public List<DbMessage> getMessagesBroadcast(String telefonServei, int limit, int idCompte) {
        List<DbMessage> messages = null;
        Statement statement = null;
        try {

            statement = connection.createStatement();
            String sql = "SELECT missatge.id, missatge, uid, tipusDades, path, latitut, longitut, users.access_hash"
                    + "\nFROM missatge INNER JOIN users ON origen=telefon WHERE idIM = 1 AND"
                    + "\ntipusMissatge = 2 AND esProcessat = 0 and missatge.telefonServei='" + telefonServei + "' and users.idCompte = " + idCompte
                    + "\nORDER BY timestamp ASC LIMIT 0," + limit + ";";
            ResultSet rs = statement.executeQuery(sql);
            if (rs != null) {
                messages = new ArrayList<>();
                while (rs.next()) {
                    messages.add(new DbMessage(rs.getInt("id"), rs.getString("missatge"), rs.getString("uid"), rs.getInt("tipusDades"), rs.getString("path"), rs.getDouble("latitut"), rs.getDouble("longitut"), telefonServei, rs.getLong("access_hash")));
                }
                rs = null;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();

                }
            } catch (SQLException ex) {
                Logger.getLogger(DbMessageMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                statement = null;
            }
        }
        return messages;
    }

    public RespostaKeyword checkIfMessageIsKeyword(int companyId, String message, boolean webservices, int valor) {
        RespostaKeyword resp = null;
        Statement statement = null;
        try {

            statement = connection.createStatement();
            String sql;
            if (webservices) {
                sql = "SELECT id, web, servei"
                        + "\nFROM respostesRapides"
                        + "\nWHERE esActiva=1 and servei=" + valor + " and tipus = (SELECT id FROM tipusResposta WHERE descripcio = 'keyword' AND idEntitat = " + companyId + ")"
                        + "\nAND id IN (select idResposta FROM sinonims WHERE descripcio ='" + message + "');";
            } else {
                sql = "SELECT id, web, servei"
                        + "\nFROM respostesRapides"
                        + "\nWHERE esActiva=1 and tipus = (SELECT id FROM tipusResposta WHERE descripcio = 'keyword' AND idEntitat = " + companyId + ")"
                        + "\nAND id IN (select idResposta FROM sinonims WHERE descripcio ='" + message + "');";
            }
            ResultSet rs = statement.executeQuery(sql);
            sql = null;
            if (rs != null) {
                int count = 0;
                String result = "";
                int servei = -1;
                int id = -1;
                while (rs.next()) {
                    id = rs.getInt("id");

                    servei = rs.getInt("servei");
                    result = rs.getString("web");
                    count++;
                }

                if (count == 1) {
                    resp = new RespostaKeyword(id, servei, result, count);
                } else {
                    resp = new RespostaKeyword(count);
                }
                rs = null;
                result = null;

            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();

                }
            } catch (SQLException ex) {
                Logger.getLogger(DbMessageMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                statement = null;
            }
        }
        return resp;
    }

    public String checkTimetable(int companyId) {
        String result = "";
        Statement statement = null;
        ResultSet rs = null;
        try {

            statement = connection.createStatement();
            String sql = "SELECT horaInici, horaFinal, missatge"
                    + "\nFROM confForaServei "
                    + "\nWHERE idEntitat = " + companyId + ";";
            rs = statement.executeQuery(sql);
            if (rs.next()) {
                Calendar now = Calendar.getInstance(TimeZone.getTimeZone("Europe/Madrid"));

                String horaIniciString = rs.getString("horaInici");
                Calendar horaInici = Calendar.getInstance(TimeZone.getTimeZone("Europe/Madrid"));
                horaInici.set(Calendar.HOUR_OF_DAY, Integer.parseInt(horaIniciString.split(":")[0]));
                horaInici.set(Calendar.MINUTE, Integer.parseInt(horaIniciString.split(":")[1]));

                String horaFinalString = rs.getString("horaFinal");
                Calendar horaFinal = Calendar.getInstance(TimeZone.getTimeZone("Europe/Madrid"));
                horaFinal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(horaFinalString.split(":")[0]));
                horaFinal.set(Calendar.MINUTE, Integer.parseInt(horaFinalString.split(":")[1]));

                if (now.before(horaInici) || now.after(horaFinal)) {
                    result = rs.getString("missatge");
                }
                now = null;
                horaInici = null;
                horaFinal = null;
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            rs = null;
            try {
                if (statement != null) {
                    statement.close();

                }

            } catch (SQLException ex) {
                Logger.getLogger(DbMessageMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                statement = null;
            }
        }
        return result;
    }

    public synchronized boolean existUsuari(String phoneNumber, int idEntitat) {
        boolean result = false;
        Statement statement = null;
        ResultSet rs = null;
        boolean connect = false;
        try {

            if (connection == null) {
                connect = true;
                connection = Database.getConnection();
            }
            //SELECT *FROM users where telefon='34627783794' and idCompte IN (select id from compte where idEntitat =2)
            statement = connection.createStatement();
            String sql = "SELECT id"
                    + "\nFROM missatge"
                    + "\nWHERE origen = '" + phoneNumber + "' and idEntitat= " + idEntitat + ";";
            rs = statement.executeQuery(sql);
            if (rs != null && rs.next()) {
                result = true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DbUserMapper.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            rs = null;
            try {
                if (statement != null) {
                    statement.close();
                }

            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                statement = null;
            }
            if (connect) {
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(DbUpdatesStateMapper.class.getName()).log(Level.SEVERE, null, ex);
                    } finally {
                        connection = null;
                    }
                }
            }
        }
        return result;

    }

}
