/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.tecnocampus.telegramservice.persistence;

/**
 *
 * @author netlab
 */
public class EncryptedChat {

    private int _id;
    private int idChat;
    private int idCompte;
    private long accessHash;
    private int userId;
    private int state;
    private byte[] key;
    private int selfDestructTime;
    private boolean isOut;
    private int date;

    /*public EncryptedChat(int _id, int userId, int state) {
        this._id = _id;
        this.userId = userId;
        this.state = state;
    }*/
      public EncryptedChat(int idChat,int idCompte, long accessHash, int userId, int state,byte[] key,int selfDestructTime,boolean isOut, int date) {
        this.idChat = idChat;
        this.idCompte = idCompte;
        this.accessHash = accessHash;
        this.userId = userId;
        this.state = state;
        this.key = key;
        this.selfDestructTime = selfDestructTime;
        this.isOut = isOut;
        this.date = date;
    }

    public EncryptedChat() {
    }

    public int getId() {
        return _id;
    }

    public void setId(int _id) {
        this._id = _id;
    }
    
    public int getIdCompte(){
        return idCompte;
    }
    
    public void setIdCompte(int idCompte){
        this.idCompte = idCompte;
    }
    
    
       public int getIdChat() {
        return idChat;
    }

    public void setIdChat(int idChat) {
        this.idChat = idChat;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public byte[] getKey() {
        return key;
    }

    public void setKey(byte[] key) {
        this.key = key;
    }

    public long getAccessHash() {
        return accessHash;
    }

    public void setAccessHash(long accessHash) {
        this.accessHash = accessHash;
    }

    public int getSelfDestructTime() {
        return selfDestructTime;
    }

    public void setSelfDestructTime(int selfDestructTime) {
        this.selfDestructTime = selfDestructTime;
    }

    public boolean isOut() {
        return isOut;
    }

    public void setOut(boolean out) {
        isOut = out;
    }
    
    public int getDate(){
        return date;
    }
    
    public void setDate(int data){
        date = data;
    }
    
}
