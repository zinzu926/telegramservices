package cat.tecnocampus.telegramservice.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import cat.tecnocampus.telegramservice.apistate.OldSession;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OldSessionMapper {

    public List<OldSession> getOldSessions(long authInfoId) {
        List<OldSession> oldSessions = new ArrayList<>();
        Connection connection = null;
        Statement statement = null;
        try {
            //Class.forName("com.mysql.jdbc.Driver");
            connection = Database.getConnection();
            statement = connection.createStatement();
            String sql = "SELECT session"
                    + "\nFROM oldsession"
                    + "\nWHERE authInfoId = " + authInfoId + ";";
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                OldSession oldSession = new OldSession(rs.getBytes("session"));
                oldSessions.add(oldSession);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }               
            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            }finally {
                statement = null;
                connection = null;
            }
        }
        return oldSessions;
    }

    public void insertAll(long authInfoId, List<OldSession> oldSessions) {
        Connection connection = null;
        PreparedStatement psmt=null;
        try {
            //Class.forName("com.mysql.jdbc.Driver");
            connection = Database.getConnection();
            connection.setAutoCommit(false);
             psmt = connection.prepareStatement("INSERT INTO oldsession (session, authInfoId) VALUES (?, ?)");
            for (OldSession oldSession : oldSessions) {
                psmt.setBytes(1, oldSession.getSession());
                psmt.setLong(2, authInfoId);
                psmt.addBatch();
            }
            psmt.executeBatch();
            connection.commit();
            

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (psmt!=null){
                    psmt.close();
                }
                if (connection != null) {
                    connection.close();
                }              
            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            }finally {
                psmt = null;
                connection = null;
            }
        }
    }

}
