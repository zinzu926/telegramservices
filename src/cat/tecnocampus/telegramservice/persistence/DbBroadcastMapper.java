/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.tecnocampus.telegramservice.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author netlab
 */
public class DbBroadcastMapper {

    public int insert(Connection connection, DbBroadcast dbBroadcast) {
        PreparedStatement psmt = null;
        boolean connect = false;
        int retorna = -1;
        try {
            if (connection == null) {
                connect = true;
                connection = Database.getConnection();
            }

            psmt = connection.prepareStatement("INSERT INTO broadcast (idRespostaRapida,idUser ) VALUES (?, ?)");
            psmt.setInt(1, dbBroadcast.getIdResposta());
            psmt.setInt(2, dbBroadcast.getIdUser());

            psmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();

        } finally {
            if (psmt != null) {
                try {
                    psmt.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ApiStorageMapper.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    psmt = null;
                }
            }
            if (connect) {
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(DbUpdatesStateMapper.class.getName()).log(Level.SEVERE, null, ex);
                    } finally {
                        connection = null;
                    }
                }
            }
        }
        return retorna;
    }

    public synchronized boolean exists(Connection connection, DbBroadcast broad) {
        boolean result = false;
        Statement statement = null;
        ResultSet rs = null;
        boolean connect = false;
        try {

            if (connection == null) {
                connect = true;
                connection = Database.getConnection();
            }
            statement = connection.createStatement();
            String sql = "SELECT id"
                    + "\nFROM broadcast"
                    + "\nWHERE idRespostaRapida = " + broad.getIdResposta() + " and idUser = " + broad.getIdUser() + ";";
            rs = statement.executeQuery(sql);
            if (rs.next()) {
                result = true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DbUserMapper.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            rs = null;
            try {
                if (statement != null) {
                    statement.close();
                }

            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                statement = null;
            }
            if (connect) {
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(DbUpdatesStateMapper.class.getName()).log(Level.SEVERE, null, ex);
                    } finally {
                        connection = null;
                    }
                }
            }
        }
        return result;

    }

    public void delete(Connection connection, DbBroadcast broad) {
        boolean connect = false;
        Statement statement = null;
        try {
            if (connection == null) {
                connect = true;
                connection = Database.getConnection();
            }
            statement = connection.createStatement();
            String sqlAuth = "DELETE FROM broadcast"
                    + "\nWHERE idRespostaRapida  = " + broad.getIdResposta() + " and idUser = " + broad.getIdUser() + ";";
            statement.executeUpdate(sqlAuth);

        } catch (SQLException ex) {
            Logger.getLogger(DbUpdatesStateMapper.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DbUpdatesStateMapper.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    statement = null;
                }
            }
            if (connect) {
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(DbUpdatesStateMapper.class.getName()).log(Level.SEVERE, null, ex);
                    } finally {
                        connection = null;
                    }
                }
            }
        }

    }

}
