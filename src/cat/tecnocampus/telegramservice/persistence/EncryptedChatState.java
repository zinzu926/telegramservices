/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.tecnocampus.telegramservice.persistence;

/**
 *
 * @author netlab
 */
public class EncryptedChatState {

    public static final int EMPTY = 0;
    public static final int NORMAL = 1;
    public static final int REQUESTED = 2;
    public static final int WAITING = 3;
    public static final int DISCARDED = 4;
}
