/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.tecnocampus.telegramservice.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author netlab
 */
public class DbUserMapper {

    /* public int insert(Connection connection, DbUser user) {
     PreparedStatement psmt = null;
     ResultSet rs = null;
     int retorna = -1;
     boolean connect = false;
     try {
     if (connection == null) {
     connect = true;
     connection = Database.getConnection();
     }

     psmt = connection.prepareStatement("INSERT INTO users (telefon,uid, telefonServei) VALUES (?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
     psmt.setString(1, user.getTelefon());
     psmt.setString(2, user.getUid());
     psmt.setString(3, user.getTelefonServei());
     psmt.executeUpdate();
     rs = psmt.getGeneratedKeys();
     if (rs.next()) {
     retorna = rs.getInt(1);
     }
     } catch (SQLException e) {
     e.printStackTrace();

     } finally {
     if (psmt != null) {
     try {
     psmt.close();
     } catch (SQLException ex) {
     Logger.getLogger(ApiStorageMapper.class.getName()).log(Level.SEVERE, null, ex);
     } finally {
     psmt = null;
     }
     }
     if (connect) {
     if (connection != null) {
     try {
     connection.close();
     } catch (SQLException ex) {
     Logger.getLogger(DbUpdatesStateMapper.class.getName()).log(Level.SEVERE, null, ex);
     } finally {
     connection = null;
     }
     }
     }
     }
     return retorna;
     }*/
    public int insertAll(Connection connection, DbUser user) {
        PreparedStatement psmt = null;
        ResultSet rs = null;
        int retorna = -1;
        boolean connect = false;
        try {
            if (connection == null) {
                connect = true;
                connection = Database.getConnection();
            }

            psmt = connection.prepareStatement("INSERT INTO users (telefon,uid,access_hash,idCompte ) VALUES (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            psmt.setString(1, user.getTelefon());
            psmt.setString(2, user.getUid());
            psmt.setLong(3, user.getAccessHash());
            psmt.setInt(4, user.getIdCompte());
            psmt.executeUpdate();
            rs = psmt.getGeneratedKeys();
            if (rs.next()) {
                retorna = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();

        } finally {
            if (psmt != null) {
                try {
                    psmt.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ApiStorageMapper.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    psmt = null;
                }
            }
            if (connect) {
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(DbUpdatesStateMapper.class.getName()).log(Level.SEVERE, null, ex);
                    } finally {
                        connection = null;
                    }
                }
            }
        }
        return retorna;
    }

    /* public String getUser(String telf) {

     return ;
     }*/
    public void updateUser(Connection connection, DbUser user) {
        boolean connect = false;
        Statement statement = null;
        try {
            if (connection == null) {
                connect = true;
                connection = Database.getConnection();
            }

            statement = connection.createStatement();
            String sql = "UPDATE users"
                    + "\nSET uid = " + user.getUid() + " , access_hash = " + user.getAccessHash()
                    + "\nWHERE telefon = '" + user.getTelefon() + "' and idCompte = " + user.getIdCompte() + ";";
            statement.executeUpdate(sql);
            sql = null;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }

            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                statement = null;
            }
            if (connect) {
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(DbUpdatesStateMapper.class.getName()).log(Level.SEVERE, null, ex);
                    } finally {
                        connection = null;
                    }
                }
            }
        }
    }

    public void updateUserUID(Connection connection, DbUser user) {
        boolean connect = false;
        Statement statement = null;
        try {
            if (connection == null) {
                connect = true;
                connection = Database.getConnection();
            }

            statement = connection.createStatement();
            String sql = "UPDATE users"
                    + "\nSET telefon  = " + user.getTelefon() + " , access_hash = " + user.getAccessHash()
                    + "\nWHERE uid = '" + user.getUid() + "' and idCompte = " + user.getIdCompte() + ";";
            statement.executeUpdate(sql);
            sql = null;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }

            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                statement = null;
            }
            if (connect) {
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(DbUpdatesStateMapper.class.getName()).log(Level.SEVERE, null, ex);
                    } finally {
                        connection = null;
                    }
                }
            }
        }
    }

    /*public void updateUserUid(Connection connection, DbUser user) {
     boolean connect = false;
     Statement statement = null;
     try {
     if (connection == null) {
     connect = true;
     connection = Database.getConnection();
     }

     statement = connection.createStatement();
     String sql = "UPDATE users"
     + "\nSET uid = " + user.getUid()
     + "\nWHERE telefon = '" + user.getTelefon() + "';";
     statement.executeUpdate(sql);
     sql = null;
     } catch (SQLException e) {
     e.printStackTrace();
     } finally {
     try {
     if (statement != null) {
     statement.close();
     }

     } catch (SQLException ex) {
     Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
     } finally {
     statement = null;
     }
     if (connect) {
     if (connection != null) {
     try {
     connection.close();
     } catch (SQLException ex) {
     Logger.getLogger(DbUpdatesStateMapper.class.getName()).log(Level.SEVERE, null, ex);
     } finally {
     connection = null;
     }
     }
     }
     }
     }*/
    public synchronized int exists(Connection connection, String phoneNumber, int idCompte) {
        int result = -1;
        Statement statement = null;
        ResultSet rs = null;
        boolean connect = false;
        try {

            if (connection == null) {
                connect = true;
                connection = Database.getConnection();
            }
            statement = connection.createStatement();
            String sql = "SELECT id"
                    + "\nFROM users"
                    + "\nWHERE telefon = '" + phoneNumber + "' and idCompte = " + idCompte + ";";
            rs = statement.executeQuery(sql);
            if (rs.next()) {
                result = rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DbUserMapper.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            rs = null;
            try {
                if (statement != null) {
                    statement.close();
                }

            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                statement = null;
            }
            if (connect) {
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(DbUpdatesStateMapper.class.getName()).log(Level.SEVERE, null, ex);
                    } finally {
                        connection = null;
                    }
                }
            }
        }
        return result;

    }

    public synchronized int existsUID(Connection connection, String uid, int idCompte) {
        int result = -1;
        Statement statement = null;
        ResultSet rs = null;
        boolean connect = false;
        try {

            if (connection == null) {
                connect = true;
                connection = Database.getConnection();
            }
            statement = connection.createStatement();
            String sql = "SELECT id"
                    + "\nFROM users"
                    + "\nWHERE uid = '" + uid + "' and idCompte = " + idCompte + ";";
            rs = statement.executeQuery(sql);
            if (rs.next()) {
                result = rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DbUserMapper.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            rs = null;
            try {
                if (statement != null) {
                    statement.close();
                }

            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                statement = null;
            }
            if (connect) {
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(DbUpdatesStateMapper.class.getName()).log(Level.SEVERE, null, ex);
                    } finally {
                        connection = null;
                    }
                }
            }
        }
        return result;

    }

    public String getOrigenPerUid(Connection connection, int uid, int idCompte) {

        String result = "";
        Statement statement = null;
        ResultSet rs = null;
        boolean connect = false;
        try {

            if (connection == null) {
                connect = true;
                connection = Database.getConnection();
            }
            statement = connection.createStatement();
            String sql = "SELECT telefon"
                    + "\nFROM users"
                    + "\nWHERE uid = '" + uid + "' and idCompte = " + idCompte + ";";
            rs = statement.executeQuery(sql);
            if (rs.next()) {
                result = rs.getString(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DbUserMapper.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            rs = null;
            try {
                if (statement != null) {
                    statement.close();
                }

            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                statement = null;
            }
            if (connect) {
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(DbUpdatesStateMapper.class.getName()).log(Level.SEVERE, null, ex);
                    } finally {
                        connection = null;
                    }
                }
            }
        }
        return result;
    }

}
