package cat.tecnocampus.telegramservice.persistence;

public class DbMessage {

    private int dbId; // id
    private String date; // data
    private String text; // missatge
    private String sender; // origen
    
    private long timestamp; // timestamp
    private int esProcessat;
    private int tipusMissatge;
    private int datatype; // tipusDades, 0 = text, 1 = image, 2 = video, 3 = audio, 4 = location, 5 = file
    private String path; // path
    private double latitude; // latitut
    private double longitude; // longitut
    private int companyId; // idEntitat
    private String phoneNumberOfService; // telefonServei
    private long access_hash;
    private int idRespostaRapida;
    private String usuari;

    // Getting message from database to send it
    public DbMessage(int dbId, String text, String sender, int datatype, String path, double latitude, double longitude, String phoneNumberOfService, long access_hash) {
        this.dbId = dbId;
        this.text = text;
        this.sender = sender;
        this.datatype = datatype;
        this.path = path;
        this.latitude = latitude;
        this.longitude = longitude;
        this.phoneNumberOfService = phoneNumberOfService;
        this.access_hash = access_hash;
    }

    // Storing message in database
    public DbMessage(String date, String text, String sender, long timestamp, int esPorcessat,int tipus, int datatype, String path, double latitude, double longitude, int companyId, String phoneNumberOfService, int idRespostaRapida) {
        this.date = date;
        this.text = text;
        this.sender = sender;
        this.timestamp = timestamp;
        this.esProcessat = esPorcessat;
        tipusMissatge=tipus;
        this.datatype = datatype;
        this.path = path;
        this.latitude = latitude;
        this.longitude = longitude;
        this.companyId = companyId;
        this.phoneNumberOfService = phoneNumberOfService;
        this.idRespostaRapida = idRespostaRapida;
    }

    public int getDbId() {
        return dbId;
    }

    public String getDate() {
        return date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
  
    
    public void setDataType(int tip){
        datatype=tip;
    }
    
    public void setTipusMissatge (int tipus){
        tipusMissatge = tipus;
    }
    
    public void setIdBD(int id){
        dbId=id;
    }
    
    public String getSender() {
        return sender;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void increaseTimestampWith() {
        this.timestamp = timestamp + 5;
    }

    public int getDatatype() {
        return datatype;
    }
    
    public int getTipusMissatge(){
        return tipusMissatge;
    }

    public String getPath() {
        return path;
    }

    public int getEsprocessat() {
        return esProcessat;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public int getCompanyId() {
        return companyId;
    }

    public String getPhoneNumberOfService() {
        return phoneNumberOfService;
    }

    public long getAccess_hash() {
        return access_hash;
    }
    
    public int getIdRespostaRapida(){
        return idRespostaRapida;
    }
    
    public void setIdRespostaRapida(int id){
        idRespostaRapida = id;
    }

}
