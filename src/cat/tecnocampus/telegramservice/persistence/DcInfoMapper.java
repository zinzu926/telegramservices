package cat.tecnocampus.telegramservice.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import cat.tecnocampus.telegramservice.apistate.DcInfo;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DcInfoMapper {
        private Connection connection;

    public void crearConnexio() {
        try {
            //Class.forName("com.mysql.jdbc.Driver");
            connection = Database.getConnection();
        } catch (SQLException ex) {
            Logger.getLogger(DbMessageMapper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void tancarConnexio() {
        try {
            if (connection != null) {
                connection.close();

            }

        } catch (SQLException ex) {
            Logger.getLogger(DbMessageMapper.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            connection = null;
        }
    }

    public List<Integer> getDcId(long apiStorageId) {
        List<Integer> dcInfos = new ArrayList<>();
        Connection connection = null;
        Statement statement = null;
        ResultSet rs = null;
        try {
            //Class.forName("com.mysql.jdbc.Driver");
            connection = Database.getConnection();
            statement = connection.createStatement();
            String sql = "SELECT dcId"
                    + "\nFROM dcinfo"
                    + "\nWHERE apiStorageId = " + apiStorageId + ";";
            rs = statement.executeQuery(sql);
            while (rs.next()) {

                dcInfos.add(rs.getInt("dcId"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            rs = null;
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }

            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                statement = null;
                connection = null;
            }
        }
        return dcInfos;
    }

    public List<DcInfo> getDcInfos(long apiStorageId) {
        List<DcInfo> dcInfos = new ArrayList<>();
        Connection connection = null;
        Statement statement = null;
        ResultSet rs = null;
        try {
            //Class.forName("com.mysql.jdbc.Driver");
            connection = Database.getConnection();
            statement = connection.createStatement();
            String sql = "SELECT dcId, address, port, version"
                    + "\nFROM dcinfo"
                    + "\nWHERE apiStorageId = " + apiStorageId + ";";
            rs = statement.executeQuery(sql);
            while (rs.next()) {
                DcInfo dcInfo = new DcInfo(rs.getInt("dcId"), rs.getString("address"), rs.getInt("port"), rs.getInt("version"));
                dcInfos.add(dcInfo);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            rs = null;
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }

            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                statement = null;
                connection = null;
            }
        }
        return dcInfos;
    }

    public void insertAll(long apiStorageId, List<DcInfo> dcInfos) {
        PreparedStatement psmt = null;
        try {
            //Class.forName("com.mysql.jdbc.Driver");
             connection = Database.getConnection();
            connection.setAutoCommit(false);
            psmt = connection.prepareStatement("INSERT INTO dcinfo (dcId, address, port, version, apiStorageId) VALUES (?, ?, ?, ?, ?)");
            for (DcInfo dcInfo : dcInfos) {
                psmt.setInt(1, dcInfo.getDcId());
                psmt.setString(2, dcInfo.getAddress());
                psmt.setInt(3, dcInfo.getPort());
                psmt.setInt(4, dcInfo.getVersion());
                psmt.setLong(5, apiStorageId);
                psmt.addBatch();
            }
            psmt.executeBatch();
            connection.commit();

            //connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (psmt != null) {
                try {
                    psmt.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DcInfoMapper.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            psmt = null;

        }

    }

     public void updateDCInfo(long idApiStorage, int dcid, String address, int port, int version) {
        Statement statement = null;
       
        ResultSet rs = null;
        try {
            
            statement = connection.createStatement();

            String sqlDC = "UPDATE dcinfo"
                    + "\nSET port = " + port+" , version = "+ version
                    + "\nWHERE apiStorageId = "+idApiStorage+" AND dcid = "+dcid+";";
            statement.executeUpdate(sqlDC);

            //connection.close();

            /* } catch (ClassNotFoundException ex) {
             Logger.getLogger(ApiStorageMapper.class.getName()).log(Level.SEVERE, null, ex);*/
        } catch (SQLException ex) {
            Logger.getLogger(ApiStorageMapper.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            rs = null;
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ApiStorageMapper.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    statement = null;
                }
            }
          
        }
    }
    
    
    public void deleteDCInfo(long idApiStorage) {
        Statement statement = null;
        Connection connection = null;
        ResultSet rs = null;
        try {
            connection = Database.getConnection();
            statement = connection.createStatement();

            String sqlDC = "DELETE FROM dcinfo"
                    + "\nWHERE apiStorageId = " + idApiStorage + ";";
            statement.executeUpdate(sqlDC);

            //connection.close();

            /* } catch (ClassNotFoundException ex) {
             Logger.getLogger(ApiStorageMapper.class.getName()).log(Level.SEVERE, null, ex);*/
        } catch (SQLException ex) {
            Logger.getLogger(ApiStorageMapper.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            rs = null;
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ApiStorageMapper.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    statement = null;
                }
            }

            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(AuthInfoMapper.class.getName()).log(Level.SEVERE, null, ex);
                } finally {
                    connection = null;
                }
            }
        }
    }

}
