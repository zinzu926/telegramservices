/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cat.tecnocampus.telegramservice.persistence;

/**
 *
 * @author netlab
 */
public class DbUser {
    
    private String uid;
    private String telefon;
    private long access_hash;
    private int idCompte;    
    
    public DbUser(String uid, String telefon, long access, int idCompte){
        this.uid= uid;
        this.telefon = telefon;
        access_hash=access;
         this.idCompte= idCompte;
    }
        
    public void setUid(String id){
        uid=id;
    }
    
    public void setTelefon (String tlf){
        telefon= tlf;
    }  
    
    public String getUid(){
        return uid;
    }
    
    public String getTelefon(){
        return telefon;
    }
    
    public long getAccessHash(){
        return access_hash;
    }
    
    public void setAccessHash(long access){
        this.access_hash = access;
    }
    
        
    public int getIdCompte(){
        return idCompte;
    }
    
    public void setTelefonServei(int idCompte){
        this.idCompte = idCompte;
    }
    
}
