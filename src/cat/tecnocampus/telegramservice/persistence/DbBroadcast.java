/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cat.tecnocampus.telegramservice.persistence;

/**
 *
 * @author netlab
 */
public class DbBroadcast {
    
    private int id;
    private int idRespostaRapida;
    private int idUser;
    
    public DbBroadcast(int idResposta, int idUser){
        idRespostaRapida=idResposta;
        this.idUser= idUser;
    }
    
    public int getIdResposta(){
        return idRespostaRapida;
    }
    
    public void setIdResposta(int id){
        idRespostaRapida=id;
    }
    
    public int getIdUser(){
        return idUser;
    }
    
    
    public void setIdUser (int id){
        idUser = id;
    }
    
}
