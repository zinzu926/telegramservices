package cat.tecnocampus.telegramservice.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import cat.tecnocampus.telegramservice.apistate.LastKnownSalt;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LastKnownSaltMapper {

    public List<LastKnownSalt> getSalts(long authInfoId) {
        List<LastKnownSalt> salts = new ArrayList<>();
        Connection connection = null;
        Statement statement = null;
        ResultSet rs = null;
        try {
            //Class.forName("com.mysql.jdbc.Driver");
            connection = Database.getConnection();
            statement = connection.createStatement();
            String sql = "SELECT validSince, validUntil, salt"
                    + "\nFROM lastknownsalt"
                    + "\nWHERE authInfoId = " + authInfoId + ";";
            rs = statement.executeQuery(sql);
            while (rs.next()) {
                int validUntil = rs.getInt("validUntil");
                if (validUntil >= (System.currentTimeMillis() / 1000L)) {
                    LastKnownSalt salt = new LastKnownSalt(rs.getInt("validSince"), validUntil, rs.getLong("salt"));
                    salts.add(salt);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            rs = null;
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
                
                
            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            }finally {
                statement = null;
                connection = null;
            }
        }
        return salts;
    }

    public void insertAll(long authInfoId, List<LastKnownSalt> salts) {
        Connection connection = null;
        PreparedStatement psmt=null;
        try {
            //Class.forName("com.mysql.jdbc.Driver");
            connection = Database.getConnection();
            connection.setAutoCommit(false);
             psmt = connection.prepareStatement("INSERT INTO lastknownsalt (validSince, validUntil, salt, authInfoId) VALUES (?, ?, ?, ?)");
            for (LastKnownSalt salt : salts) {
                psmt.setInt(1, salt.getValidSince());
                psmt.setInt(2, salt.getValidUntil());
                psmt.setLong(3, salt.getSalt());
                psmt.setLong(4, authInfoId);
                psmt.addBatch();
            }
            psmt.executeBatch();
            connection.commit();
            

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (psmt!=null){
                   psmt.close();
                }
                if (connection != null) {
                    connection.close();
                }

            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            }finally {
                psmt = null;
                connection = null;
            }
        }
        deleteExpiredSalts();
    }

    public void deleteExpiredSalts() {
        Connection connection = null;
        Statement statement = null;
        try {
            //Class.forName("com.mysql.jdbc.Driver");
            connection = Database.getConnection();
            statement = connection.createStatement();
            int unixtimestamp = (int) (System.currentTimeMillis() / 1000L);
            String sql = "DELETE FROM lastknownsalt"
                    + "\nWHERE validUntil < " + unixtimestamp;
            statement.executeUpdate(sql);
          
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
               

            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            }finally {
                statement = null;
                connection = null;
            }
            
        }
    }

    public void delete(long authInfoId) {
        Connection connection = null;
        Statement statement = null;
        try {
            //Class.forName("com.mysql.jdbc.Driver");
            connection = Database.getConnection();
            statement = connection.createStatement();           
            String sql = "DELETE FROM lastknownsalt"
                    + "\nWHERE authInfoId = " + authInfoId;
            statement.executeUpdate(sql);
          
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    connection.close();
                }
               

            } catch (SQLException ex) {
                Logger.getLogger(AccountMapper.class.getName()).log(Level.SEVERE, null, ex);
            }finally {
                statement = null;
                connection = null;
            }
            
        }
    }
    
    
}
