package cat.tecnocampus.telegramservice.thread;

import cat.tecnocampus.telegramservice.controller.UpdatesStateController;
import cat.tecnocampus.telegramservice.persistence.DbUpdatesState;
import cat.tecnocampus.telegramservice.util.EmailComunicacio;
import cat.tecnocampus.telegramservice.util.MemoryUsage;
import cat.tecnocampus.telegramservice.util.MyLogger;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.telegram.api.requests.TLRequestAccountUpdateStatus;
import org.telegram.api.requests.TLRequestUpdatesGetState;
import org.telegram.api.updates.TLState;

public class VerifyAuth implements Runnable {

    private ApplicationThread application;
    private static final int TIMEOUT = 30 * 1000;
    private static final long tempsDiff = 20 * 60;
    private boolean enabled;

    public VerifyAuth(ApplicationThread application) {
        this.application = application;

    }

    @Override
    public void run() {

        this.enabled = true;
        if (this.enabled) {
            application.getApi().doRpcCallWeak(new TLRequestAccountUpdateStatus(false));
            MyLogger.log(application.getAccount().getPhoneNumber(), "Verifying auth status");
            MemoryUsage.getInstancia().sendFreeMemory();
            int attempt = 0;
            boolean needsReAuth = canWeMakeRpcCalls(attempt);
            if (needsReAuth) {
                MyLogger.log(application.getAccount().getPhoneNumber(), "Re-auth is needed!");
                if (!application.refreshDataCenters()) {
                    EmailComunicacio email= EmailComunicacio.getInstancia();
                    email.missatge(application.getAccount().getPhoneNumber());
                }
            } else {
                MyLogger.log(application.getAccount().getPhoneNumber(), "Re-auth is NOT needed");
            }
        } else {
            MyLogger.log(application.getAccount().getPhoneNumber(), "WARNING! verifying auth status is switched off!");
        }

    }

    private boolean canWeMakeRpcCalls(int attempt) {
        while (attempt < 2) {
            try {
                MyLogger.log(application.getAccount().getPhoneNumber(), "Testing if we can make Rpc Call - attempt " + attempt);

                TLState tlState = application.getApi().doRpcCall(new TLRequestUpdatesGetState(), TIMEOUT);

                DbUpdatesState updatesState = application.getUpdatesStateController().getUpdatesState();

                if (tlState != null && updatesState != null) {
                    if (tlState.getDate() - updatesState.getDate() > tempsDiff) {
                        application.getUpdatesController().invalidateUpdates();

                    }
                    return false;

                } else {
                    attempt++;
                    return canWeMakeRpcCalls(attempt);
                }

            } catch (IOException e) {
                attempt++;
                return canWeMakeRpcCalls(attempt);
            }
        }
        return true;
    }

    public void pause() {
        this.enabled = false;
        MyLogger.log(application.getAccount().getPhoneNumber(), "Verifying auth status has been paused!");
    }

    public void resume() {
        this.enabled = true;
        MyLogger.log(application.getAccount().getPhoneNumber(), "Verifying auth status has been resumed!");
    }

}
