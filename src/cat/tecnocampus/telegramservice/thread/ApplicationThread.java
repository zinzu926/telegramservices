package cat.tecnocampus.telegramservice.thread;

import cat.tecnocampus.telegramservice.apistate.Account;
import cat.tecnocampus.telegramservice.apistate.ApiState;
import cat.tecnocampus.telegramservice.controller.AuthController;
import cat.tecnocampus.telegramservice.controller.FloodController;
import cat.tecnocampus.telegramservice.controller.IncomingMessagesController;
import cat.tecnocampus.telegramservice.controller.UpdatesStateController;
import cat.tecnocampus.telegramservice.controller.UploadController;
import cat.tecnocampus.telegramservice.persistence.DbUpdatesState;
import cat.tecnocampus.telegramservice.process.UpdateProfilePicture;
import cat.tecnocampus.telegramservice.util.ApiConfig;
import cat.tecnocampus.telegramservice.util.Configuracio;
import cat.tecnocampus.telegramservice.util.MyLogger;
import cat.tecnocampus.telegramservice.util.MySocketException;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.telegram.api.TLAbsUpdates;
import org.telegram.api.TLConfig;
import org.telegram.api.engine.ApiCallback;
import org.telegram.api.engine.AppInfo;
import org.telegram.api.engine.RpcException;
import org.telegram.api.engine.TelegramApi;
import org.telegram.api.requests.TLRequestHelpGetConfig;
import org.telegram.api.requests.TLRequestUpdatesGetState;
import org.telegram.api.updates.TLState;

public class ApplicationThread implements Runnable {

    private Account account;
    private TelegramApi api;
    private AuthController authController;
    private FloodController floodController;
    private IncomingMessagesController incomingMessagesController;
    private UploadController uploadController;
    private UpdatesStateController updatesStateController;
    private ExecutorService incomingMessageProcesses;


    /*private int relogin = 0;
     private boolean apicreated=false;*/
    public ApplicationThread(Account account) {
        this.account = account;
        this.incomingMessageProcesses = Executors.newSingleThreadExecutor();
        this.authController = new AuthController(this);
        this.uploadController = new UploadController(this);
        floodController = new FloodController(this);
        incomingMessagesController = new IncomingMessagesController(this);
        this.updatesStateController = new UpdatesStateController(this);

    }

    public void createApi() {
        MyLogger.log(account.getPhoneNumber(), "Creating API");
        this.api = new TelegramApi(new ApiState(this), new AppInfo(ApiConfig.API_ID, ApiConfig.DEVICE_MODEL, ApiConfig.SYSTEM_VERSION, ApiConfig.APP_VERSION, ApiConfig.LANG_CODE), new ApiCallback() {

            @Override
            public void onAuthCancelled(TelegramApi api) {
                if (api != ApplicationThread.this.api) {
                    return;
                }
                authController.logout();
            }

            @Override
            public void onUpdatesInvalidated(TelegramApi api) {
                if (api != ApplicationThread.this.api) {
                    return;
                }
                if (!account.isLoggedIn() || !authController.isAuthenticated()) {
                    return;
                }
                Thread thread = new Thread() {
                    @Override
                    public void run() {
                        if (incomingMessagesController != null) {
                            incomingMessagesController.invalidateUpdates();
                        }
                    }
                };
                thread.start();

            }

            @Override
            public void onUpdate(TLAbsUpdates updates) {
                if (!account.isLoggedIn() || !authController.isAuthenticated()) {
                    return;
                }
                if (incomingMessagesController != null && incomingMessageProcesses != null) {
                    incomingMessagesController.setIncomingUpdates(updates);
                    incomingMessageProcesses.execute(incomingMessagesController);
                }

            }

        });

        refreshDataCenters();

    }

    public synchronized IncomingMessagesController getUpdatesController() {
        return incomingMessagesController;
    }

    public synchronized FloodController getFloodController() {
        return floodController;
    }

    public void shutdown() {
        if (api != null) {
            api = null;

        }

        if (updatesStateController != null) {
            updatesStateController = null;
        }
        if (uploadController != null) {
            uploadController = null;
        }
        if (incomingMessagesController != null) {
            incomingMessagesController = null;
        }
        if (authController != null) {
            authController = null;
        }
        try {
            finalize();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        if (authController != null) {
            if (authController.isAuthenticated()) {
                DbUpdatesState updatesState = getUpdatesStateController().getUpdatesState();
                if (updatesState == null) {
                    TLState state = null;
                    try {
                        state = getApi().doRpcCall(new TLRequestUpdatesGetState());
                    } catch (IOException ex) {
                        Logger.getLogger(ApplicationThread.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    if (state != null) {
                        getUpdatesStateController().insertUpdatesState(state.getPts(), state.getQts(), state.getDate(), state.getSeq());
                    }
                }

                authController.startVerifyAuthPoller();
            }
        }

    }

    public void requestActivationCode() throws MySocketException {

        getAuthController().requestActivationCode();
    }

    public void createAccount(String code, int companyId, String des) throws MySocketException, RpcException {
        try {

            getAuthController().signIn(code, des);
            createFolderStructure();

        } catch (MySocketException e) {

            throw e;
        }
    }

    private void createFolderStructure() {
        MyLogger.log(account.getPhoneNumber(), "Creating folder structure");
        String path = Configuracio.pathFolder + account.getPhoneNumber();
        File dir = new File(path);
        if (!dir.exists()) {
            dir.mkdirs();
        }
    }

    public void updateImage(String imagePath) throws MySocketException {
        ExecutorService executor = Executors.newCachedThreadPool();
        Callable<Object> callable = new UpdateProfilePicture(this, imagePath);
        Future<Object> future = executor.submit(callable);
        try {
            future.get();
        } catch (InterruptedException | ExecutionException e) {
            throw new MySocketException("0<separa>Error<separa>" + e.getMessage() + "<separa></finalitzacio>");
        }
    }

    public boolean refreshDataCenters() {
        boolean ok = true;
        try {
            MyLogger.log(account.getPhoneNumber(), "Refreshing data centers");
            TLConfig config = getApi().doRpcCallNonAuth(new TLRequestHelpGetConfig());
            getApiState().updateSettings(config);
            api.resetConnectionInfo();
            MyLogger.log(account.getPhoneNumber(), "Data centers updated");
        } catch (IOException e) {
            ok = false;
            MyLogger.log(account.getPhoneNumber(), "Data centers timeout");

        }
        return ok;
    }

    public Account getAccount() {
        return account;
    }

    public TelegramApi getApi() {
        return api;
    }

    public ApiState getApiState() {
        return (ApiState) api.getState();
    }

    public UploadController getUploadController() {
        return uploadController;
    }

    public AuthController getAuthController() {
        return authController;
    }

    public UpdatesStateController getUpdatesStateController() {
        return updatesStateController;
    }

}
