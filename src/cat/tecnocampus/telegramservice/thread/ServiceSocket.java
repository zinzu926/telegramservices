package cat.tecnocampus.telegramservice.thread;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import cat.tecnocampus.telegramservice.TelegramService;
import cat.tecnocampus.telegramservice.process.ClientConnection;

public class ServiceSocket implements Runnable {
	
	//private static final int PORT = 6001; //citymes
	private static final int PORT = 5559; //010mataro

	private TelegramService service;
	private ServerSocket serverSocket;
	private ExecutorService executorService;
	
	public ServiceSocket(TelegramService service) {
		this.service = service;
		try {
			serverSocket = new ServerSocket(PORT);
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.executorService = Executors.newCachedThreadPool();
	}

	@Override
	public void run() {
		System.out.println("Service socket running");
		while(true) {
			Socket socket;
			try {
				socket = serverSocket.accept();
				ClientConnection clientConnection = new ClientConnection(service, socket);
				executorService.execute(clientConnection);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
