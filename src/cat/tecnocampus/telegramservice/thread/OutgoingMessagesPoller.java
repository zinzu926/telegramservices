package cat.tecnocampus.telegramservice.thread;

import cat.tecnocampus.telegramservice.persistence.DbMessage;
import cat.tecnocampus.telegramservice.persistence.DbMessageMapper;
import cat.tecnocampus.telegramservice.process.OutgoingMessage;
import cat.tecnocampus.telegramservice.util.MyLogger;
import java.util.HashMap;
import java.util.Map;

public class OutgoingMessagesPoller implements Runnable {

    // private ExecutorService outgoingMessageProcesses;
    private Map<String, ApplicationThread> applications;
    private Map<String, Integer> mapLimits;
    private static final int limitMissatges = 15;

    public OutgoingMessagesPoller(Map<String, ApplicationThread> applications) {

        //this.outgoingMessageProcesses = Executors.newCachedThreadPool();
        this.applications = applications;
        mapLimits = new HashMap<>();
    }

    @Override
    public void run() {

        DbMessageMapper dbMessageMapper = new DbMessageMapper();

        dbMessageMapper.crearConnexio();
        for (String tlf : dbMessageMapper.getMessages()) {
            ApplicationThread application = applications.get(tlf);
            if (application != null && !application.getFloodController().getWait() && application.getAccount().isLoggedIn()) {
                MyLogger.log("Polling  database for new outgoing messages");
                int conta = 0;
                for (DbMessage message : dbMessageMapper.getMessages(tlf,application.getAccount().getAccountId())) {
                    new OutgoingMessage(dbMessageMapper.getConnection(), application, message).send();
                    conta++;
                }
                mapLimits.put(tlf, limitMissatges - conta);
            }
        }
        for (String tlf : dbMessageMapper.getMessagesBroadcast()) {
            ApplicationThread application = applications.get(tlf);
            if (application != null && !application.getFloodController().getWait() && application.getAccount().isLoggedIn() && dbMessageMapper.checkTimetable(application.getAccount().getAccountId()).equals("")) {
                MyLogger.log("Polling  database broadcast for new outgoing messages");
                int limit;
                if (!mapLimits.containsKey(tlf)) {
                    limit = limitMissatges;
                } else {
                    limit = mapLimits.get(tlf);
                }                
                for (DbMessage message : dbMessageMapper.getMessagesBroadcast(tlf, limit,application.getAccount().getAccountId())) {
                    new OutgoingMessage(dbMessageMapper.getConnection(), application, message).send();

                }
            }
        }

        dbMessageMapper.tancarConnexio();
        dbMessageMapper = null;
        mapLimits.clear();

    }

}
